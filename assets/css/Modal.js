import React from 'react';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({


    overlay: {
        backgroundColor: 'rgba(0,0,0,.5)',
        flex: 1,
        justifyContent: 'flex-end',
        padding: 20
    },
    container: {
        backgroundColor: '#fff',
        minHeight: 300,
        maxHeight: '100%',
        padding: 15,
        borderRadius: 7
    },
    textLeft: {
        textAlign: 'left'
    },
    header:{
        position:'relative'
    },
    headerClose:{
        position:'absolute',
        top: -5,
        right: 0, 
        zIndex: 9
    }

});