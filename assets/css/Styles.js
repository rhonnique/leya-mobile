import React from "react";
import { StyleSheet } from "react-native";
import { hidden } from "ansi-colors";

export default StyleSheet.create({
  /* ****** MARGINS ****** */
  mb5: {
    marginBottom: 5
  },
  mb10: {
    marginBottom: 10
  },
  mb15: {
    marginBottom: 15
  },
  mb20: {
    marginBottom: 20
  },
  mb30: {
    marginBottom: 30
  },
  mb40: {
    marginBottom: 40
  },
  mb50: {
    marginBottom: 50
  },

  mt5: {
    marginTop: 5
  },
  mt6: {
    marginTop: 6
  },
  mt7: {
    marginTop: 7
  },
  mt8: {
    marginTop: 8
  },
  mt10: {
    marginTop: 10
  },
  mt15: {
    marginTop: 15
  },
  mt20: {
    marginTop: 20
  },
  mt30: {
    marginTop: 30
  },
  mt40: {
    marginTop: 40
  },
  mt50: {
    marginTop: 50
  },

  /* ****** PADDINGS ****** */
  p5: {
    padding: 5
  },
  p10: {
    padding: 10
  },
  p15: {
    padding: 15
  },
  p20: {
    padding: 20
  },
  p25: {
    padding: 25
  },
  py5: {
    paddingVertical: 5
  },
  py6: {
    paddingVertical: 6
  },
  py10: {
    paddingVertical: 10
  },
  py15: {
    paddingVertical: 15
  },
  py20: {
    paddingVertical: 20
  },
  py25: {
    paddingVertical: 25
  },

  px5: {
    paddingHorizontal: 5
  },
  px25: {
    paddingHorizontal: 25
  },
  px20: {
    paddingHorizontal: 20
  },
  px10: {
    paddingHorizontal: 10
  },
  px15: {
    paddingHorizontal: 15
  },

  pb5: {
    paddingBottom: 5
  },
  pb10: {
    paddingBottom: 10
  },
  pb15: {
    paddingBottom: 15
  },
  pb20: {
    paddingBottom: 20
  },
  pb25: {
    paddingBottom: 25
  },
  pl5:{
    paddingLeft: 5
  },
  pl10:{
    paddingLeft: 10
  },
  pl15:{
    paddingLeft: 15
  },
  

  /* ****** ALIGN ****** */
  alignSelfCenter: {
    alignSelf: "center"
  },

  textAlignCenter: {
    textAlign: "center"
  },

  /* ****** FONTS ****** */
  fontLight: {
    fontFamily: "nunito-sans-light"
  },
  fontRegular: {
    fontFamily: "nunito-sans-regular"
  },
  fontBold: {
    fontFamily: "nunito-sans-bold"
  },
  fontBlack: {
    fontFamily: "nunito-sans-black"
  },

  /* ****** WIDTHS ****** */
  width100: {
    width: "100%"
  },
  width80: {
    width: "80%"
  },
  width70: {
    width: "70%"
  },
  width50: {
    width: "50%"
  },
  width60: {
    width: "60%"
  },

  /* ****** CONTAINERS ****** */
  container: {
    width: "90%",
    alignSelf: "center"
  },
  containerMdx: {
    width: "85%",
    alignSelf: "center"
  },
  containerMdxl: {
    width: "80%",
    alignSelf: "center"
  },
  containerMd: {
    width: "76%",
    alignSelf: "center"
  },
  containerSm: {
    width: "60%",
    alignSelf: "center"
  },
  containerFlex: {
    flex: 1
  },
  containerAlignCC: {
    alignItems: "center",
    justifyContent: "center"
  },
  containerCard: {
    borderRadius: 5,
    minHeight: 60,
    position: "relative",
    backgroundColor: "#ffffff",
    overflow: "hidden"
  },

  cardTextWarning: {
    fontSize: 14,
    color: "#D19B00"
  },

  cardCloseBtn: {
    position: "absolute",
    right: 5,
    top: 0,
    zIndex: 3,
    paddingHorizontal: 5
  },

  flex: {
    flex: 1
  },

  /* ****** ALERTS ****** */
  alert: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "flex-start",
    paddingHorizontal: 15,
    paddingVertical: 8,
    backgroundColor: "rgba(0,0,0,.1)"
  },
  alertFixed: {
    position: "absolute",
    top: 0,
    zIndex: 1
  },
  alertWarning: {
    backgroundColor: "rgb(222,172,9)" //#FFCB10
  },
  alertDanger: {
    backgroundColor: "#FA4B48"
  },
  alertBadge: {
    fontSize: 11,
    backgroundColor: "#fff",
    borderRadius: 5,
    paddingHorizontal: 5,
    paddingVertical: 2,
    marginRight: 8
  },
  alertText: {
    fontSize: 12,
    paddingVertical: 3,
    color: "#ffffff"
  },

  /* ****** TEXT ICON VIEWS ****** */

  textIconBox: {
    flexDirection: "row",
    justifyContent: "flex-start",
    marginTop: 5
  },
  textIcon: {
    /* position:'absolute',
        top: 15,
        marginLeft: 50 */
    top: -2.5,
    marginRight: 5
  },
  textIconText: {
    paddingTop: 15
  },

  /* ****** TEXT ****** */
  textDefault: {
    color: "#666"
  },
  textDark: {
    color: "#333"
  },
  textGreen: {
    color: "#00A134"
  },
  textTheme: {
    color: "#00A134"
  },
  textDanger: {
    color: "red"
  },
  textWhite: {
    color: "#fffff"
  },
  textBlack: {
    color: "#000000"
  },
  textGrayLight: {
    color: "#999999"
  },
  textDarkOrange: {
    color: "#D19B00"
  },
  fontWeightBold: {
    fontWeight: "bold"
  },
  fontWeightSemiBold: {
    fontWeight: "600"
  },

  fontSize8: {
    fontSize: 8
  },
  fontSize9: {
    fontSize: 9
  },
  fontSize10: {
    fontSize: 10
  },
  fontSize11: {
    fontSize: 11
  },
  fontSize12: {
    fontSize: 12
  },
  fontSize13: {
    fontSize: 13
  },
  fontSize14: {
    fontSize: 14
  },
  fontSize15: {
    fontSize: 15
  },
  fontSize16: {
    fontSize: 16
  },
  fontSize17: {
    fontSize: 17
  },
  fontSize18: {
    fontSize: 18
  },
  fontSize20: {
    fontSize: 20
  },
  fontSize22: {
    fontSize: 22
  },
  fontSize25: {
    fontSize: 25
  },

  /* ****** BACKGROUNDS ****** */
  bgTheme: {
    backgroundColor: "#00A134"
  },
  bgThemeLight: {
    backgroundColor: "#33B35C"
  },
  bgThemeTrans: {
    backgroundColor: "#E5F5EA"
  },
  bgThemeOrange: {
    backgroundColor: "#FFCA33"
    //rgba(3,91,32,0.15) 0 5px 10px 0
  },
  bgDanger: {
    backgroundColor: "#F85857"
  },
  bgTransparent: {
    backgroundColor: "transparent"
  },

  bgWarning: {
    backgroundColor: "#FFF7E0"
  },

  /* ****** BORDERS ****** */
  outlineWhite3: {
    borderColor: "#fff",
    borderWidth: 3
  },

  /* ****** LINE HEIGHTS ****** */
  lh18: {
    lineHeight: 18
  },
  lh19: {
    lineHeight: 19
  },
  lh20: {
    lineHeight: 20
  },
  lh21: {
    lineHeight: 21
  },
  lh22: {
    lineHeight: 22
  },
  lh23: {
    lineHeight: 23
  },
  lh25: {
    lineHeight: 25
  },

  /* ****** SHADOWS ****** */
  boxShadowBtn: {
    shadowColor: "#ccc",
    shadowOffset: {
      width: 0,
      height: 5
    },
    shadowOpacity: 0.36,
    shadowRadius: 6.68,

    elevation: 11
  },
  boxShadow: {
    /* shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5 */


    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.2,
    shadowRadius: 4,
    elevation: 5,
  },

  /* ****** BUTTONS ****** */
  btnThemeOrange: {
    height: 45,
    width: "65%",
    borderRadius: 5,
    backgroundColor: "#FFCA33"
  },
  btnThemeDefault: {
    height: 45,
    width: "85%",
    borderRadius: 5,
    backgroundColor: "#f5f5f5"
  },

  btnTextGreen: {
    textAlign: "center",
    fontSize: 18,
    padding: 9,
    color: "#00A134",
    fontWeight: "bold"
  },
  bgGreen: {
    flex: 1,
    backgroundColor: "#00A134",
    position: "relative"
  },
  bgGreenImg: {
    resizeMode: "cover",
    position: "absolute"
  },
  header: {
    alignItems: "center",
    justifyContent: "center",
    flex: 2
  },
  header2: {
    alignItems: "center",
    justifyContent: "center",
    //justifyContent: 'flex-end',
    height: "30%"
    //flex: 7
  },
  formContainer: {
    flex: 5,
    backgroundColor: "#ffffff",
    borderRadius: 10,
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 20,
    padding: 20
  },
  headingContainer: {
    color: "#000000",
    display: "flex",
    textAlign: "center",
    marginBottom: 20
  },
  headingH1: {
    fontSize: 24,
    color: "#000000",
    display: "flex",
    textAlign: "center",
    marginBottom: 8,
    fontWeight: "bold"
  },
  headingText: {
    color: "#666",
    fontSize: 14,
    display: "flex",
    textAlign: "center"
  },

  formGroup: {
    width: "100%",
    marginBottom: 20
  },
  formGroupMaterialBg: {
    width: "100%",
    backgroundColor: "rgba(0, 0, 0, 0.2)",
    borderRadius: 5,
    paddingHorizontal: 15,
    paddingBottom: 5
  },
  tfContainerStyle: {
    backgroundColor: "rgba(0,0,0,.2)",
    borderRadius: 5,
    height: 55,
    paddingTop: 3
  },
  tfContainerStyle2: {
    backgroundColor: "rgba(0,0,0,.1)",
    borderRadius: 5,
    height: 55,
    paddingTop: 3
  },
  tfInputContainerStyle: {
    paddingBottom: 0,
    paddingTop: 0,
    paddingHorizontal: 15
  },
  tfLabelTextStyle: {
    paddingHorizontal: 15,
    bottom: 8
  },
  tfStyle: {
    top: 15
  },

  inputFormat: {
    height: 50,
    backgroundColor: "#F7F7F7",
    borderRadius: 5,
    paddingLeft: 16,
    paddingRight: 16
  },
  inputFormatGreen: {
    height: 50,
    backgroundColor: "#009430",
    borderRadius: 5,
    paddingLeft: 16,
    paddingRight: 16
  },
  positionBottom: {
    position: "absolute",
    bottom: 0
  },
  positionTopImg: {
    position: "absolute",
    top: 0,
    width: "100%"
  },

  /* ****** BUTTONS ****** */
  row: {
    flexDirection: "row",
    justifyContent: "flex-start"
  },
  rowPx: {
    width: "100%",
    paddingHorizontal: 15
  },
  rowSpace: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  rows: {
    width: "100%"
  },
  containerWrapper: {
    paddingLeft: 15,
    paddingRight: 15
  },
  containerDefault: {
    flex: 1,
    backgroundColor: "#fff"
  },
  containerWrapperDefault: {
    paddingVertical: 25,
    paddingHorizontal: 20
  },
  containerEmpty: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  emptyTitle: {
    fontSize: 18,
    color: "#333",
    fontWeight: "500",
    marginVertical: 8
  },
  emptyMsg: {
    color: "#999",
    textAlign: "center",
    lineHeight: 20
  },
  containerPadV: {
    paddingVertical: 25
  },
  containerPadH: {
    paddingHorizontal: 20
  },

  textDefault: {
    color: "#666"
  },

  textWhite: {
    color: "#ffffff"
  },

  textGrey: {
    color: "#999"
  },

  /* Buttons */
  btn: {
    height: 50,
    borderRadius: 5,
    backgroundColor: "#ffff",
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 30
  },
  btnBlock: {
    width: "100%"
  },
  btnTheme: {
    backgroundColor: "#00A134"
  },

  btnXs: {
    height: 35
  },

  btnText: {
    fontSize: 18,
    fontWeight: "bold"
  },

  btnThemeOrange: {
    backgroundColor: "#FFCA33"
  },

  btnThemeWhite: {
    height: 45,
    width: "65%",
    borderRadius: 5,
    backgroundColor: "#ffffff"
  },
  btnThemeGreen: {
    height: 45,
    width: "65%",
    borderRadius: 5,
    backgroundColor: "#00A134"
  },
  btnThemeDefault: {
    height: 45,
    width: "85%",
    borderRadius: 5,
    backgroundColor: "#f5f5f5"
  },
  btnTextGreen: {
    textAlign: "center",
    fontSize: 18,
    padding: 9,
    color: "#00A134",
    fontWeight: "bold"
  },
  btnTextWhite: {
    textAlign: "center",
    fontSize: 18,
    padding: 9,
    color: "#fff",
    fontWeight: "bold"
  },
  headingH4: {
    fontSize: 15,
    marginBottom: 20,
    fontWeight: "500"
  },
  vBottom: {
    justifyContent: "flex-end",
    flex: 1
  },
  vBottomText: {
    textAlign: "center",
    color: "#979797",
    fontSize: 11
  },
  flexEnd: {
    justifyContent: "flex-end"
  },
  summaryBox: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderBottomColor: "#f1f1f1",
    borderBottomWidth: 2
  },
  formGroupMat: {
    borderBottomColor: "#f1f1f1",
    borderBottomWidth: 2
  },
  summaryBoxLabel: {
    color: "#999",
    display: "flex"
  },
  summaryBoxText: {
    color: "#333",
    fontSize: 16,
    fontWeight: "500",
    display: "flex"
  },

  bgLightGray: {
    backgroundColor: "#f1f1f1"
  },
  textRight: {
    textAlign: "right"
  },
  textLeft: {
    textAlign: "left"
  },


  userDataContainer: {
    height: 80,
    backgroundColor: '#00A134'
  },

  porfolioBalanceContainer: { 
    height: 120,
    borderRadius:5,
    backgroundColor: '#fff',
    marginLeft: 15,
    marginRight: 15,
    justifyContent: 'center',
    position: 'relative'
  },
  porfolioBalanceContainerInner:{
    justifyContent: 'center', 
    width: '100%', 
    overflow: 'hidden', 
    borderRadius: 5, 
    position: 'relative', 
    height: 120
  },

  porfolioContainer: {
    backgroundColor: "#fff",
    position: "relative",
    paddingTop: 10
  },

  infoContainer: {
    backgroundColor: "#fff",
    height: 95,
    borderRadius: 5,
    justifyContent: "center",
    alignItems: "center",
  },

  infoContainerInner: {
    backgroundColor: "#fff",
    position: "relative",
    height: 95,
    borderRadius: 5,
    overflow: "hidden",
    justifyContent: "center",
    alignItems: "center",
    marginVertical: 10
  },

  listView: {
    padding: 20,
    backgroundColor: "#fff",
    borderBottomColor: "#f1f1f1",
    borderBottomWidth: 2,
    position: "relative",
    flexDirection: "row"
  },
  listViewText: {
    color: "#333",
    fontWeight: "500",
    display: "flex",
    fontSize: 14
  },
  listViewIcon: {
    position: "absolute",
    right: 20,
    top: 22
  },
  listItem: {
    flex: 1,
    flexDirection: "row",
    position: "relative",
    paddingVertical: 15
  },
  listBadgeBox: {
    width: 10,
    alignItems: "center",
    justifyContent: "center"
  },
  listTextBox: {
    paddingLeft: 10
  },
  listTextM: {
    color: "#333",
    fontWeight: "500",
    fontSize: 15
  },
  listTextS: {
    color: "#999",
    fontSize: 12
  },
  listAmountBox: {
    position: "absolute",
    right: 0,
    top: 15
  },
  badgeD: {
    paddingHorizontal: 5,
    paddingVertical: 12,
    borderRadius: 5,
    alignSelf: "flex-end",
    end: 0
  },
  bSuccess: {
    backgroundColor: "#CCECD6"
  },
  bSuccessText: {
    color: "#086A28",
    fontSize: 14
  },
  bDanger: {
    backgroundColor: "#FEDEDD"
  },
  bDangerText: {
    color: "#F85857",
    fontSize: 14
  },
  termsModalHeader:{
    paddingHorizontal: 15, 
    paddingVertical: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#EAEAEA",
    position:"relative"
  },
  termsModalHeaderClose:{
    position: "absolute",
    top: 13,
    right: 16
  }
});
