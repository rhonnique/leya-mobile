import {createStackNavigator, createAppContainer} from 'react-navigation';
import Home from './Home';
import Success from './../Notifications/Success';
import Register from './Register';
import Login from './Login';
import Password from './Password';
import Intro from './Intro';

const GuessNav = createStackNavigator({
  Home: { screen: Home },
  Intro: { screen: Intro },
  Success: { screen: Success},
  Register: { screen: Register},
  Login: { screen: Login},
  Password: { screen: Password }

});

export default createAppContainer(GuessNav);
