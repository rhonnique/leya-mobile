import React, { Fragment } from 'react';
import {
    StyleSheet, Text, View, Image, Alert, ActivityIndicator,
    TouchableOpacity, TextInput, ScrollView
} from 'react-native';
import { FormWithConstraints, FieldFeedbacks, FieldFeedback, Async as Async_, AsyncProps } from 'react-form-with-constraints';

import { Button, FormLabel, FormInput, FormValidationMessage } from 'react-native-elements'
import { TextField } from 'react-native-material-textfield';
import _sty from '../assets/css/Styles';
import { ElButton } from '../Components/Elements';
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay';

export default class Login extends React.Component {
    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);
        this.formRef = React.createRef();

        this.state = {
            email: ''
        };

        this.handleSubmit = this.handleSubmit.bind(this)
    }

    componentWillMount() {
        this.setState({ email: this.props.screenProps.email.toLowerCase().trim() });
    }

    handleSubmit() {
        //this.props.screenProps.sendPushNotification('title', 'body'); 
        const postData = {
            'email': this.state.email
        }

        this.props.screenProps.setSpinner(true);

        this.props.screenProps.login(postData)
            .then(response => response)
            .then(response => {
                const { customer } = response.data
                this.props.navigation.navigate('Password', {
                    customer: customer
                })

            })
            .catch((error) => {
                this.props.screenProps.setError(error);
            }).then(() => {
                this.props.screenProps.setSpinner(false);
            })
    }

    render() {
        let { email } = this.state;
        const { navigate } = this.props.navigation;

        return (
            <View style={_sty.bgGreen}>

                <Image style={[_sty.bgGreenImg, { width: '100%' }]} source={require('./../assets/images/bg-circle.png')} />
                <View style={[_sty.header2]}>
                    <Image style={{ width: 80, height: 117 }} source={require('./../assets/images/logo-white.png')} />
                </View>

                <View style={[styles.formWrapper]}>

                    <View style={[_sty.containerMdx, _sty.mb15, { height: '50%', position: 'relative' }]}>

                        <View style={_sty.formGroup}>
                            <TextField containerStyle={_sty.tfContainerStyle}
                                inputContainerStyle={_sty.tfInputContainerStyle} labelTextStyle={_sty.tfLabelTextStyle}
                                style={_sty.tfStyle} lineWidth={0} activeLineWidth={0}
                                label='Email Address' textColor={`#FFF`} tintColor={`#CCE9D5`} baseColor={`#CCE9D5`}
                                name="email" value={email} labelHeight={18}
                                onChangeText={(email) => this.setState({ email })}
                            />
                        </View>

                        {/* <TouchableOpacity onPress={this.handleSubmit}
                            style={[_sty.btn, _sty.alignSelfCenter,
                            _sty.width70, _sty.bgThemeLight, _sty.boxShadow]}>
                            <Text style={[_sty.btnText, _sty.textWhite]}>Login</Text>
                        </TouchableOpacity> */}

                        <ElButton
                            title="Log in"
                            onPress={this.handleSubmit}
                            shadow={true}
                            width={70}
                            bg="white"
                            fontWeight="bold"
                            textColor="black"
                            disabled={(email === '') ? true : false}
                        />


                    </View>




                    <Image style={[_sty.positionBottom, { width: '100%' }]} source={require('./../assets/images/curve-bottom-2.png')} />
                </View>

                <View style={{ height: '20%', alignItems: 'center', justifyContent: 'center', backgroundColor: '#fff' }}>
                    <TouchableOpacity onPress={() => navigate('Register')}>
                        <Text style={[_sty.fontSize14]}>Create an account?
                        <Text style={[_sty.fontWeightSemiBold, _sty.textTheme, _sty.fontSize18]}> Sign up</Text>
                        </Text>
                    </TouchableOpacity>
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    formWrapper: {
        width: '100%',
        height: '50%',
        position: 'relative',
        alignItems: 'center',
        justifyContent: 'center'
    },
    btnContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        height: '30%'
    },
    textTitle: {
        fontSize: 22,
        display: 'flex',
        textAlign: 'center',
        fontWeight: '700',
        marginBottom: 10,
        marginTop: 30,
        color: '#fff',
    },
    textMsg: {
        fontSize: 18,
        display: 'flex',
        textAlign: 'center',
        color: '#fff',
    }
});