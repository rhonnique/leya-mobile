import React from 'react';
import { StyleSheet, Text, View, Button, Image, TouchableOpacity } from 'react-native';
import _sty from '../assets/css/Styles';
import Swiper from 'react-native-swiper';

const styles = StyleSheet.create({
    btnWrapper: {
        backgroundColor: 'transparent',
        flexDirection: 'row',
        position: 'absolute',
        top: 0,
        left: 0,
        flex: 1,
        paddingHorizontal: 0,
        paddingVertical: 0,
        justifyContent: 'space-between',
        alignItems: 'flex-end'
    },
    nextBtn: {
        color: '#00A134',
        //paddingVertical: 20,
        fontSize: 18,
        fontWeight: 'bold'
    },
    dot: {
        backgroundColor: 'rgba(0,161,52,.3)',
        width: 10,
        height: 10,
        borderRadius: 6,
        marginLeft: 3,
        marginRight: 3,
        marginTop: 3,
        marginBottom: 3,
    },
    dotActive: {
        backgroundColor: '#fff',
        width: 14,
        height: 14,
        borderRadius: 8,
        borderWidth: 1,
        borderColor: '#00A134',
        marginLeft: 3,
        marginRight: 3,
        marginTop: 3,
        marginBottom: 3,
    },
    dotActiveInner: {
        backgroundColor: '#00A134',
        width: 12,
        height: 12,
        borderRadius: 8,
        borderWidth: 1,
        borderColor: '#ffffff'
    },
    skipWrapper: {
        zIndex: 2,
        position: 'absolute',
        right: 35,
        top: 0
    },
    skipText: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#00A134'
    },
    swipeWrapper: {
        zIndex: 2,
        position: 'absolute',
        left: 35,
        bottom: 0
    },
    swipeText: {
        color: '#999',
        fontSize: 14,
        fontWeight: 'bold'
    },
    beginWrapper: {
        zIndex: 2,
        position: 'absolute',
        right: 0,
        bottom: -12
    },
    slides: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    title: {
        color: '#333',
        fontWeight: 'bold',
        fontSize: 20,
        marginTop: 15,
        marginBottom: 5
    },
    msg: {
        color: '#666',
        fontSize: 16,
        textAlign: 'center'
    },
    slideImg:{
        //width: 333,
        height: 260,
        resizeMode: 'contain',
        marginBottom: 20
    }


})

const BtnNext = () => {
    return (
        <View style={{ paddingRight: 35 }}><Text style={styles.nextBtn}>Next</Text></View>
    );
}

const Dot = () => {
    return (
        <View style={styles.dot} />
    );
}

const DotActive = () => {
    return (
        <View style={styles.dotActive}>
            <View style={styles.dotActiveInner} />
        </View>
    );
}


const BtnDone = () => {
    if (this.state.done) {
        return (
            <TouchableOpacity style={[styles.beginWrapper, _sty.btn, _sty.btnTheme]}>
                <Text style={[_sty.btnText, _sty.textWhite]}>Begin</Text>
            </TouchableOpacity>
        );
    } else {
        return null;
    }
}

class Intro extends React.Component {
    static navigationOptions = {
        header: null
    };

    constructor() {
        super();
        this.state = {
            done: false
        }
    }

    handleIndexChange = (index) => {
        this.setState({
            done: index === 2 ? true : false
        })
    }




    render() {
        const { navigate } = this.props.navigation;

        const BtnDone = () => {
            if (this.state.done) {
                return (
                    <TouchableOpacity onPress={() => navigate('Register')}
                        style={[styles.beginWrapper, _sty.btn, _sty.btnTheme, {marginRight: 35, paddingHorizontal: 20}]}>
                        <Text style={[_sty.btnText, _sty.textWhite]}>Begin</Text>
                    </TouchableOpacity>
                );
            } else {
                return null;
            }
        }

        return (
            <View style={{ flex: 1, zIndex: 1, position: 'relative', marginHorizontal: 0, marginVertical: 40 }}>

                <TouchableOpacity onPress={() => navigate('Register')}
                    style={styles.skipWrapper}>
                    <Text style={styles.skipText}>Skip</Text>
                </TouchableOpacity>

                <View style={styles.swipeWrapper}>
                    <Text style={styles.swipeText}>Swipe left</Text>
                </View>

                <BtnDone />

                <Swiper
                    showsButtons={true}
                    loop={false}
                    dot={<Dot />}
                    activeDot={<DotActive />}
                    nextButton={<BtnNext />}
                    prevButton={<View></View>}
                    buttonWrapperStyle={styles.btnWrapper}
                    paginationStyle={{ bottom: 0 }}
                    onIndexChanged={this.handleIndexChange}

                >
                    <View style={[styles.slides, { overflow: 'hidden', paddingHorizontal: 35 }]}>
                        <Image style={[styles.slideImg]} source={require('./../assets/images/intro-1.png')} />
                        <Text style={styles.title}>Be a part of our 4:24 scheme</Text>
                        <Text style={styles.msg}>We offer a 4% flat per cycle on any investment within our single obligor limit. Cycle starts and ends on the 24th of every month.</Text>
                    </View>

                    <View style={[styles.slides, { overflow: 'hidden', paddingHorizontal: 35 }]}>
                        <Image style={[styles.slideImg]} source={require('./../assets/images/intro-2.png')} />
                        <Text style={styles.title}>Easy access to beans</Text>
                        <Text style={styles.msg}>We lend funds to salary earners and business owners in need of cash at a 10% flat rate anytime and from anywhere.</Text>
                    </View>

                    <View style={[styles.slides, { overflow: 'hidden', paddingHorizontal: 35 }]}>
                        <Image style={[styles.slideImg]} source={require('./../assets/images/intro-3.png')} />
                        <Text style={styles.title}>A bright future for your kids</Text>
                        <Text style={styles.msg}>We provide financial security to individuals, most often children, grandchildren, and organisations, such as charity and other non-profit organisations.</Text>
                    </View>

                </Swiper>
            </View>
        );
    }
}



export default Intro;