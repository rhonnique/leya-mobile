import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Linking,
  Image,
  ImageBackground,
  TouchableOpacity,
  TouchableHighlight,
  TextInput,
  ScrollView,
  Alert,
  KeyboardAvoidingView
} from "react-native";
import { TextField } from "react-native-material-textfield";
import _sty from "./../assets/css/Styles";
import { ElButton } from "../Components/Elements";

export default class Register extends React.Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);

    this.state = {
      name: "",
      phone: "",
      email: ""
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleEmail = this.handleEmail.bind(this);
    this.handlePhone = this.handlePhone.bind(this);
  }

  handleEmail(email) {
    //email = email.trim();
    email = email.toLowerCase().trim();
    this.setState({ email });
  }

  handlePhone(phone) {
    phone = phone.replace(/[^0-9]/g, "");
    this.setState({ phone });
  }

  handleSubmit() {
    const postData = {
      name: this.state.name,
      phone: this.state.phone,
      email: this.state.email
    };
    

    this.props.screenProps.setSpinner(true);

    this.props.screenProps
      .register(postData)
      .then(response => {
        return response;
      })
      .then(response => { 
        console.log(response.data,'register response...' );
        const { email } = response.data;
        this.props.navigation.navigate("Success", {
          title: "Congratulations",
          message: "Your registration is successful",
          email: email,
          goto: "Login"
        });
      })
      .catch(error => {
        console.log(JSON.stringify(error),'register error...' );
        this.props.screenProps.setError(error);
      })
      .then(() => {
        this.props.screenProps.setSpinner(false);
      });
  }

  render() {
    let { name, phone, email } = this.state;
    const { navigate } = this.props.navigation;
    const infoText = `Let’s get you all set up so you can verify your\naccount and begin investing.`;

    return (
      <ImageBackground
        source={require("./../assets/images/bg-circle.png")}
        style={[_sty.containerFlex, _sty.bgGreen]}
      >
        <KeyboardAvoidingView behavior="padding" enabled>
          <ScrollView>
            <View style={[styles.logoContainer]}>
              <Image
                style={[styles.logo]}
                source={require("./../assets/images/logo-white.png")}
              />
            </View>

            <View style={[_sty.container, _sty.formContainer]}>
              <View style={_sty.headingContainer}>
                <Text style={_sty.headingH1}>Create Account</Text>
                <Text style={_sty.headingText}>{infoText}</Text>
              </View>

              <View style={_sty.formGroup}>
                <TextField
                  containerStyle={_sty.tfContainerStyle2}
                  inputContainerStyle={_sty.tfInputContainerStyle}
                  labelTextStyle={_sty.tfLabelTextStyle}
                  style={_sty.tfStyle}
                  lineWidth={0}
                  activeLineWidth={0}
                  label="Full Name"
                  textColor={`#666`}
                  tintColor={`#666`}
                  baseColor={`#666`}
                  value={name}
                  labelHeight={18}
                  onChangeText={name => this.setState({ name })}
                />
              </View>

              <View style={_sty.formGroup}>
                <TextField
                  containerStyle={_sty.tfContainerStyle2}
                  inputContainerStyle={_sty.tfInputContainerStyle}
                  labelTextStyle={_sty.tfLabelTextStyle}
                  style={_sty.tfStyle}
                  lineWidth={0}
                  activeLineWidth={0}
                  label="Phone Number"
                  textColor={`#666`}
                  tintColor={`#666`}
                  baseColor={`#666`}
                  value={phone}
                  labelHeight={18}
                  keyboardType={'numeric'}
                  numeric
                  onChangeText={phone => this.handlePhone(phone)}
                />
              </View>

              <View style={_sty.formGroup}>
                <TextField
                  containerStyle={_sty.tfContainerStyle2}
                  inputContainerStyle={_sty.tfInputContainerStyle}
                  labelTextStyle={_sty.tfLabelTextStyle}
                  style={_sty.tfStyle}
                  lineWidth={0}
                  activeLineWidth={0}
                  label="Email Address"
                  textColor={`#666`}
                  tintColor={`#666`}
                  baseColor={`#666`}
                  value={email}
                  labelHeight={18}
                  autoCapitalize='none'
                  email-address
                  onChangeText={this.handleEmail}
                //onChangeText={email => this.setState({ email })}
                //onChangeText={email => this.setState({ email })}
                />
              </View>

              <View
                style={{
                  paddingTop: 5,
                  paddingBottom: 25,
                  fontSize: 12,
                  alignItems: "center"
                }}
              >
                <Text style={_sty.textDefault}>
                  By creating an account you agree to our
                </Text>
                <Text style={_sty.textGreen}

                >
                  <Text
                    onPress={() => { Linking.openURL(`${this.props.screenProps.api_url}/terms`) }}>
                      Terms of Service</Text>
                  {" "}<Text style={_sty.textDefault}> and </Text>{" "} 
                  <Text
                    onPress={() => { Linking.openURL(`${this.props.screenProps.api_url}/privacy-policy`) }}>
                      Privacy Policy</Text>

                </Text>
              </View>

              <ElButton 
                title="Proceed"
                onPress={this.handleSubmit}
                shadow={true}
                width={70}
                bg="#00A134"
                textColor="white"
                fontWeight="bold"
                disabled={
                  name === "" || email === "" || phone === "" ? true : false
                }
              />

              <View style={{ marginTop: 15,  alignItems: 'center', justifyContent: 'center', backgroundColor: '#fff' }}>
                <TouchableOpacity onPress={() => navigate('Login')}>
                  <Text style={[_sty.fontSize14]}>Already have an account?
                        <Text style={[_sty.fontWeightSemiBold, _sty.textTheme, _sty.fontSize18]}> Log in</Text>
                  </Text>
                </TouchableOpacity>
              </View>

            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  logoContainer: {
    //height: 180,
    alignItems: "center",
    justifyContent: "center",
    //backgroundColor:'red',
    paddingVertical: 30
  },
  logo: {
    width: 70,
    // Without height undefined it won't work
    height: 100,
    marginTop: 15

    // figure out your image aspect ratio
    //aspectRatio: 135 / 76,
  },
  msgContainer: {
    height: "70%",
    color: "#fff",
    alignItems: "center",
    justifyContent: "center"
  },
  btnContainer: {
    alignItems: "center",
    justifyContent: "center",
    height: "30%"
  },
  textTitle: {
    fontSize: 22,
    display: "flex",
    textAlign: "center",
    fontWeight: "700",
    marginBottom: 10,
    marginTop: 30,
    color: "#fff"
  },
  textMsg: {
    fontSize: 18,
    display: "flex",
    textAlign: "center",
    color: "#fff"
  }
});
