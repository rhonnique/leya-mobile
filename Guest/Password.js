import React, { Fragment } from 'react';
import {
    StyleSheet, Text, View, Button, Image, Alert,
    TouchableOpacity, TextInput, ScrollView, AsyncStorage
} from 'react-native';
import VirtualKeyboard from 'react-native-virtual-keyboard';
//import { LinearGradient } from "expo";
import { LinearGradient } from 'expo-linear-gradient';
import _sty from '../assets/css/Styles';

const ForgotLink = () => {
    return (<TouchableOpacity>
        <Text>Forgot?</Text>
    </TouchableOpacity>)
}

export default class Login extends React.Component {
    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);
        this.state = {
            password: '',
            passDot: [],
            title: '',
            email: '',
            status: 0
        };

        this.newLogin = this.newLogin.bind(this);
        this.login = this.login.bind(this);
    }

    componentWillMount() {
        const { navigation } = this.props;
        const customer = navigation.getParam('customer', {});
        //console.log(customer, 'customer.....')
        const { email, status } = customer;
        const title = parseInt(status) === 2 ? 'Create a passcode' : 'Password required';
        this.setState({ email, status: parseInt(status) })
        this.setState({ title })
    }

    changeText(newText) {
        if(this.state.password.length === 5){
            newText = newText.slice(0, -1);
        }

        this.setState({ password: newText });
        this.setState({ passDot: newText.split('') });

        console.log(newText.length, 'newText.length.....')
        if (newText.length === 5) {
            if (this.state.status === 1) {
                this.login(newText);
            } else if (this.state.status === 2) {
                this.newLogin(newText);
            } else {
                Alert.alert('Can not login customer, please retry\n(ERR_099)');
            }
        } else {
            return;
        }
    }


    newLogin(password) {
        const postData = {
            'email': this.state.email,
            'password': password,
        }

        this.props.screenProps.setSpinner(true);

        this.props.screenProps.registerPassword(postData)
            .then(response => response)
            .then(response => {
                //const { customer } = response.data;
                this.setSession(response.data.data);
            })
            .catch((error) => {
                this.props.screenProps.setError(error);
            }).then(() => {
                this.props.screenProps.setSpinner(false);
                /* this.setState({
                    password: '',
                    passDot: ''
                }) */
            })
    }

    login(password) {
        const postData = {
            'email': this.state.email,
            'password': password,
        }

        this.props.screenProps.setSpinner(true);

        this.props.screenProps.loginPassword(postData)
            .then(response => response)
            .then(response => {
                //const { customer } = response.data;

                this.setSession(response.data.data);
            })
            .catch((error) => {
                this.props.screenProps.setError(error, [401]);
            }).then(() => {
                this.props.screenProps.setSpinner(false);
                /* this.setState({
                    password: '',
                    passDot: ''
                }) */
            })
    }

    setSession(data) {
        const { token, customer } = data;
        console.log(token + '______' + customer, 'setSession::customer.data....')
        this.props.screenProps.setLogin(data)
    }



    render() {
        const { passDot } = this.state;

        return (
            <View style={styles.container}>
                <LinearGradient colors={['#ffffff', 'transparent']}
                    style={styles.LinearGradient}>
                    <View style={[styles.keyboardContainer]}>
                        <Text style={[_sty.textAlignCenter]}>{this.state.title}</Text>
                        <View style={[styles.keyboardContentWrapper]}>
                            <View style={[styles.keyboardContentRow, { position: 'relative' }]}>
                                <Image source={require('./../assets/images/password.png')} />
                                <Image source={require('./../assets/images/password.png')} />
                                <Image source={require('./../assets/images/password.png')} />
                                <Image source={require('./../assets/images/password.png')} />
                                <Image source={require('./../assets/images/password.png')} />
                            </View>
                            <View style={[styles.keyboardContentRow, { position: 'absolute' }]}>
                                {passDot.length > 0 && passDot.map((char, index) => {
                                    return (<Image key={index} source={require('./../assets/images/password-tint.png')} />)
                                })}
                            </View>
                        </View>

                        <VirtualKeyboard
                        style={{width: 300, justifyContent:'center', alignSelf:'center', alignItems: 'center'}}
                            action={<ForgotLink />}
                            cellStyle={[styles.keyCell]}
                            color='black'
                            pressMode='string'
                            onPress={(val) => this.changeText(val)}
                        />
                    </View>

                </LinearGradient>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'rgba(153,243,182,0.65)'
    },
    LinearGradient: {
        position: 'absolute', left: 0, right: 0, top: 0, height: '100%', flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    keyboardContainer: {
        alignSelf: 'center',
        width: '100%',
    },
    keyboardContentWrapper: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        height: 30,
        position: 'relative',
        marginTop: 20
    },
    keyboardContentRow: {
        width: 150,
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },

    keyCell: {
        width: 70,
        height: 70,
        borderRadius: 70 / 2,
        backgroundColor: '#fff',
        elevation: 5,
        marginHorizontal: 14
    },
    keyCellClear: {
        backgroundColor: 'transparent',
        elevation: 0
    },





    textTitle: {
        fontSize: 22,
        display: 'flex',
        textAlign: 'center',
        fontWeight: '700',
        marginBottom: 10,
        marginTop: 30,
        color: '#fff',
    },
    textMsg: {
        fontSize: 18,
        display: 'flex',
        textAlign: 'center',
        color: '#fff',
    }
});