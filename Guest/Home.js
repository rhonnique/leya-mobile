import React from 'react';
import { StyleSheet, Text, View, Image, Button, TouchableHighlight, TouchableOpacity } from 'react-native';
import _sty from '../assets/css/Styles';
import BootstrapStyleSheet from 'react-native-bootstrap-styles';
//import { Button } from 'reactstrap';


class Home extends React.Component {
    static navigationOptions = {
        title: 'Home',
        header: null
    };

    render() {
        const { navigate } = this.props.navigation;
        const infoTextString = 'Investment, Loan and Trust Fund\nopportunities for Everyone.';
        return (
            <View style={{ flex: 1 }}>
                <View style={styles.logoContainer} >
                    <Image style={{ width: 100, height: 143 }} source={require('./../assets/images/logo.png')} />
                    <Image style={[_sty.positionBottom, {width:'100%'}]} source={require('./../assets/images/curve-bottom.png')} />  
                </View>


                <View style={styles.infoContainer} >
                    <View style={[_sty.containerMdx, _sty.containerFlex, _sty.containerAlignCC]} >
                        <Text style={styles.infoText}>{infoTextString}</Text>

                        <TouchableOpacity onPress={() => navigate('Intro')}
                            style={[_sty.btn, _sty.btnBlock, _sty.btnThemeOrange, _sty.mb15]}>
                            <Text style={[_sty.btnText, _sty.fontBold]}>Get Started</Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => navigate('Login')}
                            style={[_sty.btn, _sty.btnBlock]}>
                            <Text style={[_sty.btnText, _sty.fontBold]}>Log In</Text>
                        </TouchableOpacity>
                    </View>
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    logoContainer: {
        position: 'relative',
        height: '65%',
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center'

    },
    infoContainer: {
        height: '35%',
        backgroundColor: '#00A134'
    },
    infoText: {
        fontSize: 19, 
        color: '#ffffff',
        display: 'flex',
        textAlign: 'center',
        fontWeight: "400",
        marginBottom: 20,
        textShadowColor: "#dddddd"
    }
});

export default Home;