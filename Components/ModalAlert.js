import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Text, View, StyleSheet, StatusBar, Image, ViewPropTypes, Modal,
  SafeAreaView, Switch, TouchableOpacity, TouchableHighlight
} from 'react-native';
import { Feather, Ionicons } from "react-native-vector-icons";
//import { Icon as Ion } from "react-native-vector-icons/Ionicons";
import _sty from '../assets/css/Styles';
import _sty_modal from '../assets/css/Modal';

class ModalAlert extends Component {

  static propTypes = {
    visible: PropTypes.bool,
    onClosePress: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired,
    message: PropTypes.string.isRequired
  }

  static defaultProps = {
    visible: false,
  }

  constructor(props) {
    super(props);
    this.state = {
      visible: this.props.visible,
    };

    this.closeModal = this.closeModal.bind(this)
  }

  componentDidUpdate(prevProps) {
    if (prevProps.visible !== this.props.visible) {
      this.setState({ visible: this.props.visible })
    }
  }

  closeModal() {
    this.setState({ visible: false })
  }

  onClosePress() {
    this.props.onClosePress(false);
  }

  render() {
    return (

      <Modal
        presentationStyle={`overFullScreen`}
        animationType="slide"
        transparent={true}
        visible={this.state.visible}
        onRequestClose={() => { }}
      >
        <View style={[_sty_modal.overlay]}>
          <View style={[_sty_modal.container]}>

            <View style={[_sty_modal.header]}>
              <TouchableOpacity style={[_sty_modal.headerClose]} onPress={() => { this.onClosePress() }}>
                <Ionicons  name="ios-close" size={30} color='#999' />
              </TouchableOpacity>
              <Feather name="alert-circle" size={30} color='red' />
              <Text style={[_sty.py10, _sty.fontSize18, _sty.textBlack, _sty.fontWeightSemiBold]}>{this.props.title}</Text>
            </View>

            <View style={[_sty.py5, _sty.flex]}>

            <Text style={[_sty.fontSize15]}>{this.props.message}</Text>

              <View style={[_sty.vBottom]}>
                <TouchableOpacity
                  //onPress={this.closeModal}
                  onPress={() => { this.onClosePress() }}
                  style={[_sty.btn, _sty.alignSelfCenter, _sty.bgDanger, _sty.btnBlock]}>
                  <Text style={[_sty.btnText, _sty.textWhite]}>Cancel</Text>
                </TouchableOpacity>
              </View>

            </View>

          </View>
        </View>
      </Modal>

    );
  }

}

function CloseBtn() {
  return (<TouchableOpacity
    //onPress={this.closeModal}
    style={[_sty.btn, _sty.alignSelfCenter, _sty.bgDanger, _sty.btnBlock]}>
    <Text style={[_sty.btnText, _sty.textWhite]}>Ok</Text>
  </TouchableOpacity>)
}

export { ModalAlert, CloseBtn }
