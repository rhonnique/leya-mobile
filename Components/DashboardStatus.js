import React from "react";
import {
  Text,
  View,
  StyleSheet,
  StatusBar,
  Image,
  SafeAreaView,
  Switch,
  TouchableOpacity,
  TouchableHighlight
} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import _sty from "../assets/css/Styles";
import { ElBadge } from "../Components/Elements";

const DashboardStatus = ({ data, interest_rate, type }) => {
  
  return (
    <View style={{ flexDirection: "row" }}>
      <View style={{ width: "70%" }}>
        <Text
          style={{
            textAlign: "left",
            fontSize: 18,
            color: "#fff",
            fontWeight: "bold"
          }}
        >
          {data.name}
        </Text>

        <View style={[_sty.width100, { marginBottom: 3, height: 30 }]}>
          {parseInt(data.status) === 1 ? (
            <ElBadge title="Active" bg="#D1F0DB" textColor="#00A134" />
          ) : (
            <ElBadge title="Inactive" bg="#FEDEDD" textColor="#F85857" />
          )} 
        </View>

        <Text
          style={{
            textAlign: "left",
            color: "#D1FFE0",
            justifyContent: "flex-end"
          }}
        >
          Account Officer:
          {data.account_officer ? " " + data.account_officer : " None"}
        </Text>
      </View>

      <View style={{ width: "30%" }}>
        <Text style={{ textAlign: "right", fontSize: 16, color: "#D1FFE0" }}>
          Interest Rate
        </Text>
        <Text style={{ textAlign: "right", fontSize: 25, color: "#D1FFE0", fontWeight: '600' }}>
          {interest_rate}%
        </Text>
        {type === 3 && interest_rate > 0 && (
          <Text style={{ textAlign: "right", fontSize: 12, color: "#D1FFE0" }}>
            Per Annum
          </Text>
        )}
      </View>
    </View>
  );
};

export default DashboardStatus;
