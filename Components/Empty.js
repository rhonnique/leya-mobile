import React from "react";
import {
  Text,
  View,
  StyleSheet,
  StatusBar,
  Image,
  SafeAreaView,
  Switch,
  TouchableOpacity,
  TouchableHighlight,
  Linking
} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import _sty from "../assets/css/Styles";
import { Button } from "react-native-elements";
import { ElButton } from "../Components/Elements";

const Empty = ({ title, msg, icon = {}, button = null }) => {
  return (
    <View style={_sty.containerEmpty}>
      <Icon
        name={icon.name ? icon.name : `ios-information-circle-outline`}
        size={icon.size ? icon.size : 40}
        color={icon.color ? icon.color : `#666`}
      />
      <Text style={_sty.emptyTitle}>{title ? title : "No Data"}</Text>
      <Text style={_sty.emptyMsg}>{msg ? msg : "No Result Found"}</Text>
      {/* <Button title="Call" /> */}
      {button && (<ElButton
        title="Call"
        //onPress={this.handleSubmit}
        containerStyle={{marginTop: 25}}
        titleStyle={{paddingLeft: 5, fontSize: 18}}
        shadow={true}
        width={100}
        bg="#00A134"
        textColor="white"
        onPress={()=> Linking.openURL(`tel:+2349060003039`)}
        icon={ 
          <Icon
            name="md-call"
            size={25}
            color="white"
          />
        }
      />)}
      
    </View>
  );
};

export default Empty;
