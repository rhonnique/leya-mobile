import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Text, View, StyleSheet, StatusBar, Image, ViewPropTypes,
  SafeAreaView, Switch, TouchableOpacity, TouchableHighlight
} from 'react-native';
import Icon from "react-native-vector-icons/Ionicons";
import _sty from '../assets/css/Styles';



/* const Alerts = props => {
  return (<View style={[_sty.alert, props.alertType, props.alertFixed]}>
    <Text style={_sty.alertBadge}>Hey!</Text>
    <Text style={_sty.alertText}>Ensure your withdrawal amount doesn't exceed your portfolio.</Text>
  </View>)
} */

class Alerts extends Component {

  static propTypes = {

    /* onPress: PropTypes.func.isRequired,
    backspaceImg: PropTypes.number,
    applyBackspaceTint: PropTypes.bool,
    decimal: PropTypes.bool,
    max: PropTypes.number, */

    text: PropTypes.string.isRequired,
    alertType: ViewPropTypes.style,
    alertFixed: PropTypes.bool,

  }

  static defaultProps = {
    alertFixed: true,
    alertType: _sty.alertWarning,
  }

  constructor(props) {
    super(props);
    this.state = {
      text: '',
    };
  }

  render() {
    return (
      <View style={[_sty.alert, this.props.alertType, this.props.alertFixed && _sty.alertFixed]}>
        <Text style={_sty.alertBadge}>Hey!</Text>
        <Text style={_sty.alertText}>{this.props.text}</Text>
      </View>
    );
  }

}

export default Alerts;
