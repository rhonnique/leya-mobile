import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Text, View, StyleSheet, StatusBar, Image, ViewPropTypes,
  SafeAreaView, Switch, TouchableOpacity, TouchableHighlight
} from 'react-native';
import Icon from "react-native-vector-icons/Ionicons";
import _sty from '../assets/css/Styles';



/* const Alerts = props => {
  return (<View style={[_sty.alert, props.alertType, props.alertFixed]}>
    <Text style={_sty.alertBadge}>Hey!</Text>
    <Text style={_sty.alertText}>Ensure your withdrawal amount doesn't exceed your portfolio.</Text>
  </View>)
} */

class ModalDefault extends Component {

  static propTypes = {

    /* onPress: PropTypes.func.isRequired,
    backspaceImg: PropTypes.number,
    applyBackspaceTint: PropTypes.bool,
    decimal: PropTypes.bool,
    max: PropTypes.number, */

    text: PropTypes.string.isRequired,
    alertType: ViewPropTypes.style,
    alertFixed: PropTypes.bool,

  }

  static defaultProps = {
    alertFixed: true,
    alertType: _sty.alertWarning,
  }

  constructor(props) {
    super(props);
    this.state = {
      text: '',
    };
  }

  render() {
    return (
      
      <Modal
        presentationStyle={`overFullScreen`}
        animationType="slide"
        transparent={true}
        visible={this.state.modalVisible}
        onRequestClose={() => { }}
      >
        <View style={{ backgroundColor: 'rgba(0,0,0,.5)', flex: 1, justifyContent: 'flex-end' }}>
          <View
            style={{ backgroundColor: '#fff', minHeight: 350, maxHeight: '100%', margin: 20, borderRadius: 7 }}>



            <View style={[_sty.rowSpace, { paddingHorizontal: 15, paddingVertical: 10 }]}>
              <Text
                style={[_sty.py5, _sty.fontSize16,
                _sty.textBlack, _sty.fontWeightSemiBold]}>Let's go over your request</Text>

              {this.state.tenure === 1 && (<TouchableOpacity onPress={() => this.setModalVisible(!this.state.modalVisible)}>
                <Icon name="ios-close" size={30} color='#999' />
              </TouchableOpacity>)}

            </View>
            <View style={[_sty.rowPx, _sty.py15, _sty.flex]}>


              <Card
                containerStyle={{ backgroundColor: 'rgba(0,161,52, .1)', elevation: 0, borderWidth: 0, borderRadius: 5, overflow: "hidden", paddingVertical: 0, margin: 0 }} >

                <View
                  style={[_sty.rowSpace, { borderBottomWidth: 1, borderBottomColor: '#C5E9CF', paddingVertical: 13 }]}>
                  <Text style={[_sty.textTheme]}>Proposed Amount</Text>
                  <Text style={[_sty.textTheme]}><Cf value={parseFloat(this.state.amount)} /></Text>
                </View>

                <View
                  style={[_sty.rowSpace, { borderBottomWidth: 1, borderBottomColor: '#C5E9CF', paddingVertical: 13 }]}>
                  <Text style={[_sty.textTheme]}>Interest Due</Text>
                  <Text style={[_sty.textTheme]}><Cf value={parseFloat(this.state.interest_amount)} /></Text>
                </View>

                <View
                  style={[_sty.rowSpace, { borderBottomWidth: 0, borderBottomColor: '#C5E9CF', paddingVertical: 13 }]}>
                  <Text style={[_sty.textTheme]}>Mature Date</Text>
                  <Text style={[_sty.textTheme]}>{this.state.tenure === 0 ? 'Indefinite' : this.state.month+', '+this.state.year}</Text>
                </View>

              </Card>


              <View style={[_sty.vBottom]}>
                <View style={[_sty.rowSpace]}>
                  <TouchableOpacity onPress={this.handleSubmit}
                    style={[_sty.btn, _sty.alignSelfCenter, _sty.bgTheme]}>
                    <Text style={[_sty.btnText, _sty.textWhite]}>Accept</Text>
                  </TouchableOpacity>

                  <TouchableOpacity /* onPress={this.state.tenure === 0 ? this.goBack() : this.setModalVisible(!this.state.modalVisible)}  */
                    onPress={this.closeModal}
                    style={[_sty.btn, _sty.alignSelfCenter, _sty.bgDanger]}>
                    <Text style={[_sty.btnText, _sty.textWhite]}>Cancel</Text>
                  </TouchableOpacity>
                </View>
              </View>

            </View>

          </View>
        </View>
      </Modal>

    );
  }

}

export default ModalDefault;
