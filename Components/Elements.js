import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { Button, Card, Badge } from 'react-native-elements'
import _sty from './../assets/css/Styles';
import { bold } from 'ansi-colors';


class ElBadge extends Component {

    static propTypes = {
        title: PropTypes.string.isRequired,
        status: PropTypes.string,
        textColor: PropTypes.string,
        fontWeight: PropTypes.string,
        bg: PropTypes.string,
    }

    static defaultProps = {
        visible: false,
        status: 'warning',
        textColor: 'black',
        fontWeight: 'normal',
    }

    render() {
        return (
            <Badge value={this.props.title} status={this.props.status}
                containerStyle={{ alignSelf: 'flex-start', marginTop: 5 }}
                badgeStyle={[styles.badgeStyle, (this.props.bg && { backgroundColor: this.props.bg })]}
                textStyle={[{ fontWeight: this.props.fontWeight, color: this.props.textColor, lineHeight: 16 }]}
                {...this.props}
            />
        );
    }

}

class ElButton extends Component {

    static propTypes = {
        title: PropTypes.string.isRequired,
        width: PropTypes.number,
        shadow: PropTypes.bool,

        textColor: PropTypes.string,
        bg: PropTypes.string,
    }

    static defaultProps = {
        width: 100,
        textColor: 'black'
    }

    render() {
        return (
            <Button
                title={this.props.title}
                buttonStyle={[{ height: 45, borderRadius: 5 }, (this.props.bg && { backgroundColor: this.props.bg })]}
                containerStyle={[_sty.alignSelfCenter, (this.props.shadow && _sty.boxShadow), (this.props.width && { width: `${this.props.width}%` })]}
                titleStyle={[{ fontSize: 18, fontWeight: 'bold' }, (this.props.textColor && { color: this.props.textColor })]}
                {...this.props}
            />
        );
    }

}

const styles = StyleSheet.create({
    badgeStyle: {
        borderRadius: 5,
        paddingHorizontal: 2,
        height: 24
    }
})


export { ElBadge, ElButton }
