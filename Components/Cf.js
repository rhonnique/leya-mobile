import React from 'react';
import { Text, View } from 'react-native';
import NumberFormat from 'react-number-format';
import _sty from './../assets/css/Styles';

function Cf(props) {
    
    return (
        <NumberFormat value={props.value} decimalScale={2} fixedDecimalScale={true}
        displayType={'text'} thousandSeparator={true}
        renderText={value => <Text>&#8358;{value}</Text> } />
    )
}

export default Cf;
 