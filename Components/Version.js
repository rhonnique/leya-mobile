import React from 'react';
import { Text, View } from 'react-native';
import _sty from '../assets/css/Styles';

export default class Version extends React.Component {

    render() {
        return (<View style={_sty.vBottom}>
            <Text style={_sty.vBottomText}>Version 1.0.0.12345</Text>
        </View>);
    }
}