export default {
  header: {
    borderBottomColor: '#f1f1f1',
    borderBottomWidth:2,
    height: 200,
    alignItems: 'center',
    justifyContent: 'center'
  },
  avatar:{
    width:104,
    height: 104,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.2,
    shadowRadius: 4,
    elevation: 5,
    alignItems: 'center',
    justifyContent: 'center',

    borderRadius:50,

    backgroundColor: '#fff'
  },
  profileStatus:{
    padding: 20,
    borderBottomColor: '#f1f1f1',
    borderBottomWidth:2,
  },

  profileList:{
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 15,
    paddingBottom: 15,
    flex: 1, 
    flexDirection: 'row'
  },
  profileListText:{
    fontWeight:'500',
    display:'flex',
    fontSize:14
  },
  profileListImg:{
    marginRight:210
  },

  footerContainer: {
    padding: 20,
    backgroundColor: '#fff',
  },




  container: {
    paddingTop: 20,
    flex: 1
  },
  navItemStyle: {
    padding: 10
  },
  navSectionStyle: {
    backgroundColor: 'lightgrey'
  },
  sectionHeadingStyle: {
    paddingVertical: 10,
    paddingHorizontal: 5
  },
  
};