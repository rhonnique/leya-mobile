import React, { Component } from 'react';
import {
  Text, View, StyleSheet, StatusBar, Image,
  SafeAreaView, Switch, TouchableOpacity, TouchableHighlight
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import S from './Transactions.style';
import Icon from "react-native-vector-icons/Ionicons";
import _sty from "../../assets/css/Styles";


class Transactions extends Component {
  static navigationOptions = ({ navigation }) => ({
    drawerLockMode: "locked-closed",
    title: 'TRANSACTION HISTORY',
    headerTintColor: '#666',
    headerTitleStyle: {
      fontSize: 15
    },
    headerStyle: {
      elevation: 0,
      shadowOpacity: 0,
      backgroundColor: '#F7F7F7',
      borderBottomWidth: 1,
      borderBottomColor: '#D7D7D7'
    }
  })

  componentDidMount() {
    // StatusBar.setBackgroundColor('red', true)
  }

  componentWillUnmount() {
    //this._navListener.remove();   <Switch value={true} />
  }

  navigateToScreen = (route) => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <SafeAreaView style={styles.container}>

        <TouchableOpacity style={_sty.listView} onPress={() => navigate('TransInvestment')}>
          <Text style={_sty.listViewText}>Investment</Text>
          <Icon style={_sty.listViewIcon} name="ios-arrow-forward" size={15} color='#666' />
        </TouchableOpacity>

        <TouchableOpacity style={_sty.listView} onPress={() => navigate('TransLoans')}>
          <Text style={_sty.listViewText}>Loans</Text>
          <Icon style={_sty.listViewIcon} name="ios-arrow-forward" size={15} color='#666' />
        </TouchableOpacity>

        <TouchableOpacity style={_sty.listView} onPress={() => navigate('TransTrustFunds')}>
          <Text style={_sty.listViewText}>Trust Funds</Text>
          <Icon style={_sty.listViewIcon} name="ios-arrow-forward" size={15} color='#666' />
        </TouchableOpacity>


      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f1f1f1',
    paddingTop: 40
}
});

export default Transactions;