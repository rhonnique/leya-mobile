import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  RefreshControl
} from "react-native";
import S from "./Transactions.style";
import _sty from "../../assets/css/Styles";
import Icon from "react-native-vector-icons/Ionicons";
import Empty from "./../../Components/Empty";
import { Button, Badge } from "react-native-elements";
import moment from "moment";

class TransLoans extends Component {
  static navigationOptions = ({ navigation }) => ({
    drawerLockMode: "locked-closed",
    title: "LOANS HISTORY",
    headerTintColor: "#666",
    headerTitleStyle: {
      fontSize: 15
    },
    headerStyle: {
      elevation: 0,
      shadowOpacity: 0,
      backgroundColor: "#F7F7F7",
      borderBottomWidth: 1,
      borderBottomColor: "#D7D7D7"
    }
  });

  constructor(props) {
    super(props);
    this.state = {
      data: null,
      dataLoader: { loading: false, error: false, rd: false }
    };

    this.fetchData = this.fetchData.bind(this);
  }

  componentWillMount() {
    this.fetchData();
  }

  fetchData() {
    var dataLoader = { loading: true, error: false, rd: false };
    this.setState({ dataLoader });

    this.props.screenProps
      .loanGet()
      .then(response => response)
      .then(response => {
        console.log(JSON.stringify(response.data), "loan history....");
        var dataLoader = { loading: false, error: false, rd: true };
        this.setState({ dataLoader, data: response.data });
      })
      .catch(error => {
        var dataLoader = { loading: false, error: true, rd: false };
        this.setState({ dataLoader });
        this.props.screenProps.handleAxiosError(error);
        console.log(JSON.stringify(error), "loan history error ....");
      });
  }

  render() {
    const { dataLoader, data } = this.state;
    return (
      <SafeAreaView
        style={[_sty.containerDefault, _sty.containerWrapperDefault]}
      >
        {/* ****** If loading ****** */}
        {dataLoader.loading && (
          <View style={[_sty.containerAlignCC, { flex: 1 }]}>
            <Text>Loading ...</Text>
          </View>
        )}

        {/* ****** If an error occured ****** */}
        {dataLoader.error && (
          <View
            style={[_sty.containerAlignCC, { paddingHorizontal: 50, flex: 1 }]}
          >
            <Icon name="ios-alert" size={30} color="#959A97" />
            <Text style={{ fontSize: 18, color: "#959A97", fontWeight: "500" }}>
              An error occurred, please retry
            </Text>
            <Text
              style={{
                fontSize: 14,
                color: "#999",
                textAlign: "center",
                marginVertical: 10
              }}
            >
              {`The system can not fetch data, please click refresh`}
            </Text>

            <Button
              title="Reload Data"
              type="clear"
              onPress={this.fetchData}
              titleStyle={[_sty.textDefault, _sty.fontSize14]}
            />
          </View>
        )}

        {/* ****** If no data found ****** */}
        {dataLoader.rd && data.data && data.data.length === 0 && (
          <Empty
            title={"No Loans"}
            msg={"There is no transactions so far."}
          />
        )}

        {dataLoader.rd &&
          data.data !== null &&
          Object.entries(data.data).length > 0 && (
            <ScrollView
              refreshControl={<RefreshControl onRefresh={this.fetchData} />}
            >
              {data.data.map((row, index) => {
                let type = "";
                let trans_source = "";
                let amount_label = "";
                let amount = (row.amount / 1).toFixed(2);
                amount = amount
                  .toString()
                  .replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                switch (row.payment_type) {
                  case 1:
                    type = "Loan Disburstment";
                    amount_label = (
                      <Badge
                        value={`+${amount}`}
                        badgeStyle={[S.badgeD, S.bSuccess]}
                        textStyle={S.bSuccessText}
                      />
                    );
                    break;
                  case 2:
                    type = "Loan Repayment";
                    amount_label = (
                      <Badge
                        value={`-${amount}`}
                        badgeStyle={[S.badgeD, S.bDanger]}
                        textStyle={S.bDangerText}
                      />
                    );
                    break;
                }

                /* switch (row.trans_source) {
                  case 1:
                      trans_source = "Fund Transfer";
                    break;
                  case 2:
                      trans_source = "Debit Card";
                    break;
                  default:
                      trans_source = "";
                    break;
                } */

                return (
                  <TouchableOpacity key={index} style={S.listItem}>
                    <View style={S.listBadgeBox}>
                      <Badge status="success" />
                    </View>

                    <View style={S.listTextBox}>
                      <Text style={S.listTextM}>{type}</Text>
                      <Text style={S.listTextS}></Text>
                    </View>

                    <View style={S.listAmountBox}>
                      {amount_label}
                      <Text
                        style={[
                          S.listTextS,
                          _sty.textRight,
                          { paddingRight: 5 }
                        ]}
                      >
                        {moment(row.trans_date).format('MMM DD, YYYY')}
                      </Text>
                    </View>
                  </TouchableOpacity>
                );
              })}
            </ScrollView>
          )}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});

export default TransLoans;
