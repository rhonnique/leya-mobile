import React, { Component } from 'react';
import {
  Text, View, StyleSheet, StatusBar, Image, ScrollView,
  SafeAreaView, Switch, TouchableOpacity, TouchableHighlight
} from 'react-native';
import { List, ListItem } from 'react-native-elements'
import { Avatar, Badge, Icon, withBadge } from 'react-native-elements'
import _sty  from '../../assets/css/Styles';
import S from './Transactions.style';


class TransTrustFund extends Component {
  static navigationOptions = ({ navigation }) => ({
    drawerLockMode: "locked-closed",
    title: 'TRUST FUND HISTORY',
    headerTintColor: '#666',
    headerTitleStyle: {
      fontSize: 15
    },
    headerStyle: {
      elevation: 0,
      shadowOpacity: 0,
      backgroundColor: '#F7F7F7',
      borderBottomWidth: 1,
      borderBottomColor: '#D7D7D7'
    }
  })


  render() {
    return (
      <SafeAreaView style={[_sty.containerDefault, _sty.containerWrapperDefault]}>
        <ScrollView>

          <TouchableOpacity style={S.listItem}>

            <View style={S.listBadgeBox} >
              <Badge status="success" />
            </View>

            <View style={S.listTextBox}>
              <Text style={S.listTextM}>Retail Collection</Text>
              <Text style={S.listTextS}>Card **5490</Text>
            </View>

            <View style={S.listAmountBox}>
              <Badge value="+100,000,000.00" badgeStyle={[S.badgeD, S.bSuccess]} textStyle={S.bSuccessText} />
              <Text style={[S.listTextS, _sty.textRight, { paddingRight: 5 }]}>Today, 11:00 AM</Text>
            </View>

          </TouchableOpacity>


          <View style={S.listItem}>
            <View style={S.listBadgeBox} >
              <Badge status="error" />
            </View>

            <View style={S.listTextBox}>
              <Text style={S.listTextM}>Retail Preliquidation</Text>
              <Text style={S.listTextS}>Transfered to 0260835140</Text>
            </View>

            <View style={S.listAmountBox}>
              <Badge value="-40,000,000.00" badgeStyle={[S.badgeD, S.bDanger]} textStyle={S.bDangerText} />
              <Text style={[S.listTextS, _sty.textRight, { paddingRight: 5 }]}>22-4-18, 12:52 PM</Text>
            </View>
          </View>

          <View style={S.listItem}>
            <View style={S.listBadgeBox} >
              <Badge status="success" />
            </View>

            <View style={S.listTextBox}>
              <Text style={S.listTextM}>Retail Top-up</Text>
              <Text style={S.listTextS}>Card **5490</Text>
            </View>

            <View style={S.listAmountBox}>
              <Badge value="+100,000" badgeStyle={[S.badgeD, S.bSuccess]} textStyle={S.bSuccessText} />
              <Text style={[S.listTextS, _sty.textRight, { paddingRight: 5 }]}>02-1-18, 11:00 AM</Text>
            </View>
          </View>

          <View style={S.listItem}>
            <View style={S.listBadgeBox} >
              <Badge status="error" />
            </View>

            <View style={S.listTextBox}>
              <Text style={S.listTextM}>Retail Interest repayment</Text>
              <Text style={S.listTextS}>Transfered to 0260835140</Text>
            </View>

            <View style={S.listAmountBox}>
              <Badge value="-40,000.00" badgeStyle={[S.badgeD, S.bDanger]} textStyle={S.bDangerText} />
              <Text style={[S.listTextS, _sty.textRight, { paddingRight: 5 }]}>22-4-18, 12:52 PM</Text>
            </View>
          </View>


          <View style={S.listItem}>
            <View style={S.listBadgeBox} >
              <Badge status="success" />
            </View>

            <View style={S.listTextBox}>
              <Text style={S.listTextM}>Reatail Collected</Text>
              <Text style={S.listTextS}>Card **5490</Text>
            </View>

            <View style={S.listAmountBox}>
              <Badge value="+100,000" badgeStyle={[S.badgeD, S.bSuccess]} textStyle={S.bSuccessText} />
              <Text style={[S.listTextS, _sty.textRight, { paddingRight: 5 }]}>02-1-18, 11:00 AM</Text>
            </View>
          </View>


          <View style={S.listItem}>
            <View style={S.listBadgeBox} >
              <Badge status="error" />
            </View>

            <View style={S.listTextBox}>
              <Text style={S.listTextM}>Retail Processing Fee</Text>
              <Text style={S.listTextS}>Transfered to 0260835140</Text>
            </View>

            <View style={S.listAmountBox}>
              <Badge value="-10,000.00" badgeStyle={[S.badgeD, S.bDanger]} textStyle={S.bDangerText} />
              <Text style={[S.listTextS, _sty.textRight, { paddingRight: 5 }]}>22-4-18, 12:52 PM</Text>
            </View>
          </View>

          <View style={S.listItem}>
            <View style={S.listBadgeBox} >
              <Badge status="success" />
            </View>

            <View style={S.listTextBox}>
              <Text style={S.listTextM}>Commercial Top-up</Text>
              <Text style={S.listTextS}>Card **5490</Text>
            </View>

            <View style={S.listAmountBox}>
              <Badge value="+100,000" badgeStyle={[S.badgeD, S.bSuccess]} textStyle={S.bSuccessText} />
              <Text style={[S.listTextS, _sty.textRight, { paddingRight: 5 }]}>05-10-17, 11:00 AM</Text>
            </View>
          </View>

          <View style={S.listItem}>
            <View style={S.listBadgeBox} >
              <Badge status="success" />
            </View>

            <View style={S.listTextBox}>
              <Text style={S.listTextM}>Commercial Collected</Text>
              <Text style={S.listTextS}>Card **5490</Text>
            </View>

            <View style={S.listAmountBox}>
              <Badge value="+100,000" badgeStyle={[S.badgeD, S.bSuccess]} textStyle={S.bSuccessText} />
              <Text style={[S.listTextS, _sty.textRight, { paddingRight: 5 }]}>02-9-17, 11:00 AM</Text>
            </View>
          </View>


          <View style={S.listItem}>
            <View style={S.listBadgeBox} >
              <Badge status="error" />
            </View>

            <View style={S.listTextBox}>
              <Text style={S.listTextM}>Retail Penal Charge</Text>
              <Text style={S.listTextS}>Transfered to 0260835140</Text>
            </View>

            <View style={S.listAmountBox}>
              <Badge value="-10,000.00" badgeStyle={[S.badgeD, S.bDanger]} textStyle={S.bDangerText} />
              <Text style={[S.listTextS, _sty.textRight, { paddingRight: 5 }]}>22-4-18, 12:52 PM</Text>
            </View>
          </View>

          <View style={S.listItem}>
            <View style={S.listBadgeBox} >
              <Badge status="error" />
            </View>

            <View style={S.listTextBox}>
              <Text style={S.listTextM}>Retail WHT</Text>
              <Text style={S.listTextS}>Transfered to 0260835140</Text>
            </View>

            <View style={S.listAmountBox}>
              <Badge value="-10,000.00" badgeStyle={[S.badgeD, S.bDanger]} textStyle={S.bDangerText} />
              <Text style={[S.listTextS, _sty.textRight, { paddingRight: 5 }]}>22-4-18, 12:52 PM</Text>
            </View>
          </View>

          <View style={S.listItem}>
            <View style={S.listBadgeBox} >
              <Badge status="error" />
            </View>

            <View style={S.listTextBox}>
              <Text style={S.listTextM}>Commercial Processing Fee</Text>
              <Text style={S.listTextS}>Transfered to 0260835140</Text>
            </View>

            <View style={S.listAmountBox}>
              <Badge value="-200" badgeStyle={[S.badgeD, S.bDanger]} textStyle={S.bDangerText} />
              <Text style={[S.listTextS, _sty.textRight, { paddingRight: 5 }]}>22-4-18, 12:52 PM</Text>
            </View>
          </View>

        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
});

export default TransTrustFund;