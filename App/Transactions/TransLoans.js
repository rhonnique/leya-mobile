import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  RefreshControl
} from "react-native";
import S from "./Transactions.style";
import _sty from "../../assets/css/Styles";
import Icon from "react-native-vector-icons/Ionicons";
import Empty from "./../../Components/Empty";
import { Button, Badge } from "react-native-elements";
import { ElBadge } from "./../../Components/Elements";
import moment from "moment";

class TransLoans extends Component {
  static navigationOptions = ({ navigation }) => ({
    drawerLockMode: "locked-closed",
    title: "LOANS HISTORY",
    headerTintColor: "#666",
    headerTitleStyle: {
      fontSize: 15
    },
    headerStyle: {
      elevation: 0,
      shadowOpacity: 0,
      backgroundColor: "#F7F7F7",
      borderBottomWidth: 1,
      borderBottomColor: "#D7D7D7"
    }
  });

  constructor(props) {
    super(props);
    this.state = {
      data: null,
      dataLoader: { loading: false, error: false, rd: false }
    };

    this.fetchData = this.fetchData.bind(this);
  }

  componentWillMount() {
    this.fetchData();
  }

  fetchData() {
    var dataLoader = { loading: true, error: false, rd: false };
    this.setState({ dataLoader });

    this.props.screenProps
      .loanGet()
      .then(response => response)
      .then(response => {
        console.log(response.data, "loan history....");
        var dataLoader = { loading: false, error: false, rd: true };
        this.setState({ dataLoader, data: response.data });
      })
      .catch(error => {
        var dataLoader = { loading: false, error: true, rd: false };
        this.setState({ dataLoader });
        this.props.screenProps.handleAxiosError(error);
        console.log(JSON.stringify(error), "investment history error ....");
      });
  }

  render() {
    const { dataLoader, data } = this.state;
    return (
      <SafeAreaView
        style={[_sty.containerDefault]}
      >
        {/* ****** If loading ****** */}
        {dataLoader.loading && (
          <View style={[_sty.containerAlignCC, { flex: 1 }]}>
            <Text>Loading ...</Text>
          </View>
        )}

        {/* ****** If an error occured ****** */}
        {dataLoader.error && (
          <View
            style={[_sty.containerAlignCC, { paddingHorizontal: 50, flex: 1 }]}
          >
            <Icon name="ios-alert" size={30} color="#959A97" />
            <Text style={{ fontSize: 18, color: "#959A97", fontWeight: "500" }}>
              An error occurred, please retry
            </Text>
            <Text
              style={{
                fontSize: 14,
                color: "#999",
                textAlign: "center",
                marginVertical: 10
              }}
            >
              {`The system can not fetch data, please click refresh`}
            </Text>

            <Button
              title="Reload Data"
              type="clear"
              onPress={this.fetchData}
              titleStyle={[_sty.textDefault, _sty.fontSize14]}
            />
          </View>
        )}

        {/* ****** If no data found ****** */}
        {dataLoader.rd && data.data && data.data.length === 0 && (
          <Empty
            title={"No Loans"}
            msg={"There is no loans so far."}
          />
        )}

        {dataLoader.rd &&
          data.data !== null &&
          Object.entries(data.data).length > 0 && (
            <ScrollView style={{ paddingHorizontal: 15 }}
              refreshControl={<RefreshControl onRefresh={this.fetchData} />}
            >
              {data.data.map((row, index) => {
                let topup = "";
                let type = "";
                let trans_source = "";
                let amount_label = "";
                let amount = (row.amount / 1).toFixed(2);
                amount = amount
                  .toString()
                  .replace(/\B(?=(\d{3})+(?!\d))/g, ",");

                switch (row.topup) {
                  case 0:
                    topup = `Collected`;
                    break;
                  case 1:
                    topup = `Top-up`;
                    break;
                }

                switch (row.type) {
                  case 1:
                    type = `Retail ${topup}`;
                    amount_label = (<ElBadge title={`+${amount}`} bg="#D1F0DB" textColor="#00A134" />);
                    break;
                  case 2:
                    type = `Commercial ${topup}`;
                    amount_label = (<ElBadge title={`+${amount}`} bg="#D1F0DB" textColor="#00A134" />);
                    break;
                }

                switch (row.payment_type) {
                  case 1:
                    trans_source = "Fund Transfer";
                    break;
                  case 2:
                    trans_source = "Debit Card";
                    break;
                }

                return (
                  <TouchableOpacity key={index} style={_sty.listItem}>
                    <View style={_sty.listBadgeBox}>
                      <Badge status="success" />
                    </View>

                    <View style={_sty.listTextBox}>
                      <Text style={_sty.listTextM}>{type}</Text>
                      <Text style={_sty.listTextS}>{trans_source}</Text>
                    </View>

                    <View style={_sty.listAmountBox}>
                      {amount_label}
                      <Text
                        style={[
                          _sty.listTextS,
                          _sty.textRight,
                          { position: 'relative', right: 5 }
                        ]}
                      >
                        {moment(row.trans_date).format('MMM DD, YYYY')}
                      </Text>
                    </View>
                  </TouchableOpacity>
                );
              })}
            </ScrollView>
          )}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});

export default TransLoans;
