import React, { Component, Fragment } from 'react'
import {
  View,
  Text, Alert, RefreshControl, Dimensions,
  StyleSheet, Image, StatusBar, TouchableOpacity, Modal, ScrollView
} from 'react-native'
import { Button, Card, Badge } from 'react-native-elements'
import Icon from "react-native-vector-icons/Ionicons";
import _sty from '../../assets/css/Styles';
import DashboardStatus from '../../Components/DashboardStatus';
import Cf from '../../Components/Cf';
import { ElBadge } from '../../Components/Elements';
import moment from "moment";
import { ModalAlert } from '../../Components/ModalAlert';
import { ElButton } from "../../Components/Elements";
import HTML from 'react-native-render-html';
import { CheckBox, Body, ListItem } from 'native-base';

const MyStatusBar = ({ backgroundColor, ...props }) => (
  <View style={[styles.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);

class Investment extends Component {

  static navigationOptions = ({ navigation }) => ({
    tabBarLabel: "4:24 Investment"
  })

  constructor(props) {
    super(props);
    this.state = {
      customer: null,
      investment: null,
      investmentLoader: { loading: false, error: false, rd: false },

      callbackSign: 0,
      modalVisible: false,
      requestId: 0,
      modalAlertVisible: false,
      modalAlertTitle: '',
      modalAlertMessage: '',

      termsLoader: { loading: false, error: false, rd: false },
      terms: null,
      modalVisibleTerms: false,
      term_investment: false,
      term_investment_check: false
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.fetchInvestment = this.fetchInvestment.bind(this);
    this.setRequestId = this.setRequestId.bind(this);
    this.modalAlertSet = this.modalAlertSet.bind(this);
    this.initWithdrawFund = this.initWithdrawFund.bind(this);

    this.fetchTerms = this.fetchTerms.bind(this);
    this.setTerms = this.setTerms.bind(this);
    this.updateTerms = this.updateTerms.bind(this);
  }


  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  setModalVisibleTerms(visible) {
    this.setState({ modalVisibleTerms: visible });
    this.fetchTerms();
  }

  componentWillMount() { 
    const { customer } = this.props.screenProps;
    this.setState({
      term_investment : customer.term_investment === 1 ? true : false,
      customer
    });
    this.fetchInvestment();
  }

  componentDidMount() {
    this.forceUpdate();
  }

  componentDidUpdate(prevProps) {

    const { navigation } = this.props;
    const callback = navigation.getParam('callback', null);
    const callbackSign = navigation.getParam('callbackSign', 0);

    if (callback && this.state.callbackSign !== callbackSign && callback.reload === true) {
      this.fetchInvestment();
      this.setState({ callbackSign })
    }
  }

  fetchInvestment() {
    var investmentLoader = { loading: true, error: false, rd: false }
    this.setState({ investmentLoader })

    this.props.screenProps.requestGet(1)
      .then(response => response)
      .then(response => {
        console.log(JSON.stringify(response.data), 'investment....');
        var investmentLoader = { loading: false, error: false, rd: true }
        this.setState({ investmentLoader, investment: response.data })
        this.props.screenProps.investmentSet(response.data.data);

      })
      .catch((error) => {
        var investmentLoader = { loading: false, error: true, rd: false }
        this.setState({ investmentLoader })
        this.props.screenProps.handleAxiosError(error);
        console.log(JSON.stringify(error), 'investment error ....');
      });
  }

  fetchTerms() {
    if (this.state.terms === null) {
      var termsLoader = { loading: true, error: false, rd: false };
      this.setState({ termsLoader });

      this.props.screenProps
        .termsGet()
        .then(response => response)
        .then(response => {
          var termsLoader = { loading: false, error: false, rd: true };
          this.setState({ termsLoader, terms: response.data && response.data.terms });
        })
        .catch(error => {
          var termsLoader = { loading: false, error: true, rd: false };
          this.setState({ termsLoader });
        });
    }
  }

  handleSubmit(id) {
    //console.log(id, 'id.....')
    //this.setState({ modalVisible: false });
    this.setModalVisible(false);
    this.props.screenProps.setSpinner(true);
    this.props.screenProps.investmentDelete(id)
      .then(response => response)
      .then(response => {
        this.fetchInvestment();
      })
      .catch((error) => {
        this.props.screenProps.setError(error);
      }).then(() => {
        this.props.screenProps.setSpinner(false);
      })
  }

  setRequestId(id) {
    this.setState({ requestId: id })
  }

  initWithdrawFund() {
    const { navigate } = this.props.navigation;
    navigate('WithdrawFunds')
  }

  modalAlertSet(status) {
    this.setState({ modalAlertVisible: status })
  }

  inCheck(dataLoader, data) {
    return dataLoader.rd && data && data.data && Object.entries(data.data).length > 0;
  }


  initRequest(type) {
    const { term_investment } = this.state.customer;
    if (parseInt(term_investment) === 0) {
      this.setModalVisibleTerms(true);
      return;
    }

    if (this.state.investment && this.state.investment.requests &&
      this.state.investment.requests.length === 5) {
      this.setState({
        modalAlertTitle: 'No more request!',
        modalAlertMessage: `You have reached your maximum number of requests for today. You only have a maximum of 5 requests per day.`,
        modalAlertVisible: true
      })
      return;
    }

    this.props.screenProps.setSpinner(true);
    this.props.screenProps.customerGet()
      .then(response => response)
      .then(response => {
        const { customer, settings } = response.data;
        //console.log(JSON.stringify(customer), 'customer updated......')
        this.props.screenProps.customerSet(customer);
        this.props.screenProps.settingsSet(settings);
        const { navigation } = this.props;
        navigation.navigate('InvestmentRequest', {
          type: type
        })
      })
      .catch((error) => {
        this.props.screenProps.setError(error);
      }).then(() => {
        this.props.screenProps.setSpinner(false);
      })

  }


  setTerms(value) {
    this.setState({ term_investment_check: value });
  }

  updateTerms() {
    this.setModalVisibleTerms(false);
    const postData = {
      term_investment: 1
    };

    this.props.screenProps
      .customerEdit(postData)
      .then(response => response)
      .then(response => {
        //Alert.alert('done');
        console.log(JSON.stringify(response.data), 'updateNotify......')
        this.setState({ term_investment: true });
        const { customer } = response.data;
        this.props.screenProps.customerSet(customer);
        this.setState({ customer });
        this.props.screenProps.termsInvestmentsSet(true);
        this.initRequest(1);

      })
      .catch(error => {
        //console.log(JSON.stringify(error.response.data), "errrrr 0000");
      });
  }

  render() {


    const { investmentLoader, investment, termsLoader, terms, term_investment_check } = this.state;
    const { customer } = this.props.screenProps;
    const portfolioHeight = this.inCheck(investmentLoader, investment) ? 140 : 110;

    return (<Fragment>

      <ModalAlert
        visible={this.state.modalAlertVisible}
        onClosePress={this.modalAlertSet}
        title={this.state.modalAlertTitle}
        message={this.state.modalAlertMessage}
      />

      <Modal
        presentationStyle={`overFullScreen`}
        animationType="slide"
        transparent={true}
        visible={this.state.modalVisible}
        onRequestClose={() => { }}
      >
        <View style={{ backgroundColor: 'rgba(0,0,0,.5)', flex: 1, justifyContent: 'flex-end' }}>
          <View
            style={{ backgroundColor: '#fff', minHeight: 350, maxHeight: '100%', margin: 20, borderRadius: 7 }}>



            <View style={[_sty.rowSpace, { paddingHorizontal: 15, paddingVertical: 10 }]}>
              <Text
                style={[_sty.py5, _sty.fontSize25,
                _sty.textBlack]}>Are you sure?</Text>

              <TouchableOpacity onPress={() => this.setModalVisible(!this.state.modalVisible)}>
                <Icon name="ios-close" size={30} color='#999' />
              </TouchableOpacity>

            </View>
            <View style={[_sty.rowPx, _sty.py15, _sty.flex]}>


              <Text style={[_sty.fontSize16, _sty.mb15]}>Your are about to terminate your pending request, please note that accepting will result to the following:</Text>

              <Text style={[_sty.fontSize14, _sty.mb15, { marginLeft: 10 }]}>Leya will not process your request.</Text>

              <Text style={[_sty.fontSize14, _sty.mb15, { marginLeft: 10 }]}>You are not required and should not make any deposit to the bank.</Text>






              <View style={[_sty.vBottom]}>
                <View style={[_sty.rowSpace]}>
                  <TouchableOpacity onPress={() => this.handleSubmit(this.state.requestId)}
                    style={[_sty.btn, _sty.alignSelfCenter, _sty.bgTheme]}>
                    <Text style={[_sty.btnText, _sty.textWhite]}>Accept</Text>
                  </TouchableOpacity>

                  <TouchableOpacity onPress={() => this.setModalVisible(!this.state.modalVisible)}
                    style={[_sty.btn, _sty.alignSelfCenter, _sty.bgDanger]}>
                    <Text style={[_sty.btnText, _sty.textWhite]}>Cancel</Text>
                  </TouchableOpacity>
                </View>
              </View>

            </View>

          </View>
        </View>
      </Modal>




      {/* Terms and condition */}
      <Modal
        presentationStyle={`overFullScreen`}
        animationType="slide"
        transparent={true}
        visible={this.state.modalVisibleTerms}
        onRequestClose={() => { }}
      >
        <View style={{ backgroundColor: 'rgba(0,0,0,.5)', flex: 1, justifyContent: 'flex-end' }}>
          <View style={{ backgroundColor: '#fff', height: '85%', margin: 20, borderRadius: 7 }}>
            <View style={[_sty.flex]}>


              {/* Header */}
              <View style={[_sty.termsModalHeader]}>
                <Text style={[_sty.py5, _sty.fontSize18, _sty.textBlack, _sty.textAlignCenter]}>Terms of Service</Text>

                <TouchableOpacity style={[_sty.termsModalHeaderClose]}
                  onPress={() => this.setModalVisibleTerms(!this.state.modalVisibleTerms)}>
                  <Icon name="ios-close" size={30} color='#999' />
                </TouchableOpacity>
              </View>
              {/* /Header */}

              {termsLoader.loading && <View style={[_sty.containerAlignCC, _sty.containerFlex]}>
                <Text>Loading ...</Text>
              </View>}

              {/* ****** If an error occured ****** */}
              {termsLoader.error && (
                <View style={[_sty.containerAlignCC, _sty.containerFlex]}>
                  <Icon name="ios-alert" size={30} color="#959A97" />
                  <Text style={{ fontSize: 18, color: "#959A97", fontWeight: "500" }}>
                    An error occurred, please retry
            </Text>
                  <Text
                    style={{
                      fontSize: 14,
                      color: "#999",
                      textAlign: "center",
                      marginVertical: 10
                    }}
                  >
                    {`The system can not fetch data, please click refresh`}
                  </Text>

                  <Button
                    title="Reload Data"
                    type="clear"
                    onPress={this.fetchTerms}
                    titleStyle={[_sty.textDefault, _sty.fontSize14]}
                  />
                </View>
              )}

              {termsLoader.rd && terms !== null && (<ScrollView style={[_sty.containerFlex, _sty.p20]}>
                <HTML html={terms} imagesMaxWidth={Dimensions.get('window').width} />
              </ScrollView>)}



              <View style={[_sty.flexEnd, _sty.py10, _sty.pb25]}>
                <View>

                  <ListItem style={{
                    width: 230, alignSelf: "center", borderBottomWidth: 0,
                    marginLeft: 0, paddingRight: 0
                  }}>
                    <CheckBox checked={term_investment_check}
                      color="green"
                      //disabled={this.state.term_investment_check === true ? true : false}
                      onPress={() => this.setTerms(!this.state.term_investment_check)}
                    />
                    <Body>
                      <Text style={[_sty.pl10]}>I agree to the Terms of Service</Text>
                    </Body>
                  </ListItem>


                </View>
                <View /* style={[_sty.rowSpace]} */>
                  {/* <TouchableOpacity onPress={() => this.handleSubmit(this.state.requestId)}
                    style={[_sty.btn, _sty.alignSelfCenter, _sty.bgTheme]}>
                    <Text style={[_sty.btnText, _sty.textWhite]}>Accept</Text>
                  </TouchableOpacity> */}

                  <ElButton
                    title="Accept"
                    onPress={this.updateTerms}
                    shadow={true}
                    width={70}
                    bg="#00A134"
                    textColor="white"
                    disabled={
                      !term_investment_check ? true : false
                    }
                  />
                </View>
              </View>

            </View>

          </View>
        </View>
      </Modal>
      {/* Terms and condition */}


      <View style={{ flex: 1 }}>
        {/* <MyStatusBar backgroundColor="#5E8D48" barStyle="light-content" /> */}

        {/* Dashboard Data */}
        <View style={[_sty.pb10]}>
          <View style={[_sty.userDataContainer]}>
            <View style={[_sty.containerWrapper]}>
              <DashboardStatus
                data={customer}
                interest_rate={this.inCheck(investmentLoader, investment) ? investment.data.interest_rate : 0}
              />
            </View>
          </View>

          <View style={[_sty.porfolioContainer]}>
            <Image style={_sty.positionTopImg} source={require('./../../assets/images//bg-curve-top.png')} />

            {/* Portfolio */}
            <View style={[_sty.porfolioBalanceContainer, _sty.boxShadow, {
              height: portfolioHeight
            }]}>

              <View style={[_sty.porfolioBalanceContainerInner, {
                height: portfolioHeight
              }]}>


                <Image style={{ resizeMode: 'contain', width: '100%' }} source={require('./../../assets/images/flora.png')} />
                <View style={{ position: 'absolute', paddingHorizontal: 15, width: '100%' }}>


                  <Text style={[{ fontSize: 17, color: '#000' }]}>Your Portfolio balance is:</Text>
                  <Text style={[{ fontSize: 25, color: '#000', fontWeight: 'bold' }]}>
                    <Cf value={this.inCheck(investmentLoader, investment) ? investment.data.amount : 0} />
                  </Text>

                  <TouchableOpacity style={_sty.textIconBox} onPress={() => this.initRequest(1)}>
                    <Icon name="ios-add-circle" size={25} color='#00A134' style={_sty.textIcon} />
                    <Text style={[_sty.fontSize14, _sty.textBlack]}> Add Funds</Text>
                  </TouchableOpacity>

                  {this.inCheck(investmentLoader, investment) && (
                    <TouchableOpacity onPress={() => this.initRequest(2)}>
                      <ElBadge fontWeight="bold" title="Withdraw Funds" status="warning" />
                    </TouchableOpacity>)}

                </View>

              </View>

            </View>
            {/* /Portfolio */}


          </View>

        </View>

        <View style={{ flex: 5, backgroundColor: '#fff', alignItems: 'center', justifyContent: 'center' }}>
          {investmentLoader.loading && (<Text>Loading ...</Text>)}

          {/* ****** If an error occured ****** */}
          {investmentLoader.error && (<View style={[_sty.containerAlignCC, { paddingHorizontal: 50 }]}>
            <Icon name="ios-alert" size={30} color='#959A97' />
            <Text style={{ fontSize: 18, color: '#959A97', fontWeight: '500' }}>An error occurred, please retry</Text>
            <Text style={{ fontSize: 14, color: '#999', textAlign: 'center', marginVertical: 10 }}>
              {`The system can not fetch data, please click refresh`}
            </Text>

            <Button title="Reload Data" type="clear" onPress={this.fetchInvestment}
              titleStyle={[_sty.textDefault, _sty.fontSize14]} />
          </View>)}



          {/* ****** If no data found ****** */}
          {investmentLoader.rd && (investment.data === null && investment.requests === null) && (<View style={{ width: 175, alignItems: 'center' }}>
            <Icon name="ios-alert" size={30} color='#959A97' />
            <Text style={{ fontSize: 18, color: '#959A97', fontWeight: '500' }}>No Investment</Text>
            <Text style={{ fontSize: 14, color: '#999', textAlign: 'center', marginTop: 10, marginBottom: 20 }}>
              {`Join by adding funds to\n your portfolio.`}
            </Text>

            <View style={_sty.textIconBox}>
              <Text
                style={[
                  _sty.fontSize14,
                  _sty.textGrayLight,
                  { marginRight: 2 }
                ]}
              >
                {" "}
                Tab{" "}
              </Text>
              <Icon
                name="ios-add-circle"
                size={25}
                color="#EBF0ED"
                style={_sty.textIcon}
              />
              <Text style={[_sty.fontSize14, _sty.textGrayLight]}>
                {" "}
                to add one
                  </Text>
            </View>
          </View>)}





          {investmentLoader.rd && (investment.data !== null || investment.requests !== null) && (<Fragment>
            <View style={[{ flex: 1, width: '100%', alignSelf: 'center' }]}>
              <ScrollView
                refreshControl={
                  <RefreshControl
                    onRefresh={this.fetchInvestment}
                  />
                }>



                {/* *** Pending request **** */}
                {investment.requests && (<Fragment>

                  <View style={[{ padding: 15 }]}>
                    <Text style={[_sty.width100, _sty.mb15]}>Pending request{investment.requests.length > 1 && 's'}</Text>
                    {investment.requests.map((row, index) => {
                      //const tenure_date = row.tenure_date;

                      return (<View key={index} style={[_sty.containerCard, _sty.bgWarning, _sty.mb20, { paddingTop: 15, paddingBottom: 7 }]}>
                        <TouchableOpacity onPress={() => {
                          this.setModalVisible(true);
                          this.setRequestId(row.id)
                        }}
                          style={[_sty.cardCloseBtn]}>
                          <Icon name="ios-close" size={25} color='#999' />
                        </TouchableOpacity>

                        <View
                          style={[_sty.rowSpace, { paddingHorizontal: 15, paddingVertical: 4 }]}>
                          <Text style={[_sty.cardTextWarning]}>{parseInt(row.request_type) === 3 ? 'Withdrawal' : 'Proposed'} amount</Text>
                          <Text style={[_sty.cardTextWarning]}><Cf value={row.amount} /></Text>
                        </View>

                        <View
                          style={[_sty.rowSpace, { paddingHorizontal: 15, paddingVertical: 5 }]}>
                          <Text style={[_sty.cardTextWarning]}>{parseInt(row.request_type) === 3 ? 'Withdrawal' : 'Maturity'} date</Text>
                          <Text style={[_sty.cardTextWarning]}>{parseInt(row.request_type) === 3 ?
                            moment(row.tenure_date).format('MMM DD, YYYY') : moment(row.tenure_date).format('MMM, YYYY')}</Text>
                        </View>

                      </View>)
                    })}

                    <View style={[_sty.infoContainer, _sty.mt10, _sty.boxShadow]}>
                      <View style={[_sty.infoContainerInner]}>
                        <Image style={{ resizeMode: 'contain', width: '100%', position: 'absolute' }} source={require('./../../assets/images/pending-bg.png')} />
                        <Text style={{ textAlign: 'center', paddingHorizontal: 8 }}>Hey {customer.name}! we have recieved your request and will get back to you  in the next 24 hours.</Text>
                      </View>
                    </View>


                  </View>


                </Fragment>)}
                {/* *** Pending request **** */}





                {/* *** Approved investment **** */}
                {investment.data && (<Fragment>
                  <View style={[{ padding: 15 }]}>
                    <Text style={[_sty.width100, _sty.mb10]}>Your Interest information
                     </Text>

                    <Card
                      containerStyle={{
                        backgroundColor: 'rgba(0,161,52, .1)', elevation: 0, borderWidth: 0,
                        borderRadius: 5, overflow: "hidden", paddingVertical: 0, margin: 0, marginBottom: 15
                      }} >

                      <View
                        style={[_sty.rowSpace, { borderBottomWidth: 1, borderBottomColor: '#C5E9CF', paddingVertical: 9 }]}>
                        <Text style={[_sty.textTheme]}>Interest Due Monthly
                        <Text style={[_sty.textDanger]}> *</Text></Text>
                        <Text style={[_sty.textTheme]}><Cf value={investment.data.interest_amount} /></Text>
                      </View>

                      <View
                        style={[_sty.rowSpace, { borderBottomWidth: 1, borderBottomColor: '#C5E9CF', paddingVertical: 9 }]}>
                        <Text style={[_sty.textTheme]}>Investment Amount</Text>
                        <Text style={[_sty.textTheme]}><Cf value={investment.data.amount} /></Text>
                      </View>

                      <View
                        style={[_sty.rowSpace, { borderBottomWidth: 0, borderBottomColor: '#C5E9CF', paddingVertical: 9 }]}>
                        <Text style={[_sty.textTheme]}>Mature Date</Text>
                        <Text style={[_sty.textTheme]}>{moment(investment.data.tenure_date).format('MMM, YYYY')}</Text>
                      </View>

                      <Text style={[_sty.textRight, _sty.mt10, _sty.mb10, _sty.textDanger]}>*Exclusive WHT</Text>

                    </Card>


                    <View style={[_sty.infoContainer, _sty.mt10, _sty.boxShadow]}>
                      <View style={[_sty.infoContainerInner]}>
                        <Image style={{ resizeMode: 'contain', width: '100%', position: 'absolute' }} source={require('./../../assets/images/confetti.png')} />
                        <Text style={{ textAlign: 'center', marginBottom: 5 }}>Hey {customer.name}! here is your income so far</Text>
                        <Text style={{ textAlign: 'center', fontSize: 22, fontWeight: 'bold' }}><Cf value={investment.data.interest_amount} /></Text>
                      </View>
                    </View>

                  </View>
                </Fragment>)}
                {/* *** Approved investment **** */}







              </ScrollView>
            </View>
          </Fragment>)}




        </View>


      </View>
    </Fragment>)
  }
}


const styles = StyleSheet.create({

})

export default Investment


