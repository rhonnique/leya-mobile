import React, { Component, Fragment } from "react";
import {
  View,
  Text,
  Alert,
  RefreshControl,
  StyleSheet,
  Image,
  StatusBar,
  TouchableOpacity,
  Modal,
  ScrollView,
  Dimensions
} from "react-native";
import { Button, Card, Badge } from "react-native-elements";
import Icon from "react-native-vector-icons/Ionicons";
import _sty from "../../assets/css/Styles";
import DashboardStatus from "../../Components/DashboardStatus";
import Cf from "../../Components/Cf";
import { ElBadge, ElButton } from '../../Components/Elements';
import moment from "moment";
import { ModalAlert } from "../../Components/ModalAlert";
import * as Progress from "react-native-progress";
import HTML from 'react-native-render-html';
import { CheckBox, Body, ListItem } from 'native-base';

const MyStatusBar = ({ backgroundColor, ...props }) => (
  <View style={[styles.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);

class TrustFunds extends Component {
  static navigationOptions = ({ navigation }) => ({
    tabBarLabel: "Trust Funds"
  });

  constructor(props) {
    super(props);
    this.state = {
      customer: null,
      trust: null,
      dataLoader: { loading: false, error: false, rd: false },
      callbackSign: 0,
      modalVisible: false,
      requestId: 0,

      modalAlertVisible: false,
      modalAlertTitle: "",
      modalAlertMessage: "",

      termsLoader: { loading: false, error: false, rd: false },
      terms: null,
      modalVisibleTerms: false,
      term_trust: false,
      term_trust_check: false
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.fetchTrust = this.fetchTrust.bind(this);
    this.setRequestId = this.setRequestId.bind(this);
    this.modalAlertSet = this.modalAlertSet.bind(this);
    this.initAddFund = this.initAddFund.bind(this);
    this.initWithdrawFund = this.initWithdrawFund.bind(this);

    this.fetchTerms = this.fetchTerms.bind(this);
    this.setTerms = this.setTerms.bind(this);
    this.updateTerms = this.updateTerms.bind(this);
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  setModalVisibleTerms(visible) {
    this.setState({ modalVisibleTerms: visible });
    this.fetchTerms();
  }

  componentWillMount() {
    const { customer } = this.props.screenProps;
    this.setState({
      term_trust: customer.term_trust === 1 ? true : false,
      customer
    });
    this.fetchTrust();
  }

  componentDidMount() {
    this.forceUpdate();
  }

  componentDidUpdate(prevProps) {
    const { navigation } = this.props;
    const callback = navigation.getParam("callback", null);
    const callbackSign = navigation.getParam("callbackSign", 0);

    if (
      callback &&
      this.state.callbackSign !== callbackSign &&
      callback.reload === true
    ) {
      this.fetchTrust();
      this.setState({ callbackSign });
    }
  }

  fetchTrust() {
    var dataLoader = { loading: true, error: false, rd: false };
    this.setState({ dataLoader });

    this.props.screenProps
      .requestGet(3)
      .then(response => response)
      .then(response => {
        //const { trust } = response.data;
        console.log(response.data, "trustfunds....");
        var dataLoader = { loading: false, error: false, rd: true };
        this.setState({ dataLoader, trust: response.data });
        this.props.screenProps.trustSet(response.data.data);
      })
      .catch(error => {
        var dataLoader = { loading: false, error: true, rd: false };
        this.setState({ dataLoader });
        this.props.screenProps.handleAxiosError(error);
      });
  }

  fetchTerms() {
    if (this.state.terms === null) {
      var termsLoader = { loading: true, error: false, rd: false };
      this.setState({ termsLoader });

      this.props.screenProps
        .termsGet()
        .then(response => response)
        .then(response => {
          var termsLoader = { loading: false, error: false, rd: true };
          this.setState({ termsLoader, terms: response.data && response.data.terms });
        })
        .catch(error => {
          var termsLoader = { loading: false, error: true, rd: false };
          this.setState({ termsLoader });
        });
    }
  }

  handleSubmit(id) {
    //console.log(id, 'id.....')
    //this.setState({ modalVisible: false });
    this.setModalVisible(false);
    this.props.screenProps.setSpinner(true);
    this.props.screenProps
      .trustDelete(id)
      .then(response => response)
      .then(response => {
        this.fetchTrust();
      })
      .catch(error => {
        this.props.screenProps.setError(error);
      })
      .then(() => {
        this.props.screenProps.setSpinner(false);
      });
  }

  setRequestId(id) {
    this.setState({ requestId: id });
  }

  initAddFund() {
    if (
      this.state.trust &&
      this.state.trust.requests &&
      this.state.trust.requests.length === 5
    ) {
      this.setState({
        modalAlertTitle: "No more request!",
        modalAlertMessage: `You have reached your maximum number of requests for today. You only have a maximum of 5 requests per day.`,
        modalAlertVisible: true
      });
      return;
    }

    this.props.screenProps.setSpinner(true);
    this.props.screenProps
      .customerGet()
      .then(response => response)
      .then(response => {
        const { customer, settings } = response.data;
        this.props.screenProps.customerSet(customer);
        this.props.screenProps.settingsSet(settings);
        const { navigation } = this.props;
        navigation.navigate("AddFunds");
      })
      .catch(error => {
        this.props.screenProps.setError(error);
      })
      .then(() => {
        this.props.screenProps.setSpinner(false);
      });
  }

  initWithdrawFund() {
    const { navigate } = this.props.navigation;
    navigate("WithdrawFunds");
  }

  fx(div, by) {
    //return (div/by);
    return Math.floor((div / by) * 100);
    //return Math.floor(($div/$by) * 100);
  }

  fxp(div, by) {
    return div / by;
    //return parseFloat((div/by).toFixed(2));
    //return Math.floor(($div/$by) * 100);
  }

  modalAlertSet(status) {
    this.setState({ modalAlertVisible: status });
  }

  inCheck(dataLoader, data) {
    return (
      dataLoader.rd && data && data.data && Object.entries(data.data).length > 0
    );
  }

  initRequest() {
    const { term_trust } = this.state.customer;
    if (parseInt(term_trust) === 0) {
      this.setModalVisibleTerms(true);
      return;
    }

    this.props.screenProps.setSpinner(true);
    this.props.screenProps.customerGet()
      .then(response => response)
      .then(response => {
        const { customer, settings } = response.data;
        this.props.screenProps.customerSet(customer);
        this.props.screenProps.settingsSet(settings);
        const { navigation } = this.props;
        navigation.navigate("TrustRequest");
      })
      .catch((error) => {
        this.props.screenProps.setError(error);
      }).then(() => {
        this.props.screenProps.setSpinner(false);
      })
  }


  setTerms(value) {
    this.setState({ term_trust_check: value });
  }

  updateTerms() {
    this.setModalVisibleTerms(false);
    const postData = {
      term_trust: 1
    };

    this.props.screenProps
      .customerEdit(postData)
      .then(response => response)
      .then(response => {
        //console.log(JSON.stringify(response.data), 'updateNotify......')
        this.setState({ term_trust: true });
        const { customer } = response.data;
        this.setState({ customer });
        this.props.screenProps.customerSet(customer);
        this.props.screenProps.termsTrustsSet(true);
        this.initRequest();
      })
      .catch(error => {
        //console.log(JSON.stringify(error.response.data), "errrrr 0000");
      });
  }

  render() {
    const { dataLoader, trust, termsLoader, terms, term_trust_check } = this.state;
    const { customer } = this.props.screenProps;

    return (
      <Fragment>
        <ModalAlert
          visible={this.state.modalAlertVisible}
          onClosePress={this.modalAlertSet}
          title={this.state.modalAlertTitle}
          message={this.state.modalAlertMessage}
        />

        <Modal
          presentationStyle={`overFullScreen`}
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => { }}
        >
          <View
            style={{
              backgroundColor: "rgba(0,0,0,.5)",
              flex: 1,
              justifyContent: "flex-end"
            }}
          >
            <View
              style={{
                backgroundColor: "#fff",
                minHeight: 350,
                maxHeight: "100%",
                margin: 20,
                borderRadius: 7
              }}
            >
              <View
                style={[
                  _sty.rowSpace,
                  { paddingHorizontal: 15, paddingVertical: 10 }
                ]}
              >
                <Text style={[_sty.py5, _sty.fontSize25, _sty.textBlack]}>
                  Are you sure?
                </Text>

                <TouchableOpacity
                  onPress={() => this.setModalVisible(!this.state.modalVisible)}
                >
                  <Icon name="ios-close" size={30} color="#999" />
                </TouchableOpacity>
              </View>
              <View style={[_sty.rowPx, _sty.py15, _sty.flex]}>
                <Text style={[_sty.fontSize16, _sty.mb15]}>
                  Your are about to terminate your pending request, please note
                  that accepting will result to the following:
                </Text>

                <Text style={[_sty.fontSize14, _sty.mb15, { marginLeft: 10 }]}>
                  Leya will not process your request.
                </Text>

                <Text style={[_sty.fontSize14, _sty.mb15, { marginLeft: 10 }]}>
                  You are not required and should not make any deposit to the
                  bank.
                </Text>

                <View style={[_sty.vBottom]}>
                  <View style={[_sty.rowSpace]}>
                    <TouchableOpacity
                      onPress={() => this.handleSubmit(this.state.requestId)}
                      style={[_sty.btn, _sty.alignSelfCenter, _sty.bgTheme]}
                    >
                      <Text style={[_sty.btnText, _sty.textWhite]}>Accept</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                      onPress={() =>
                        this.setModalVisible(!this.state.modalVisible)
                      }
                      style={[_sty.btn, _sty.alignSelfCenter, _sty.bgDanger]}
                    >
                      <Text style={[_sty.btnText, _sty.textWhite]}>Cancel</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </Modal>





        {/* Terms and condition */}
        <Modal
          presentationStyle={`overFullScreen`}
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisibleTerms}
          onRequestClose={() => { }}
        >
          <View style={{ backgroundColor: 'rgba(0,0,0,.5)', flex: 1, justifyContent: 'flex-end' }}>
            <View style={{ backgroundColor: '#fff', height: '85%', margin: 20, borderRadius: 7 }}>
              <View style={[_sty.flex]}>


                {/* Header */}
                <View style={[_sty.termsModalHeader]}>
                  <Text style={[_sty.py5, _sty.fontSize18, _sty.textBlack, _sty.textAlignCenter]}>Terms of Service</Text>

                  <TouchableOpacity style={[_sty.termsModalHeaderClose]}
                    onPress={() => this.setModalVisibleTerms(!this.state.modalVisibleTerms)}>
                    <Icon name="ios-close" size={30} color='#999' />
                  </TouchableOpacity>
                </View>
                {/* /Header */}

                {termsLoader.loading && <View style={[_sty.containerAlignCC, _sty.containerFlex]}>
                  <Text>Loading ...</Text>
                </View>}

                {/* ****** If an error occured ****** */}
                {termsLoader.error && (
                  <View style={[_sty.containerAlignCC, _sty.containerFlex]}>
                    <Icon name="ios-alert" size={30} color="#959A97" />
                    <Text style={{ fontSize: 18, color: "#959A97", fontWeight: "500" }}>
                      An error occurred, please retry
            </Text>
                    <Text
                      style={{
                        fontSize: 14,
                        color: "#999",
                        textAlign: "center",
                        marginVertical: 10
                      }}
                    >
                      {`The system can not fetch data, please click refresh`}
                    </Text>

                    <Button
                      title="Reload Data"
                      type="clear"
                      onPress={this.fetchTerms}
                      titleStyle={[_sty.textDefault, _sty.fontSize14]}
                    />
                  </View>
                )}

                {termsLoader.rd && terms !== null && (<ScrollView style={[_sty.containerFlex, _sty.p20]}>
                  <HTML html={terms} imagesMaxWidth={Dimensions.get('window').width} />
                </ScrollView>)}



                <View style={[_sty.flexEnd, _sty.py10, _sty.pb25]}>
                  <View>

                    <ListItem style={{
                      width: 230, alignSelf: "center", borderBottomWidth: 0,
                      marginLeft: 0, paddingRight: 0
                    }}>
                      <CheckBox checked={term_trust_check}
                        color="green"
                        onPress={() => this.setTerms(!this.state.term_trust_check)}
                      />
                      <Body>
                        <Text style={[_sty.pl10]}>I agree to the Terms of Service</Text>
                      </Body>
                    </ListItem>


                  </View>
                  <View>

                    <ElButton
                      title="Accept"
                      onPress={this.updateTerms}
                      shadow={true}
                      width={70}
                      bg="#00A134"
                      textColor="white"
                      disabled={
                        !term_trust_check ? true : false
                      }
                    />
                  </View>
                </View>

              </View>

            </View>
          </View>
        </Modal>
        {/* Terms and condition */}



        <View style={{ flex: 1 }}>
          {/* <MyStatusBar backgroundColor="#5E8D48" barStyle="light-content" /> */}


          {/* Dashboard Data */}
          <View style={[_sty.pb10]}>
            <View style={[styles.userDataContainer]}>
              <View style={_sty.containerWrapper}>
                <DashboardStatus
                  data={customer}
                  type={3}
                  interest_rate={
                    this.inCheck(dataLoader, trust)
                      ? trust.data.interest_rate
                      : 0
                  }
                />
              </View>
            </View>

            <View style={[_sty.porfolioContainer]}>
              <Image
                style={_sty.positionTopImg}
                source={require("./../../assets/images//bg-curve-top.png")}
              />


              {/* Portfolio */}
              <View style={[_sty.porfolioBalanceContainer, _sty.boxShadow]}>
                <View style={[_sty.porfolioBalanceContainerInner]}>
                  <Image style={{ resizeMode: "contain", width: "100%" }} source={require("./../../assets/images/trust-bg.png")} />
                  <View style={{ position: "absolute", paddingHorizontal: 15, width: "100%" }}>
                    <Text style={[{ fontSize: 17, color: "#000" }]}>Your total trust fund:</Text>
                    <Text style={[{ fontSize: 25, color: "#000", fontWeight: "bold" }]}>
                      <Cf value={this.inCheck(dataLoader, trust) ? trust.data.amount : 0} />
                    </Text>

                    <TouchableOpacity style={_sty.textIconBox} onPress={() => this.initRequest()} >
                      <Icon name="ios-add-circle" size={25} color="#00A134" style={_sty.textIcon} />
                      <Text style={[_sty.fontSize14, _sty.textBlack]}>Add Benefactor</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
              {/* /Portfolio */}


            </View>
          </View>
          {/* /Dashboard Data */}

          <View
            style={{
              flex: 5,
              backgroundColor: "#fff",
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            {dataLoader.loading && <Text>Loading ...</Text>}

            {/* ****** If an error occured ****** */}
            {dataLoader.error && (
              <View style={[_sty.containerAlignCC, { paddingHorizontal: 50 }]}>
                <Icon name="ios-alert" size={30} color="#959A97" />
                <Text
                  style={{ fontSize: 18, color: "#959A97", fontWeight: "500" }}
                >
                  An error occurred, please retry
                </Text>
                <Text
                  style={{
                    fontSize: 14,
                    color: "#999",
                    textAlign: "center",
                    marginVertical: 10
                  }}
                >
                  {`The system can not fetch data, please click refresh`}
                </Text>

                <Button
                  title="Reload Data"
                  type="clear"
                  onPress={this.fetchTrust}
                  titleStyle={[_sty.textDefault, _sty.fontSize14]}
                />
              </View>
            )}

            {/* ****** If no data found ****** */}
            {dataLoader.rd && (trust.data === null && trust.requests === null) && (
              <View style={{ width: 175, alignItems: "center" }}>
                <Icon name="ios-alert" size={30} color="#959A97" />
                <Text
                  style={{ fontSize: 18, color: "#959A97", fontWeight: "500" }}
                >
                  No Funds
                </Text>
                <Text
                  style={{
                    fontSize: 14,
                    color: "#999",
                    textAlign: "center",
                    marginTop: 10,
                    marginBottom: 20
                  }}
                >
                  {`You have no benefactors\nlisted in this account.`}
                </Text>

                <View style={_sty.textIconBox}>
                  <Text
                    style={[
                      _sty.fontSize14,
                      _sty.textGrayLight,
                      { marginRight: 2 }
                    ]}
                  >
                    {" "}
                    Tab{" "}
                  </Text>
                  <Icon
                    name="ios-add-circle"
                    size={25}
                    color="#EBF0ED"
                    style={_sty.textIcon}
                  />
                  <Text style={[_sty.fontSize14, _sty.textGrayLight]}>
                    {" "}
                    to add one
                  </Text>
                </View>
              </View>
            )}

            {dataLoader.rd && (trust.data !== null || trust.requests !== null) && (
              <Fragment>
                <View style={[{ flex: 1, width: "100%", alignSelf: "center" }]}>
                  <ScrollView
                    refreshControl={
                      <RefreshControl onRefresh={this.fetchTrust} />
                    }
                  >
                    {/* *** Pending request **** */}
                    {trust.requests && (
                      <Fragment>
                        <View style={[{ padding: 15 }]}>
                          <Text style={[_sty.width100, _sty.mb15]}>
                            Pending request{trust.requests.length > 1 && "s"}
                          </Text>
                          {trust.requests.map((row, index) => {
                            return (
                              <View
                                key={index}
                                style={[
                                  _sty.containerCard,
                                  _sty.bgWarning,
                                  _sty.mb20,
                                  { paddingTop: 15, paddingBottom: 7 }
                                ]}
                              >
                                <TouchableOpacity
                                  onPress={() => {
                                    this.setModalVisible(true);
                                    this.setRequestId(row.id);
                                  }}
                                  style={[_sty.cardCloseBtn]}
                                >
                                  <Icon
                                    name="ios-close"
                                    size={25}
                                    color="#999"
                                  />
                                </TouchableOpacity>

                                <View
                                  style={[
                                    _sty.rowSpace,
                                    {
                                      paddingHorizontal: 15,
                                      paddingVertical: 4
                                    }
                                  ]}
                                >
                                  <Text style={[_sty.cardTextWarning]}>
                                    No. of benefactors
                                  </Text>
                                  <Text style={[_sty.cardTextWarning]}>
                                    {row.benefactors}
                                  </Text>
                                </View>

                                <View
                                  style={[
                                    _sty.rowSpace,
                                    {
                                      paddingHorizontal: 15,
                                      paddingVertical: 4
                                    }
                                  ]}
                                >
                                  <Text style={[_sty.cardTextWarning]}>
                                    Targeted trust amount
                                  </Text>
                                  <Text style={[_sty.cardTextWarning]}>
                                    <Cf value={row.amount} />
                                  </Text>
                                </View>

                                <View
                                  style={[
                                    _sty.rowSpace,
                                    {
                                      paddingHorizontal: 15,
                                      paddingVertical: 5
                                    }
                                  ]}
                                >
                                  <Text style={[_sty.cardTextWarning]}>
                                    Proposed tenure
                                  </Text>
                                  <Text style={[_sty.cardTextWarning]}>
                                    {row.tenure}{" "}
                                    {row.tenure > 1 ? "Years" : "Year"}
                                  </Text>
                                </View>
                              </View>
                            );
                          })}

                          <View style={[_sty.infoContainer, _sty.mt10, _sty.boxShadow]}>
                            <View style={[_sty.infoContainerInner]}>
                              <Image
                                style={{
                                  resizeMode: "contain",
                                  width: "100%",
                                  position: "absolute"
                                }}
                                source={require("./../../assets/images/pending-bg.png")}
                              />
                              <Text style={{ textAlign: "center" }}>
                                Hey {customer.name}! we have recieved your request
                                and will get back to you in the next 24 hours.
                            </Text>
                            </View>
                          </View>

                        </View>
                      </Fragment>
                    )}
                    {/* *** Pending request **** */}

                    {/* *** Trust Benefactors Object.entries(data.data).length **** */}
                    {trust.data && (
                      <Fragment>
                        <View style={[{ padding: 15 }]}>
                          <Text style={[_sty.width100, _sty.mb10]}>
                            Benefactors
                          </Text>

                          {trust.data_benefactors &&
                            Object.entries(trust.data_benefactors).length > 0 &&
                            trust.data_benefactors.map((row, index) => {
                              return (
                                <View key={index} style={[styles.bnContainer, _sty.boxShadow]}>
                                  <View style={[_sty.rowSpace]}>
                                    <View style={[_sty.width50]}>
                                      <Text
                                        style={[
                                          _sty.fontSize10,
                                          _sty.fontWeightSemiBold
                                        ]}
                                      >
                                        BENEFACTOR {index + 1}
                                      </Text>
                                      <Text style={[_sty.fontSize14]}>
                                        {row.name}
                                      </Text>
                                    </View>
                                    <View style={[_sty.width50]}>
                                      <Text
                                        style={[
                                          _sty.textRight,
                                          _sty.fontSize10,
                                          _sty.fontWeightSemiBold
                                        ]}
                                      >
                                        AMOUNT SAVED
                                      </Text>
                                      <Text
                                        style={[
                                          _sty.textRight,
                                          _sty.fontSize16,
                                          _sty.textTheme,
                                          _sty.fontBold
                                        ]}
                                      >
                                        <Cf value={row.saved_amount} />
                                      </Text>
                                    </View>
                                  </View>

                                  {/* Progress bar */}
                                  <Progress.Bar
                                    style={{ marginTop: 10 }}
                                    progress={this.fxp(
                                      row.saved_amount,
                                      row.target_amount
                                    )}
                                    width={null}
                                    height={5}
                                    borderWidth={0}
                                    color={"#00A134"}
                                    unfilledColor={"#f7f7f7"}
                                  />
                                  <Text
                                    style={[
                                      _sty.textRight,
                                      _sty.textGrey,
                                      _sty.mt5,
                                      _sty.fontSize12
                                    ]}
                                  >
                                    {/* `${Math.floor((`${row.saved_amount}`/`${row.target_amount}`) * 100)}` */}
                                    {this.fx(
                                      row.saved_amount,
                                      row.target_amount
                                    )}
                                    %
                                  </Text>

                                  <View style={[_sty.rowSpace, _sty.mt10]}>
                                    <View style={[_sty.width50]}>
                                      <Text
                                        style={[
                                          _sty.fontSize9,
                                          _sty.fontWeightSemiBold
                                        ]}
                                      >
                                        GOAL
                                      </Text>
                                      <Text style={[_sty.fontSize12]}>
                                        <Cf value={row.target_amount} />
                                      </Text>
                                    </View>
                                    <View style={[_sty.width50]}>
                                      <Text
                                        style={[
                                          _sty.textRight,
                                          _sty.fontSize9,
                                          _sty.fontWeightSemiBold
                                        ]}
                                      >
                                        MATURITY DATE
                                      </Text>
                                      <Text
                                        style={[
                                          _sty.textRight,
                                          _sty.fontSize12,
                                          _sty.textTheme,
                                          _sty.fontBold
                                        ]}
                                      >
                                        {moment(row.tenure_date).format(
                                          "MMM DD, YYYY"
                                        )}
                                      </Text>
                                    </View>
                                  </View>
                                </View>
                              );
                            })}

                          {/* <Card
                      containerStyle={{
                        backgroundColor: 'rgba(0,161,52, .1)', elevation: 0, borderWidth: 0,
                        borderRadius: 5, overflow: "hidden", paddingVertical: 0, margin: 0, marginBottom: 15
                      }} >

                      <View
                        style={[_sty.rowSpace, { borderBottomWidth: 1, borderBottomColor: '#C5E9CF', paddingVertical: 9 }]}>
                        <Text style={[_sty.textTheme]}>Proposed Amount</Text>
                        <Text style={[_sty.textTheme]}><Cf value={trust.data.amount} /></Text>
                      </View>

                      <View
                        style={[_sty.rowSpace, { borderBottomWidth: 1, borderBottomColor: '#C5E9CF', paddingVertical: 9 }]}>
                        <Text style={[_sty.textTheme]}>Interest Due</Text>
                        <Text style={[_sty.textTheme]}><Cf value={trust.data.interest_amount} /></Text>
                      </View>

                      <View
                        style={[_sty.rowSpace, { borderBottomWidth: 0, borderBottomColor: '#C5E9CF', paddingVertical: 9 }]}>
                        <Text style={[_sty.textTheme]}>Mature Date</Text>
                        <Text style={[_sty.textTheme]}>{moment(trust.data.tenure_date).format('MMM, YYYY')}</Text>
                      </View>

                    </Card> */}
                        </View>
                      </Fragment>
                    )}
                    {/* *** Approved trust **** */}
                  </ScrollView>
                </View>
              </Fragment>
            )}
          </View>
        </View>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  userDataContainer: {
    height: 80,
    backgroundColor: "#00A134"
  },

  bnContainer: {
    backgroundColor: "#fff",
    position: "relative",
    minHeight: 100,
    borderRadius: 5,
    marginBottom: 35,
    padding: 20
  }
});

export default TrustFunds;
