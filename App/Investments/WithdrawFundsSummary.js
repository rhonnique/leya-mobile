import React, { Component, Fragment } from 'react';
import { Text, View, StyleSheet, Alert, Modal, Picker, TouchableHighlight, TouchableOpacity } from 'react-native';
//import Modal from "react-native-modal";
import { ModalAlert } from '../../Components/ModalAlert';
import moment from "moment";

import Icon from "react-native-vector-icons/Ionicons";
import _sty from '../../assets/css/Styles';

import Cf from '../../Components/Cf';
import { Card, ListItem, Button } from 'react-native-elements'
import { DatePicker } from 'native-base';
//import DatePicker from 'react-native-datepicker'



//moment.suppressDeprecationWarnings = true;

class WithdrawFundsSummary extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'WITHDRAW FUNDS',
    headerTintColor: '#FFF',
    headerTitleStyle: {
      fontSize: 15
    },
    headerStyle: {
      elevation: 0,
      shadowOpacity: 0,
      backgroundColor: '#00A134',
      borderBottomWidth: 0
    }
  })

  constructor(props) {
    super(props);
    this.state = {
      defaultDate: moment().toDate(),
      modalVisible: false,
      investment_balance: 0,
      amount: 0,
      balance: 0,
      interest_amount: 0,
      interest_rate: this.props.screenProps.customer.interest_rate,
      tenure: 0,
      modalAlertVisible: false,
      modalAlertTitle: '',
      modalAlertMessage: '',
      withdrawalDate: null
    }

    this.handleSubmit = this.handleSubmit.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.handlePreview = this.handlePreview.bind(this)
    this.modalAlertSet = this.modalAlertSet.bind(this)
    this.setDate = this.setDate.bind(this);
    this.initSummary = this.initSummary.bind(this)

  }

  componentWillMount() {
    const { navigation } = this.props;
    const amount = navigation.getParam('amount', 0);
    const investment_balance = this.props.screenProps.investment && this.props.screenProps.investment.amount ?      this.props.screenProps.investment.amount : 0;
    this.setState({ amount: parseFloat(amount), investment_balance: parseFloat(investment_balance) });
  }

  componentWillUnmount() {
    this.setState({ modalVisible: false });
  }

  setDate(newDate) {
    this.setState({ withdrawalDate: moment(newDate).format('YYYY-MM-DD') });
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  closeModal() {
    this.setModalVisible(!this.state.modalVisible)
  }

  goBack() {
    const { goBack } = this.props.navigation;
    goBack();
  }

  handleSubmit() {
    this.props.screenProps.setSpinner(true);

    const tenure_date = this.state.year + '-' + this.state.month + '-01';
    const postData = {
      amount: this.state.amount,
      tenure: 2,
      tenure_date: tenure_date,
      interest_rate: this.state.interest_rate
    }

    this.props.screenProps.investmentRequest(postData)
      .then(response => {
        return response
      })
      .then(response => {
        //const { email } = response.data.data;
        this.setState({ modalVisible: false });
        //console.log(response.data.data, 'response 0000')
        this.props.navigation.navigate('Success', {
          title: 'Awesome!',
          message: `Your request has been sent.\nYour account officer will contact you upon approval.`,
          goto: 'Investment',
          callback: { reload: true }
        })

      })
      .catch((error) => {
        if (error.response && error.response.data) {
          const { code, message } = error.response.data;
          console.log(JSON.stringify(error.response.data), 'errrrr 0000');
          Alert.alert(`${message}\n${code}`);
        } else {
          Alert.alert(`An error occurred, please retry!`);
        }
      }).then(() => {
        this.props.screenProps.setSpinner(false);
      })


    //console.log(this.state.month, 'month....')
    /* this.props.screenProps.setSpinner(true);
    setTimeout(() => {
      this.props.screenProps.setSpinner(false);
      this.setState({ modalVisible: false })
      this.props.navigation.navigate('Success', {
        title: 'Awesome!',
        message: `Your request has been sent.\nYour account officer will contact you upon approval.`,
        goto: 'Investment',
      })
    }, 5000); */
  }

  handlePreview() {
    const date = moment(`${this.state.month} ${moment().format(`DD`)}, ${this.state.year}`).format(`YYYY-MM-DD`)
    if (moment().diff(date, 'months') && moment().diff(date, 'months') > 0) {
      this.setState({
        modalAlertTitle: 'Oops..There\'s an Error',
        modalAlertMessage: 'Date and year can not be less than current date/year',
        modalAlertVisible: true
      })
    } else {
      this.setModalVisible(true)
    }
  }

  modalAlertSet(status) {
    this.setState({ modalAlertVisible: status })
  }

  initSummary() {
    const investment_balance = this.state.investment_balance;
    const amount = this.state.amount;
    const balance = (investment_balance - amount);

    if (this.state.withdrawalDate === null) {
      this.setState({
        modalAlertTitle: 'Oops..There\'s an Error',
        modalAlertMessage: 'Please select withdrawal date',
        modalAlertVisible: true
      })
      return
    }

    if (amount > investment_balance) {
      this.setState({
        modalAlertTitle: 'Oops..There\'s an Error',
        modalAlertMessage: 'Withdrawal amount can not exceed your portfolio.',
        modalAlertVisible: true
      })
      return
    }

    this.setState({ balance })
    this.setModalVisible(true)
  }


  render() {

    return (<Fragment>

      <ModalAlert
        visible={this.state.modalAlertVisible}
        onClosePress={this.modalAlertSet}
        title={this.state.modalAlertTitle}
        message={this.state.modalAlertMessage}
      />

      <Modal
        presentationStyle={`overFullScreen`}
        animationType="slide"
        transparent={true}
        visible={this.state.modalVisible}
        onRequestClose={() => { }}
      >
        <View style={{ backgroundColor: 'rgba(0,0,0,.5)', flex: 1, justifyContent: 'flex-end' }}>
          <View
            style={{ backgroundColor: '#fff', minHeight: 350, maxHeight: '100%', margin: 20, borderRadius: 7 }}>



            <View style={[_sty.rowSpace, { paddingHorizontal: 15, paddingVertical: 10 }]}>
              <Text
                style={[_sty.py5, _sty.fontSize18,
                _sty.textBlack, _sty.fontWeightSemiBold]}>Let's go over your request</Text>

              {this.state.tenure === 1 && (<TouchableOpacity onPress={() => this.setModalVisible(!this.state.modalVisible)}>
                <Icon name="ios-close" size={30} color='#999' />
              </TouchableOpacity>)}

            </View>
            <View style={[_sty.rowPx, _sty.py15, _sty.flex]}>


              <Card
                containerStyle={{ backgroundColor: 'rgba(0,161,52, .1)', elevation: 0, borderWidth: 0, borderRadius: 5, overflow: "hidden", paddingVertical: 0, margin: 0 }} >

                <View
                  style={[_sty.rowSpace, { borderBottomWidth: 1, borderBottomColor: '#C5E9CF', paddingVertical: 13 }]}>
                  <Text style={[_sty.textTheme]}>Withdrawal Amount</Text>
                  <Text style={[_sty.textTheme]}><Cf value={parseFloat(this.state.amount)} /></Text>
                </View>

                <View
                  style={[_sty.rowSpace, { borderBottomWidth: 1, borderBottomColor: '#C5E9CF', paddingVertical: 13 }]}>
                  <Text style={[_sty.textTheme]}>New Balance</Text>
                  <Text style={[_sty.textTheme]}><Cf value={parseFloat(this.state.balance)} /></Text>
                </View>

                <View
                  style={[_sty.rowSpace, { borderBottomWidth: 0, borderBottomColor: '#C5E9CF', paddingVertical: 13 }]}>
                  <Text style={[_sty.textTheme]}>Withdrawal Date</Text>
                  <Text style={[_sty.textTheme]}>{moment(this.state.withdrawalDate).format('MMM DD, YYYY')}</Text>
                </View>

              </Card>


              <View style={[_sty.vBottom]}>
                <View style={[_sty.rowSpace]}>
                  <TouchableOpacity onPress={this.handleSubmit}
                    style={[_sty.btn, _sty.alignSelfCenter, _sty.bgTheme]}>
                    <Text style={[_sty.btnText, _sty.textWhite]}>Accept</Text>
                  </TouchableOpacity>

                  <TouchableOpacity /* onPress={this.state.tenure === 0 ? this.goBack() : this.setModalVisible(!this.state.modalVisible)}  */
                    onPress={this.closeModal}
                    style={[_sty.btn, _sty.alignSelfCenter, _sty.bgDanger]}>
                    <Text style={[_sty.btnText, _sty.textWhite]}>Cancel</Text>
                  </TouchableOpacity>
                </View>
              </View>

            </View>

          </View>
        </View>
      </Modal>


      <View style={[_sty.containerFlex]}>

        <View style={[_sty.rows, _sty.bgTheme, { paddingVertical: 25 }]}>
          <View style={[_sty.containerMdx]}>
            <Text style={[_sty.fontSize20, _sty.textWhite]}>When do you want your cash?</Text>
          </View>
        </View>


        <View style={[_sty.containerMdx, { paddingVertical: 25 }]}>

          {/* Month Selected */}
          <View style={[_sty.rowSpace, _sty.mb20]}>
            <View><Text style={[_sty.py6, _sty.fontSize18]}>Month Selected</Text></View>
            <View style={{ width: 135, borderWidth: 2, borderColor: 'green', borderRadius: 5, overflow: 'hidden' }}>
              {<DatePicker
                defaultDate={this.state.defaultDate}
                minimumDate={this.state.defaultDate}
                locale={"en"}
                timeZoneOffsetInMinutes={undefined}
                modalTransparent={false}
                animationType={"fade"}
                androidMode={"default"}
                placeHolderText="Pick a date"
                textStyle={{ color: "#666", paddingVertical: 7, paddingHorizontal: 10 }}
                placeHolderTextStyle={{ color: "#666", paddingVertical: 7, paddingHorizontal: 10 }}
                onDateChange={this.setDate}
                disabled={false}
                formatChosenDate={date => { return moment(date).format('MMM DD, YYYY'); }}
              />}
            </View>
          </View>


        </View>

        <View style={[_sty.vBottom, _sty.mb40]}>
          <TouchableOpacity
            onPress={this.initSummary}
            style={[_sty.btn, _sty.alignSelfCenter,
            _sty.width60, _sty.bgThemeLight, _sty.boxShadow]}>
            <Text style={[_sty.btnText, _sty.textWhite]}>Request</Text>
          </TouchableOpacity>
        </View>


      </View>

    </Fragment>
    );
  }
}

const styles = StyleSheet.create({

  itemStyle: {
    height: 36,
    color: 'black'
  },
  picker: {
    width: 100
  },

  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
  },
  showPickerBtn: {
    height: 44,
    backgroundColor: '#973BC2',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 16,
    borderRadius: 6,
  },
  yearMonthText: {
    fontSize: 20,
    marginTop: 12
  },

});


export default WithdrawFundsSummary;