import React, { Component, Fragment } from 'react';
import { Text, View, StyleSheet, Alert, Modal, Picker, TouchableHighlight, TouchableOpacity } from 'react-native';
//import Modal from "react-native-modal";
import { ModalAlert } from '../../Components/ModalAlert';
import moment from "moment";

import Icon from "react-native-vector-icons/Ionicons";
import _sty from '../../assets/css/Styles';

import Cf from '../../Components/Cf';
import { Card, ListItem, Button } from 'react-native-elements'


const year = () => {
  return ['2019', '2021', '2022', '2023', '2024', '2025', '2026', '2027', '2028', '2029', '2030']
}

const month = () => {
  return ['January', 'Febuary', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
}

class AddFundsSummary extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'ADD FUNDS',
    headerTintColor: '#FFF',
    headerTitleStyle: {
      fontSize: 15
    },
    headerStyle: {
      elevation: 0,
      shadowOpacity: 0,
      backgroundColor: '#00A134',
      borderBottomWidth: 0
    }
  })

  constructor(props) {
    super(props);
    this.state = {
      month: moment().format(`MMM`),
      year: moment().format(`YYYY`),
      modalVisible: false,
      amount: 0,
      interest_amount: 0,
      interest_rate: 0,
      tenure: 0,
      modalAlertVisible: false,
      modalAlertTitle: '',
      modalAlertMessage: '',
    }

    this.handleSubmit = this.handleSubmit.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.handlePreview = this.handlePreview.bind(this)
    this.modalAlertSet = this.modalAlertSet.bind(this)

  }

  componentWillMount() {
    const { navigation } = this.props;

    let interest_rate = this.props.screenProps.customer.interest_rate > 0 
    ? this.props.screenProps.customer.interest_rate : this.props.screenProps.settings.interest_rate;
    interest_rate = parseInt(interest_rate);
    console.log(interest_rate, 'interest_rate....')
    let amount = navigation.getParam('amount', 0);
    let interest_amount = (parseFloat(amount) / 100) * interest_rate;
    amount = parseFloat(amount);
    interest_amount = parseFloat(interest_amount);
    this.setState({ amount, interest_amount, interest_rate });
  }

  componentWillUnmount() {
    this.setState({ modalVisible: false });
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  closeModal() {
    this.setModalVisible(!this.state.modalVisible)
  }

  goBack() {
    const { goBack } = this.props.navigation;
    goBack();
  }

  handleSubmit() {
    this.props.screenProps.setSpinner(true);

    const tenure_date = this.state.year + '-' + this.state.month + '-01';
    const postData = {
      amount: this.state.amount,
      tenure: 1,
      type: 1,
      tenure_date: tenure_date,
      interest_amount: this.state.interest_amount,
      interest_rate: this.state.interest_rate
    }

    this.props.screenProps.investmentRequest(postData)
      .then(response => {
        return response
      })
      .then(response => {
        //const { email } = response.data.data;
        this.setState({ modalVisible: false });
        //console.log(response.data.data, 'response 0000')
        this.props.navigation.navigate('Success', {
          title: 'Awesome!',
          message: `Your request has been sent.\nYour account officer will contact you upon approval.`,
          goto: 'Investment',
          callback: { reload: true }
        })

      })
      .catch((error) => {
        if (error.response && error.response.data) {
          const { code, message } = error.response.data;
          console.log(JSON.stringify(error.response.data), 'errrrr 0000');
          Alert.alert(`${message}\n${code}`);
        } else {
          Alert.alert(`An error occurred, please retry!`);
        }
      }).then(() => {
        this.props.screenProps.setSpinner(false);
      })


    //console.log(this.state.month, 'month....')
    /* this.props.screenProps.setSpinner(true);
    setTimeout(() => {
      this.props.screenProps.setSpinner(false);
      this.setState({ modalVisible: false })
      this.props.navigation.navigate('Success', {
        title: 'Awesome!',
        message: `Your request has been sent.\nYour account officer will contact you upon approval.`,
        goto: 'Investment',
      })
    }, 5000); */
  }

  handlePreview() {
    const date = moment(`${this.state.month} ${moment().format(`DD`)}, ${this.state.year}`).format(`YYYY-MM-DD`)
    if (moment().diff(date, 'months') && moment().diff(date, 'months') > 0) {
      this.setState({
        modalAlertTitle: 'Oops..There\'s an Error',
        modalAlertMessage: 'Date and year can not be less than current date/year',
        modalAlertVisible: true
      })
    } else {
      this.setModalVisible(true)
    }
  }

  modalAlertSet(status) {
    this.setState({ modalAlertVisible: status })
  }


  render() {

    return (<Fragment>

      <ModalAlert
        visible={this.state.modalAlertVisible}
        onClosePress={this.modalAlertSet}
        title={this.state.modalAlertTitle}
        message={this.state.modalAlertMessage}
      />

      <Modal
        presentationStyle={`overFullScreen`}
        animationType="slide"
        transparent={true}
        visible={this.state.modalVisible}
        onRequestClose={() => { }}
      >
        <View style={{ backgroundColor: 'rgba(0,0,0,.5)', flex: 1, justifyContent: 'flex-end' }}>
          <View
            style={{ backgroundColor: '#fff', minHeight: 350, maxHeight: '100%', margin: 20, borderRadius: 7 }}>



            <View style={[_sty.rowSpace, { paddingHorizontal: 15, paddingVertical: 10 }]}>
              <Text
                style={[_sty.py5, _sty.fontSize16,
                _sty.textBlack, _sty.fontWeightSemiBold]}>Let's go over your request</Text>

              {this.state.tenure === 1 && (<TouchableOpacity onPress={() => this.setModalVisible(!this.state.modalVisible)}>
                <Icon name="ios-close" size={30} color='#999' />
              </TouchableOpacity>)}

            </View>
            <View style={[_sty.rowPx, _sty.py15, _sty.flex]}>

              <Card
                containerStyle={{ backgroundColor: 'rgba(0,161,52, .1)', elevation: 0, borderWidth: 0, borderRadius: 5, overflow: "hidden", paddingVertical: 0, margin: 0 }} >

                <View
                  style={[_sty.rowSpace, { borderBottomWidth: 1, borderBottomColor: '#C5E9CF', paddingVertical: 13 }]}>
                  <Text style={[_sty.textTheme]}>Proposed Amount</Text>
                  <Text style={[_sty.textTheme]}><Cf value={parseFloat(this.state.amount)} /></Text>
                </View>

                <View
                  style={[_sty.rowSpace, { borderBottomWidth: 1, borderBottomColor: '#C5E9CF', paddingVertical: 13 }]}>
                  <Text style={[_sty.textTheme]}>Interest Due</Text>
                  <Text style={[_sty.textTheme]}><Cf value={parseFloat(this.state.interest_amount)} /></Text>
                </View>

                <View
                  style={[_sty.rowSpace, { borderBottomWidth: 0, borderBottomColor: '#C5E9CF', paddingVertical: 13 }]}>
                  <Text style={[_sty.textTheme]}>Mature Date</Text>
                  <Text style={[_sty.textTheme]}>{this.state.month + ', ' + this.state.year}</Text>
                </View>

              </Card>


              <View style={[_sty.vBottom]}>
                <View style={[_sty.rowSpace]}>
                  <TouchableOpacity onPress={this.handleSubmit}
                    style={[_sty.btn, _sty.alignSelfCenter, _sty.bgTheme]}>
                    <Text style={[_sty.btnText, _sty.textWhite]}>Accept</Text>
                  </TouchableOpacity>

                  <TouchableOpacity /* onPress={this.state.tenure === 0 ? this.goBack() : this.setModalVisible(!this.state.modalVisible)}  */
                    onPress={this.closeModal}
                    style={[_sty.btn, _sty.alignSelfCenter, _sty.bgDanger]}>
                    <Text style={[_sty.btnText, _sty.textWhite]}>Cancel</Text>
                  </TouchableOpacity>
                </View>
              </View>

            </View>

          </View>
        </View>
      </Modal>

      {/* <Modal
        isVisible={this.state.modalVisible}
        onSwipeComplete={() => this.setState({ isVisible: false })}
        swipeDirection="left">
        <View style={{ flex: 1 }}>
          <Text>I am the modal content!</Text>
        </View>
      </Modal> */}

      <View style={[_sty.containerFlex]}>


        <View style={[_sty.rows, _sty.bgTheme, { paddingVertical: 25 }]}>
          <View style={[_sty.containerMdx]}>
            <Text style={[_sty.fontSize20, _sty.textWhite]}>What is your proposed tenor?</Text>
          </View>
        </View>


        <View style={[_sty.containerMdx, { paddingVertical: 25 }]}>

          {/* Month Selected */}
          <View style={[_sty.rowSpace, _sty.mb20]}>
            <View><Text style={[_sty.py5, _sty.fontSize18]}>Month Selected</Text></View>
            <View style={{ height: 40, width: 160, borderWidth: 2, borderColor: 'green', borderRadius: 5, overflow: 'hidden' }}>
              <Picker
                selectedValue={this.state.month}
                style={styles.itemStyle}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({ month: itemValue })
                }>
                {month().map((month, index) => <Picker.Item key={index} label={month} value={month} />)}
              </Picker>
            </View>
          </View>


          {/* Year Selected */}
          <View style={[_sty.rowSpace, _sty.mb20]}>
            <View><Text style={[_sty.py5, _sty.fontSize18]}>Year Selected</Text></View>
            <View style={{ height: 40, width: 140, borderWidth: 2, borderColor: 'green', borderRadius: 5, overflow: 'hidden' }}>
              <Picker
                selectedValue={this.state.year}
                style={styles.itemStyle}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({ year: itemValue })
                }>
                {year().map((year, index) => <Picker.Item key={index} label={year} value={year} />)}
              </Picker>
            </View>
          </View>
        </View>

        <View style={[_sty.vBottom, _sty.mb40]}>
          <TouchableOpacity
            onPress={this.handlePreview}
            //onPress={() => this.setModalVisible(true)}
            style={[_sty.btn, _sty.alignSelfCenter,
            _sty.width60, _sty.bgThemeLight, _sty.boxShadow]}>
            <Text style={[_sty.btnText, _sty.textWhite]}>Proceed</Text>
          </TouchableOpacity>
        </View>





      </View>

    </Fragment>
    );
  }
}

const styles = StyleSheet.create({

  itemStyle: {
    height: 36,
    color: 'black'
  },
  picker: {
    width: 100
  },

  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
  },
  showPickerBtn: {
    height: 44,
    backgroundColor: '#973BC2',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 16,
    borderRadius: 6,
  },
  yearMonthText: {
    fontSize: 20,
    marginTop: 12
  },

});


export default AddFundsSummary;