import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  StatusBar,
  Image,
  SafeAreaView,
  Switch,
  TouchableOpacity
} from "react-native";
import { Button } from "react-native-elements";

import _sty from "../../assets/css/Styles";
import S from "./Settings.style";

class Settings extends Component {
  static navigationOptions = ({ navigation }) => ({
    drawerLockMode: "locked-closed",
    title: "SETTINGS",
    headerTintColor: "#666",
    headerTitleStyle: {
      fontSize: 15
    },
    headerStyle: {
      elevation: 0,
      shadowOpacity: 0,
      backgroundColor: "#F7F7F7",
      borderBottomWidth: 1,
      borderBottomColor: "#D7D7D7"
    }
  });

  constructor(props) {
    super(props);
    this.state = {
      notify: false
    };

    this.updateNotify = this.updateNotify.bind(this);
  }

  componentWillMount() {
    const notify = this.props.screenProps.parseBoolean(this.props.screenProps.customer.notify);
    console.log(notify, "set_notify...."); 
    //console.log(JSON.stringify(this.props.screenProps), "set_notify...."); 
    this.setState({ notify }); 
    //StatusBar.setBackgroundColor("red", true);
  }

  componentWillUnmount() {
    //this._navListener.remove();   <Switch value={true} />
  }

  updateNotify(value) {
    this.setState({ notify: value });

    const postData = {
      notify: (value === true ? 1 : 2)
    };

    this.props.screenProps
      .customerEdit(postData)
      .then(response => response)
      .then(async response => {
        console.log(JSON.stringify(response.data), 'updateNotify......')
        const { customer } = response.data;
        await this.props.screenProps.customerSet(customer);
        //await this.props.screenProps.notifySet(value);
      })
      .catch(error => {
        //this.props.screenProps.setError(error);
      });
  }


  render() {
    const { notify } = this.state;
    const { navigate } = this.props.navigation;
    return (
      <SafeAreaView style={S.container}>
        <View style={S.listView}>
          <View style={{ width: 45 }}>
            <Image source={require("./../../assets/images/ico-bell.png")} />
          </View>
          <Text style={S.listViewText}>Enable Notification</Text>
          <Switch
            style={S.listViewSwitch}
            onValueChange={this.updateNotify}
            value={notify}
          />
        </View>

        <TouchableOpacity
          style={S.listView}
          onPress={() => navigate("ChangePassword")}
        >
          <View style={{ width: 45 }}>
            <Image source={require("./../../assets/images/ico-lock.png")} />
          </View>
          <Text style={S.listViewText}>Change PIN</Text>
        </TouchableOpacity>

        {/* <View style={S.listView}>
          <View style={{ width: 45 }}>
            <Image source={require('./../../assets/images/ico-fingerprint.png')} />
          </View>
          <Text style={S.listViewText}>Enable Fingerprint</Text>
          <Switch style={S.listViewSwitch} value={true} />
        </View> */}

        <View style={[_sty.vBottom, _sty.mb15]}>
          <View style={_sty.containerSm}>
            <Button
              onPress={this.props.screenProps.setLogout}
              containerStyle={[_sty.mb15]}
              buttonStyle={[_sty.bgDanger, { height: 40 }]}
              titleStyle={{ paddingTop: 0, paddingBottom: 3, fontSize: 18 }}
              title="Logout"
              raised={true}
            />
            <Text style={_sty.vBottomText}>Version 1.0.0.12345</Text>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});

export default Settings;
