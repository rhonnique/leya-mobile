import React, { Component, Fragment } from "react";
import {
  Text,
  View,
  StyleSheet,
  StatusBar,
  RefreshControl,
  SafeAreaView,
  ScrollView,
  WebView,
  Dimensions 
} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import _sty from "../assets/css/Styles";
import { Button } from "react-native-elements";
import HTML from 'react-native-render-html';

class Privacy extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: "PRIVACY POLICY",
    headerTintColor: "#666",
    headerTitleStyle: {
      fontSize: 15
    },
    headerStyle: {
      elevation: 0,
      shadowOpacity: 0,
      backgroundColor: "#F7F7F7",
      borderBottomWidth: 1,
      borderBottomColor: "#D7D7D7"
    }
  });

  constructor(props) {
    super(props);
    this.state = {
      term: null,
      loader: { loading: false, error: false, rd: false }
    };

    this.fetchTerms = this.fetchTerms.bind(this);
  }

  componentWillMount() {
    this.fetchTerms();
  }

  fetchTerms() {
    var loader = { loading: true, error: false, rd: false };
    this.setState({ loader });

    this.props.screenProps
      .termsGet()
      .then(response => response)
      .then(response => {
        //console.log(JSON.stringify(response.data), "termsGet....");
        var loader = { loading: false, error: false, rd: true };
        this.setState({ loader, terms: response.data && response.data.privacy });
      })
      .catch(error => {
        var loader = { loading: false, error: true, rd: false };
        this.setState({ loader });
      });
  }

  render() {
    const { loader, terms } = this.state;
    var htmlCode = "<b>I am rendered in a <i>WebView</i></b>";
    return (
      <View style={[styles.container]}>
        {loader.loading && <View style={[_sty.containerAlignCC, { paddingHorizontal: 50, flex: 1 }]}>
          <Text>Loading ...</Text>
        </View>}

        {/* ****** If an error occured ****** */}
        {loader.error && (
          <View style={[_sty.containerAlignCC, { paddingHorizontal: 50 }]}>
            <Icon name="ios-alert" size={30} color="#959A97" />
            <Text style={{ fontSize: 18, color: "#959A97", fontWeight: "500" }}>
              An error occurred, please retry
            </Text>
            <Text
              style={{
                fontSize: 14,
                color: "#999",
                textAlign: "center",
                marginVertical: 10
              }}
            >
              {`The system can not fetch data, please click refresh`}
            </Text>

            <Button
              title="Reload Data"
              type="clear"
              onPress={this.fetchTerms}
              titleStyle={[_sty.textDefault, _sty.fontSize14]}
            />
          </View>
        )}

        {loader.rd && terms !== null && (
          <Fragment>
            <ScrollView
              refreshControl={<RefreshControl onRefresh={this.fetchTerms} />}
            >
              <HTML html={terms} imagesMaxWidth={Dimensions.get('window').width} />
            </ScrollView>
          </Fragment>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    padding: 20
  },
  heading: {
    textTransform: "uppercase",
    color: "#000",
    fontWeight: "bold",
    marginBottom: 20
  },
  p: {
    marginBottom: 25,
    lineHeight: 19,
    color: "#333"
  }
});

export default Privacy;
