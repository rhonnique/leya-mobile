import PropTypes from 'prop-types';
import React, { Component } from 'react';
import styles from './DrawerMenu.style';
import { NavigationActions } from 'react-navigation';
import { ScrollView, Text, View, Image, TouchableOpacity } from 'react-native';
import { StackNavigator } from 'react-navigation';
import _sty from "../assets/css/Styles";
import Icon from "react-native-vector-icons/Ionicons";

class DrawerMenu extends Component {
  navigateToScreen = (route) => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
  }

  render() {
    const { customer } = this.props.screenProps;
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={styles.avatar}>
          <Image style={{ height: 72, width: 50 }} source={require('./../assets/images/logo.png')} />
          </View>
          <Text style={{ fontSize: 18, color: '#333', marginBottom: 5, 
          marginTop: 10, fontWeight: '500' }}>{customer && customer.name}</Text> 
          <Text style={{ fontSize: 14, color: '#999' }}>{customer && parseInt(customer.status) === 1 ? 'Active':'Inactive'}</Text>
        </View>

        {/* <TouchableOpacity style={styles.profileStatus} onPress={this.navigateToScreen('Profile')}>
          <Text style={{ fontWeight: '500' }}>My Profile <Text style={{ color: '#666', fontWeight: 'normal' }}> (Complete 40%)</Text></Text>
        </TouchableOpacity> */}
        <TouchableOpacity style={_sty.listView} onPress={this.navigateToScreen('Profile')}>
          <Text style={_sty.listViewText}>My Profile</Text>
          <Icon style={_sty.listViewIcon} name="ios-arrow-forward" size={15} color='#666' />
        </TouchableOpacity>

        <ScrollView>

          <TouchableOpacity style={styles.profileList} onPress={this.navigateToScreen('Transactions')}>
            <View style={{ width: 45 }}><Image source={require('./../assets/images/ico-activity.png')} /></View>
            <Text style={styles.profileListText}>Transaction History</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.profileList} onPress={this.navigateToScreen('Faq')}>
            <View style={{ width: 45 }}><Image source={require('./../assets/images/ico-faq.png')} /></View>
            <Text style={styles.profileListText}>Frequently Asked Questions</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.profileList} onPress={this.navigateToScreen('Privacy')}>
            <View style={{ width: 45 }}><Image source={require('./../assets/images/ico-privacy.png')} /></View>
            <Text style={styles.profileListText}>Privacy Policy</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.profileList} onPress={this.navigateToScreen('Terms')}>
            <View style={{ width: 45 }}><Image source={require('./../assets/images/ico-terms.png')} /></View>
            <Text style={styles.profileListText}>Terms and Conditions</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.profileList} onPress={this.navigateToScreen('Help')}>
            <View style={{ width: 45 }}><Image source={require('./../assets/images/ico-help.png')} /></View>
            <Text style={styles.profileListText}>Help</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.profileList} onPress={this.navigateToScreen('Settings')}>
            <View style={{ width: 45 }}><Image source={require('./../assets/images/ico-settings.png')} /></View>
            <Text style={styles.profileListText}>Settings</Text>
          </TouchableOpacity>





          {/* <View>
            <View style={styles.navSectionStyle}>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('Page1')}>
                Page1
              </Text>
            </View>
          </View>
          <View>
            <View style={styles.navSectionStyle}>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('Page2')}>
                Page2
              </Text>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('Plain')}>
                Plain
              </Text>
            </View>
          </View>
          <View>
            <Text style={styles.sectionHeadingStyle}>
              Section 3
            </Text>
            <View style={styles.navSectionStyle}>
              <Text style={styles.navItemStyle} onPress={this.navigateToScreen('Page4')}>
                Page4
              </Text>
            </View>
          </View> */} 


        </ScrollView>



        <View style={styles.footerContainer}>
          <Text style={{fontSize:10, color:'#979797'}}>Version 1.0.0.12345</Text>
        </View>
      </View>
    );
  }
}

DrawerMenu.propTypes = {
  navigation: PropTypes.object
};

export default DrawerMenu;