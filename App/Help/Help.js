import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  StatusBar,
  Image,
  SafeAreaView,
  Switch,
  TouchableOpacity,
  ScrollView
} from "react-native";
import _sty from "../../assets/css/Styles";
import Empty from "./../../Components/Empty";

class Help extends Component {
  static navigationOptions = ({ navigation }) => ({
    drawerLockMode: "locked-closed",
    title: "CHAT",
    headerTintColor: "#666",
    headerTitleStyle: {
      fontSize: 15
    },
    headerStyle: {
      elevation: 0,
      shadowOpacity: 0,
      backgroundColor: "#F7F7F7",
      borderBottomWidth: 1,
      borderBottomColor: "#D7D7D7"
    }
  });

  render() {
    return (
      <SafeAreaView style={_sty.containerDefault}>
        <Empty
          button={true}
          title={"No agents available"}
          msg={`Am sorry there is no agents available at the\nmoment to chat. Please try again later or call\nour help line.`}
          icon={{ color: "red" }}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    padding: 20
  },
  heading: {
    textTransform: "uppercase",
    color: "#000",
    fontWeight: "bold",
    marginBottom: 20
  },
  p: {
    marginBottom: 25,
    lineHeight: 19,
    color: "#333"
  }
});

export default Help;
