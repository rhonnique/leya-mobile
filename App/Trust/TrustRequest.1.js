import React, { Component, Fragment } from 'react';
import { Text, View, StyleSheet, TextInput, Modal, TouchableOpacity, KeyboardAvoidingView, SafeAreaView } from 'react-native';
//import Modal from "react-native-modal";
import { ModalAlert } from '../../Components/ModalAlert';
import { ElButton } from '../../Components/Elements';
import moment from "moment";

import Icon from "react-native-vector-icons/Ionicons";
import _sty from '../../assets/css/Styles';

import Cf from '../../Components/Cf';
import { Card, ListItem, Button, ButtonGroup } from 'react-native-elements'

class TrustRequest extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'ADD BENEFACTOR',
    headerTintColor: '#FFF',
    headerTitleStyle: {
      fontSize: 15
    },
    headerStyle: {
      elevation: 0,
      shadowOpacity: 0,
      backgroundColor: '#00A134',
      borderBottomWidth: 0
    }
  })

  constructor(props) {
    super(props);
    this.state = {
      benefactors: 0,
      amount: 0,
      amountText: '',
      tenure: 0,
      modalVisible: false,
      modalAlertVisible: false,
      modalAlertTitle: '',
      modalAlertMessage: '',

      selectedIndex: null
    }

    this.updateIndex = this.updateIndex.bind(this)
    this.updateAmount = this.updateAmount.bind(this)
    this.amountChange = this.amountChange.bind(this)
    this.setModalVisible = this.setModalVisible.bind(this)
    this.handlePreview = this.handlePreview.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)

  }

  componentWillMount() {
    /* const { navigation } = this.props;
    let amount = navigation.getParam('amount', 0);
    amount = parseFloat(amount);
    this.setState({ amount }); */
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  closeModal() {
    this.setModalVisible(!this.state.modalVisible)
  }

  updateIndex(selectedIndex) {
    this.setState({ selectedIndex, benefactors: (selectedIndex + 1) })
  }

  goBack() {
    const { goBack } = this.props.navigation;
    goBack();
  }

  updateAmount(type) {
    let amount = this.state.amount;
    amount = type === 1 ? amount + 1 : (amount > 0 ? amount - 1 : amount);
    this.setState({ amount })
  }

  updateTenure(type) {
    let tenure = this.state.tenure;
    tenure = type === 1 ? tenure + 1 : (tenure > 0 ? tenure - 1 : tenure);
    this.setState({ tenure })
  }

  amountChange(amount) {
    amount = amount.replace(/[^0-9]/g, '');
    let amountText = amount;
    amount = amount === '' ? 0 : parseInt(amount);
    this.setState({ amount, amountText })
  }


  handlePreview() {
    this.setModalVisible(true);
  }


  handleSubmit() {
    this.props.screenProps.setSpinner(true);

    const postData = {
      amount: this.state.amount,
      benefactors: this.state.benefactors,
      tenure: this.state.tenure,
      type: 3,
      request_type: 6,
    }
    console.log(postData, 'postData....')

    this.props.screenProps.requestAdd(postData)
      .then(response => {
        return response
      })
      .then(() => {
        //const { email } = response.data.data;
        this.setState({ modalVisible: false });
        //console.log(response.data.data, 'response 0000')
        this.props.navigation.navigate('Success', {
          title: 'Awesome!',
          message: `Your request has been sent.\nYour account officer will contact you upon approval.`,
          goto: 'TrustFunds',
          callback: { reload: true }
        })

      })
      .catch((error) => {
        this.props.screenProps.setError(error);
      }).then(() => {
        this.props.screenProps.setSpinner(false);
      })
  }


  render() {
    const buttons = ['1', '2', '3', '4']
    const { selectedIndex, amount, tenure, amountText, benefactors } = this.state

    return (<Fragment>

      <Modal
        presentationStyle={`overFullScreen`}
        animationType="slide"
        transparent={true}
        visible={this.state.modalVisible}
        onRequestClose={() => { }}
      >
        <View style={{ backgroundColor: 'rgba(0,0,0,.5)', flex: 1, justifyContent: 'flex-end' }}>
          <View
            style={{ backgroundColor: '#fff', minHeight: 350, maxHeight: '100%', margin: 20, borderRadius: 7 }}>



            <View style={[_sty.rowSpace, { paddingHorizontal: 15, paddingVertical: 10 }]}>
              <Text
                style={[_sty.py5, _sty.fontSize22,
                _sty.textBlack, _sty.fontRegular]}>Let's go over your request</Text>

              <TouchableOpacity onPress={() => this.setModalVisible(!this.state.modalVisible)}>
                <Icon name="ios-close" size={30} color='#999' />
              </TouchableOpacity>

            </View>
            <View style={[_sty.rowPx, _sty.py15, _sty.flex]}>

              <Card
                containerStyle={{ backgroundColor: 'rgba(0,161,52, .1)', elevation: 0, borderWidth: 0, borderRadius: 5, overflow: "hidden", paddingVertical: 0, margin: 0 }} >


                <View
                  style={[_sty.rowSpace, { borderBottomWidth: 1, borderBottomColor: '#C5E9CF', paddingVertical: 13 }]}>
                  <Text style={[_sty.textTheme]}>Number of Benefactors</Text>
                  <Text style={[_sty.textTheme]}>{benefactors}</Text>
                </View>

                <View
                  style={[_sty.rowSpace, { borderBottomWidth: 1, borderBottomColor: '#C5E9CF', paddingVertical: 13 }]}>
                  <Text style={[_sty.textTheme]}>Target Trust Fund Amount</Text>
                  <Text style={[_sty.textTheme]}><Cf value={parseFloat(amount)} /></Text>
                </View>

                <View
                  style={[_sty.rowSpace, { borderBottomWidth: 0, borderBottomColor: '#C5E9CF', paddingVertical: 13 }]}>
                  <Text style={[_sty.textTheme]}>Proposed Tenure</Text>
                  <Text style={[_sty.textTheme]}>{tenure} Years</Text>
                </View>



              </Card>


              <View style={[_sty.vBottom]}>
                <View style={[_sty.rowSpace]}>
                  <TouchableOpacity onPress={this.handleSubmit}
                    style={[_sty.btn, _sty.alignSelfCenter, _sty.bgTheme]}>
                    <Text style={[_sty.btnText, _sty.textWhite]}>Accept</Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    onPress={this.closeModal}
                    style={[_sty.btn, _sty.alignSelfCenter, _sty.bgDanger]}>
                    <Text style={[_sty.btnText, _sty.textWhite]}>Cancel</Text>
                  </TouchableOpacity>
                </View>
              </View>

            </View>

          </View>
        </View>
      </Modal>


      <View style={[_sty.containerFlex, _sty.bgThemeTrans]}>

      <KeyboardAvoidingView behavior="padding" enabled>
      <SafeAreaView>
        

          <View style={[_sty.containerMdx, { paddingVertical: 25 }]}>

            <Card
              containerStyle={[_sty.boxShadow, _sty.mb30, { backgroundColor: '#ffffff', borderWidth: 0, borderRadius: 5, overflow: "hidden", paddingVertical: 15, margin: 0 }]} >

              <Text style={[_sty.fontWeightSemiBold, _sty.textTheme, _sty.fontSize16, _sty.mb10]}>Number of Benefactors</Text>

              <ButtonGroup
                onPress={this.updateIndex}
                selectedIndex={selectedIndex}
                buttons={buttons}
                innerBorderStyle={{ width: 0, color: 'transparent' }}
                containerStyle={{ height: 35, marginBottom: 0, marginTop: 0, marginLeft: 0, marginRight: 0, borderWidth: 0 }}
                buttonStyle={{ backgroundColor: '#F7F7F7', borderWidth: 0, borderRadius: 50, /* justifyContent: 'center', alignSelf: 'center', */ height: 35, width: 35 }}
                selectedButtonStyle={{ backgroundColor: '#00A134' }}
                textStyle={{ justifyContent: 'center', alignSelf: 'center', fontWeight: '600' }}
              />

            </Card>



            <Card
              containerStyle={[_sty.boxShadow, _sty.mb30, { backgroundColor: '#ffffff', borderWidth: 0, borderRadius: 5, overflow: "hidden", paddingVertical: 15, margin: 0 }]} >

              <Text style={[_sty.fontWeightSemiBold, _sty.textTheme, _sty.fontSize16, _sty.mb10]}>Target Trust Fund Amount</Text>

              <View style={styles.inputContainer}>
                <View style={{ width: '100%', height: 25 }} >
                  <TextInput
                    style={{ height: 25, borderColor: 'transparent', borderWidth: 0, fontSize: 25, fontWeight: '600', margin: 4 }}
                    onChangeText={this.amountChange}
                    keyboardType={'numeric'}
                    placeholder="0"
                    placeholderTextColor="#000000"
                    value={`${amountText}`}
                    numeric
                  />
                </View>
              </View>



            </Card>



            <Card
              containerStyle={[_sty.boxShadow, _sty.mb30, { backgroundColor: '#ffffff', borderWidth: 0, borderRadius: 5, overflow: "hidden", paddingVertical: 15, margin: 0 }]} >

              <Text style={[_sty.fontWeightSemiBold, _sty.textTheme, _sty.fontSize16, _sty.mb10]}>Proposed Tenure (Years)</Text>


              <View style={styles.inputContainer}>
                <View style={{ width: '75%', height: 25 }} >
                  <Text style={[_sty.fontSize25, _sty.fontWeightSemiBold, { lineHeight: 26 }]}>
                    {parseInt(tenure)}
                  </Text>
                </View>


                <View style={{ width: '25%', height: 25, position: 'relative' }}>
                  <TouchableOpacity onPress={() => this.updateTenure(1)} style={[styles.iconBox, { right: 35 }]}>
                    <Icon name="ios-add" size={15} color='#000000' />
                  </TouchableOpacity>

                  <TouchableOpacity onPress={() => this.updateTenure(0)} style={[styles.iconBox, { right: 0 }]}>
                    <Icon name="ios-remove" size={15} color='#000000' />
                  </TouchableOpacity>
                </View>
              </View>



            </Card>


          </View>

          <View style={[_sty.vBottom, _sty.mb40]}>
            <ElButton
              title="Proceed"
              onPress={this.handlePreview}
              shadow={true}
              width={70}
              bg="#00A134"
              textColor="white"
              disabled={(benefactors === 0 || amount === 0 || tenure === 0) ? true : false}
            />
          </View>





        

        </SafeAreaView>
      </KeyboardAvoidingView>


      </View>

    </Fragment>
    );
  }
}

const styles = StyleSheet.create({



  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
  },
  inputContainer: {
    backgroundColor: '#F7F7F7',
    height: 35,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 5,
    borderRadius: 5
  },
  iconBox: {
    backgroundColor: '#EAEAEA',
    width: 25,
    height: 25,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute'
  }

});


export default TrustRequest;