import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  StatusBar,
  Image,
  Linking,
  SafeAreaView,
  Switch,
  TouchableOpacity,
  ScrollView,
  TouchableHighlight,
  Alert
} from "react-native";
import Version from "./../../Components/Version";
import S from "./Profile.style";
import _sty from "../../assets/css/Styles";
import { TextField } from "react-native-material-textfield";

class ChangePassword extends Component {
  static navigationOptions = ({ navigation }) => ({
    drawerLockMode: "locked-closed",
    title: "CHANGE PIN",
    headerTintColor: "#666",
    headerTitleStyle: {
      fontSize: 15
    },
    headerStyle: {
      elevation: 0,
      shadowOpacity: 0,
      backgroundColor: "#F7F7F7",
      borderBottomWidth: 1,
      borderBottomColor: "#D7D7D7"
    }
  });

  constructor(props) {
    super(props);

    this.state = {
      current_pin: "",
      new_pin: "",
      confirm_new_pin: ""
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleSubmit() {
    const postData = {
      current_pin: this.state.current_pin,
      new_pin: this.state.new_pin,
      confirm_new_pin: this.state.confirm_new_pin
    };

    console.log(postData, 'change pass....');

    this.props.screenProps.setSpinner(true);

    this.props.screenProps
      .changePin(postData)
      .then(response => {
        return response;
      })
      .then(async response => {
        this.props.screenProps.setLogout();

      })
      .catch(error => {
        //console.log(JSON.stringify(error), "errrrr 0000");
        //console.log(error.response.status, "errrrr 0000....");
        this.props.screenProps.setError(error);
      })
      .then(() => {
        this.props.screenProps.setSpinner(false);
      });
  }

  /* amountChange(amount) {
    amount = amount.replace(/[^0-9]/g, '');
    let amountText = amount;
    amount = amount === '' ? 0 : parseInt(amount);
    this.setState({ amount, amountText })
  } */

  handleChange(name, value) {
    //const { name, value } = e.target
    value = value.replace(/[^0-9]/g, '');
    this.setState({ [name]: value })
  }

  //<Text style={[_sty.headingH4, _sty.textGreen]}>Personal Information</Text>
  render() {
    const { current_pin, new_pin, confirm_new_pin } = this.state;
    return (
      <View style={[_sty.containerDefault, _sty.containerPadV]}>
        <View style={{ flex: 10 }}>
          <ScrollView>
            <Text
              style={[_sty.headingH4, _sty.textGreen, _sty.containerPadH]}
            />

            <View style={_sty.formGroupMat}>
              <TextField
                textContentType={"password"}
                secureTextEntry={true}
                inputContainerPadding={5}
                lineWidth={0}
                activeLineWidth={0}
                inputContainerStyle={{ paddingHorizontal: 20 }}
                tintColor={"#999"}
                labelTextStyle={{
                  paddingHorizontal: 20
                }}
                label="Current Pin"
                value={current_pin}

                onChangeText={current_pin => this.handleChange('current_pin', current_pin)}
                maxLength={5}
                keyboardType={'numeric'}
                numeric
              />
            </View>

            <View style={_sty.formGroupMat}>
              <TextField
                textContentType={"password"}
                secureTextEntry={true}
                inputContainerPadding={5}
                lineWidth={0}
                activeLineWidth={0}
                inputContainerStyle={{ paddingHorizontal: 20 }}
                tintColor={"#999"}
                labelTextStyle={{
                  paddingHorizontal: 20
                }}
                label="New Pin"
                value={new_pin}

                onChangeText={new_pin => this.handleChange('new_pin', new_pin)}
                maxLength={5}
                keyboardType={'numeric'}
                numeric
              />
            </View>

            <View style={_sty.formGroupMat}>
            <TextField
                textContentType={"password"}
                secureTextEntry={true}
                inputContainerPadding={5}
                lineWidth={0}
                activeLineWidth={0}
                inputContainerStyle={{ paddingHorizontal: 20 }}
                tintColor={"#999"}
                labelTextStyle={{
                  paddingHorizontal: 20
                }}
                label="Confirm New Pin"
                value={confirm_new_pin}

                onChangeText={confirm_new_pin => this.handleChange('confirm_new_pin', confirm_new_pin)}
                maxLength={5}
                keyboardType={'numeric'}
                numeric
              />
            </View>

            {/* <Text style={[_sty.headingH4, _sty.textGreen, _sty.containerPadH]}>
              Financial Information
            </Text>

            <View style={_sty.summaryBox}>
              <Text style={_sty.summaryBoxLabel}>Bank Account Number</Text>
              <Text style={_sty.summaryBoxText}>1234567890</Text>
            </View>

            <View style={[_sty.summaryBox, _sty.mb40]}>
              <Text style={_sty.summaryBoxLabel}>Bank Name</Text>
              <Text style={_sty.summaryBoxText}>Zenith Bank PLC</Text>
            </View> */}
          </ScrollView>
        </View>

        <View
          style={{
            flex: 2,
            justifyContent: "flex-start",
            paddingTop: 20,
            alignItems: "center"
          }}
        >
          <TouchableHighlight
            onPress={this.handleSubmit}
            style={[_sty.btnThemeGreen]}
          >
            <Text style={[_sty.btnTextWhite, _sty.fontBold]}>Submit</Text>
          </TouchableHighlight>
        </View>

        <Version />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    padding: 20
  },
  heading: {
    textTransform: "uppercase",
    color: "#000",
    fontWeight: "bold",
    marginBottom: 20
  },
  p: {
    marginBottom: 25,
    lineHeight: 19,
    color: "#333"
  }
});

export default ChangePassword;
