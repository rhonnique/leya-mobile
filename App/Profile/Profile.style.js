
export default {
    container: {
        flex: 1,
        backgroundColor: '#f1f1f1',
        paddingTop: 40
    },
    listView: {
        padding: 20,
        backgroundColor: '#fff',
        borderBottomColor: '#f1f1f1',
        borderBottomWidth: 2,
        position: 'relative',
        flexDirection: 'row'
    },
    listViewText: {
        color: '#000',
        fontWeight: '500',
        display:'flex',
        fontSize: 14
    },
    listViewSwitch: {
        position: 'absolute',
        right: 20,
        top: 15
    }
};