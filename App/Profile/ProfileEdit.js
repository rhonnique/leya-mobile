import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  StatusBar,
  Image,
  Linking,
  SafeAreaView,
  Switch,
  TouchableOpacity,
  ScrollView,
  TouchableHighlight,
  Alert
} from "react-native";
import Version from "./../../Components/Version";
import S from "./Profile.style";
import _sty from "../../assets/css/Styles";
import { TextField } from "react-native-material-textfield";

class ProfileEdit extends Component {
  static navigationOptions = ({ navigation }) => ({
    drawerLockMode: "locked-closed",
    title: "EDIT PROFILE",
    headerTintColor: "#666",
    headerTitleStyle: {
      fontSize: 15
    },
    headerStyle: {
      elevation: 0,
      shadowOpacity: 0,
      backgroundColor: "#F7F7F7",
      borderBottomWidth: 1,
      borderBottomColor: "#D7D7D7"
    }
  });

  constructor(props) {
    super(props);

    const { id, name, phone, email } = this.props.screenProps.customer;
    this.state = {
      id: id,
      name: name,
      phone: phone,
      email: email 
    };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit() {
    const postData = {
        'id': this.state.id,
        'name': this.state.name,
        'phone': this.state.phone
    }

    this.props.screenProps.setSpinner(true);

    this.props.screenProps.customerEdit(postData)
        .then(response => {
            return response
        })
        .then( async (response) => {
          const { customer } = response.data;
          await this.props.screenProps.customerSet(customer);

            this.props.navigation.navigate('Success', {
                title: 'Congratulations',
                message: 'Your profile was successfully updated',
                goto: 'Profile'
            })

        })
        .catch((error) => {
          this.props.screenProps.setError(error);
        }).then(() => {
            this.props.screenProps.setSpinner(false);
        })
}

  //<Text style={[_sty.headingH4, _sty.textGreen]}>Personal Information</Text>
  render() {
    const { name, phone, email } = this.state;
    return (
      <View style={[_sty.containerDefault, _sty.containerPadV]}>
        <View style={{ flex: 10 }}>
          <ScrollView>
            <Text style={[_sty.headingH4, _sty.textGreen, _sty.containerPadH]}>
              Personal Information
            </Text>

            <View style={_sty.formGroupMat}>
              <TextField
                inputContainerPadding={5}
                lineWidth={0}
                activeLineWidth={0}
                inputContainerStyle={{ paddingHorizontal: 20 }}
                tintColor={"#999"}
                labelTextStyle={{
                  paddingHorizontal: 20
                }}
                label="Full Name"
                value={name}
                onChangeText={name => this.setState({ name })}
              />
            </View>

            <View style={_sty.formGroupMat}>
              <TextField
                inputContainerPadding={5}
                lineWidth={0}
                activeLineWidth={0}
                inputContainerStyle={{ paddingHorizontal: 20 }}
                tintColor={"#999"}
                labelTextStyle={{
                  paddingHorizontal: 20
                }}
                label="Email Address"
                value={email}
                onChangeText={email => this.setState({ email })}
                disabled={true}
                disabledLineWidth={0}
              />
            </View>

            <View style={_sty.formGroupMat}>
              <TextField
                inputContainerPadding={5}
                lineWidth={0}
                activeLineWidth={0}
                inputContainerStyle={{ paddingHorizontal: 20 }}
                tintColor={"#999"}
                labelTextStyle={{
                  paddingHorizontal: 20
                }}
                label="Phone Number"
                value={phone}
                onChangeText={phone => this.setState({ phone })}
              />
            </View>

            {/* <Text style={[_sty.headingH4, _sty.textGreen, _sty.containerPadH]}>
              Financial Information
            </Text>

            <View style={_sty.summaryBox}>
              <Text style={_sty.summaryBoxLabel}>Bank Account Number</Text>
              <Text style={_sty.summaryBoxText}>1234567890</Text>
            </View>

            <View style={[_sty.summaryBox, _sty.mb40]}>
              <Text style={_sty.summaryBoxLabel}>Bank Name</Text>
              <Text style={_sty.summaryBoxText}>Zenith Bank PLC</Text>
            </View> */}
          </ScrollView>
        </View>

        <View
          style={{
            flex: 2,
            justifyContent: "flex-start",
            paddingTop: 20,
            alignItems: "center"
          }}
        >
          <TouchableHighlight
            onPress={this.handleSubmit}
            style={[_sty.btnThemeGreen]}
          >
            <Text style={_sty.btnTextWhite}>Submit</Text>
          </TouchableHighlight>
        </View>

        <Version />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    padding: 20
  },
  heading: {
    textTransform: "uppercase",
    color: "#000",
    fontWeight: "bold",
    marginBottom: 20
  },
  p: {
    marginBottom: 25,
    lineHeight: 19,
    color: "#333"
  }
});

export default ProfileEdit;
