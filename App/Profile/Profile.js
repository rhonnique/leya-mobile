import React, { Component } from 'react';
import {
  Text, View, StyleSheet, StatusBar, Image, Linking,
  SafeAreaView, Switch, TouchableOpacity, ScrollView, TouchableHighlight
} from 'react-native';
import Version from './../../Components/Version';
import S from './Profile.style';
import _sty  from '../../assets/css/Styles';


class Profile extends Component {
  static navigationOptions = ({ navigation }) => ({
    drawerLockMode: "locked-closed",
    title: 'PROFILE',
    headerTintColor: '#666',
    headerTitleStyle: {
      fontSize: 15
    },
    headerStyle: {
      elevation: 0,
      shadowOpacity: 0,
      backgroundColor: '#F7F7F7',
      borderBottomWidth: 1,
      borderBottomColor: '#D7D7D7'
    }
  })

  constructor(props) {
    super(props);

    const { name, phone, email } = this.props.screenProps.customer;
    this.state = {
      name: name,
      phone: phone,
      email: email 
    };

  }

  //<Text style={[_sty.headingH4, _sty.textGreen]}>Personal Information</Text>
  render() {
    const { name, phone, email } = this.state;
    const { navigate } = this.props.navigation;
    return (
      <View style={[_sty.containerDefault, _sty.containerPadV]}>
        <View style={{ flex: 10 }}>
          <ScrollView>
            <Text style={[_sty.headingH4, _sty.textGreen, _sty.containerPadH]}>Personal Information</Text>

            <View style={_sty.summaryBox}>
              <Text style={_sty.summaryBoxLabel}>Full Name</Text>
              <Text style={_sty.summaryBoxText}>{name}</Text>
            </View>

            <View style={_sty.summaryBox}>
              <Text style={_sty.summaryBoxLabel}>Email Address</Text>
              <Text style={_sty.summaryBoxText}>{email}</Text>
            </View>

            <View style={[_sty.summaryBox, _sty.mb40]}>
              <Text style={_sty.summaryBoxLabel}>Phone Number</Text>
              <Text style={_sty.summaryBoxText}>{phone}</Text>
            </View>

            <Text style={[_sty.headingH4, _sty.textGreen, _sty.containerPadH]}>Financial Information</Text>

            <View style={_sty.summaryBox}>
              <Text style={_sty.summaryBoxLabel}>Bank Account Number</Text>
              <Text style={_sty.summaryBoxText}>1234567890</Text>
            </View>

            <View style={[_sty.summaryBox, _sty.mb40]}>
              <Text style={_sty.summaryBoxLabel}>Bank Name</Text>
              <Text style={_sty.summaryBoxText}>Zenith Bank PLC</Text>
            </View>
          </ScrollView>

        </View>

        <View style={{ flex: 2, justifyContent: 'flex-start', paddingTop: 20, alignItems: 'center' }}>
          <TouchableHighlight onPress={()=> Linking.openURL(`tel:+2349060003039`)}
            style={[_sty.btnThemeGreen]}>
            <Text style={_sty.btnTextWhite}>Call to update profile</Text>
          </TouchableHighlight>
        </View>

        <Version />

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    padding: 20
  },
  heading: {
    textTransform: 'uppercase',
    color: '#000',
    fontWeight: 'bold',
    marginBottom: 20
  },
  p: {
    marginBottom: 25,
    lineHeight: 19,
    color: '#333'
  }
});

export default Profile;