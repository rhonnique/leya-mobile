import {
  createStackNavigator, 
  createAppContainer, 
  createDrawerNavigator
} from 'react-navigation';
import { Dimensions } from 'react-native';


import DrawerMenu from './DrawerMenu'
import StackNav from './StackNav';

const drawernav = createDrawerNavigator({
  Item1: {
      screen: StackNav,
    }
  }, {
    contentComponent: DrawerMenu,
    drawerWidth: Dimensions.get('window').width - 70,  
});

export default createAppContainer(drawernav);
