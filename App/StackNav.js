import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View, TouchableOpacity, Image
} from 'react-native';

import { createStackNavigator, createBottomTabNavigator, DrawerActions } from 'react-navigation';
import Icon from "react-native-vector-icons/Ionicons";

// Dashboard
import Investment from './Dashboard/Investment'
import Beans from './Dashboard/Beans'
import TrustFunds from './Dashboard/TrustFunds'

// Success
import Success from './../Notifications/Success'

// Settings
import Settings from './Settings/Settings'

// Help
import Help from './Help/Help'

// Profile
import Profile from './Profile/Profile'
import ProfileEdit from './Profile/ProfileEdit'
import ChangePassword from './Profile/ChangePassword'

// Transactions
import Transactions from './Transactions/Transactions'
import TransInvestment from './Transactions/TransInvestment'
import TransLoans from './Transactions/TransLoans'
import TransTrustFunds from './Transactions/TransTrustFunds'

import Terms from './Terms'
import Privacy from './Privacy'
import Faq from './Faq'

import InvestmentRequest from './Investments/InvestmentRequest'
import InvestmentRequestSummary from './Investments/InvestmentRequestSummary'

import LoanRequest from './Loans/LoanRequest'
import LoanType from './Loans/LoanType'
import LoanCycle from './Loans/LoanCycle'
import LoanRequestSummary from './Loans/LoanRequestSummary';

import TrustRequest from './Trust/TrustRequest'

const Dashboard = createBottomTabNavigator({
  Investment: { screen: Investment },
  Beans: { screen: Beans },
  TrustFunds: { screen: TrustFunds },
}, {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        switch (routeName) {
          case 'Investment':
            return focused ? (
              <Image style={{ resizeMode: 'cover', width: 31, height: 35 }} source={require(`./../assets/images/tab-icon-1-active.png`)} />
            ) : (
                <Image style={{ resizeMode: 'cover', width: 31, height: 35 }} source={require(`./../assets/images/tab-icon-1.png`)} />
              );
            break;

          case 'Beans':
            return focused ? (
              <Image style={{ resizeMode: 'cover', width: 31, height: 28, marginTop: 5 }} source={require(`./../assets/images/tab-icon-2-active.png`)} />
            ) : (
                <Image style={{ resizeMode: 'cover', width: 31, height: 28, marginTop: 5 }} source={require(`./../assets/images/tab-icon-2.png`)} />
              );
            break;

          case 'TrustFunds':
            return focused ? (
              <Image style={{ resizeMode: 'cover', width: 34, height: 25 }} source={require(`./../assets/images/tab-icon-3-active.png`)} />
            ) : (
                <Image style={{ resizeMode: 'cover', width: 34, height: 25 }} source={require(`./../assets/images/tab-icon-3.png`)} />
              );
            break;

          default:
            return focused ? (
              <Image style={{ resizeMode: 'cover', width: 31, height: 35 }} source={require(`./../assets/images/tab-icon-1-active.png`)} />
            ) : (
                <Image style={{ resizeMode: 'cover', width: 31, height: 35 }} source={require(`./../assets/images/tab-icon-1.png`)} />
              );
        }
      },
    }),
    tabBarOptions: {
      activeTintColor: '#00A134',
      inactiveTintColor: '#BABABA',
      style: {
        borderTopColor: '#EAEAEA',
        height: 60,
        paddingBottom: 5
      },
    }

  })






const StackNav = createStackNavigator({

  Main: {
    screen: Dashboard,
    navigationOptions: ({ navigation }) => {
      const { index } = navigation.state;
      let title = '';
      switch(index){
        case 0:
        title = 'PORTFOLIO OVERVIEW';
        break;
        case 1:
        title = 'LOAN OVERVIEW';
        break;
        case 2:
        title = 'TRUST FUND OVERVIEW';
        break;
      }
      return {
        title: title,
        headerTintColor: '#fff',
        headerTitleStyle: {
          width: '100%',
          textAlign: 'center',
          alignSelf: 'center', flex: 1, fontSize: 15
        },
        headerLeft: (
          <TouchableOpacity style={{ marginLeft: 15 }}
            onPress={() => navigation.dispatch(DrawerActions.openDrawer())}>
            <Icon name="ios-menu" size={30} color='#fff' />
          </TouchableOpacity>
        ),
        headerRight: (
          <TouchableOpacity style={{ marginRight: 15 }}
            onPress={() => navigation.dispatch(DrawerActions.openDrawer())}>
            <Icon name="md-notifications" size={30} color='#fff' />
          </TouchableOpacity>
        ),
        headerStyle: {
          elevation: 0,
          shadowOpacity: 0,
          backgroundColor: '#00A134',
          borderBottomWidth: 0
        }
      };
    }
  },
  Success: {
    screen: Success
  },

  Settings: {
    screen: Settings,
    contentOptions: {
      inactiveBackgroundColor: '#000000',
    }
  },

  Help: {
    screen: Help,
  },

  Profile: { screen: Profile, },
  ProfileEdit: { screen: ProfileEdit, },
  ChangePassword: { screen: ChangePassword, },

  Transactions: {
    screen: Transactions,
  },
  TransInvestment: {
    screen: TransInvestment,
  },
  TransLoans: {
    screen: TransLoans,
  },
  TransTrustFunds: {
    screen: TransTrustFunds,
  },

  Terms: {
    screen: Terms
  },
  Privacy: {
    screen: Privacy
  },
  Faq: {
    screen: Faq
  },

  InvestmentRequest: { screen: InvestmentRequest },
  InvestmentRequestSummary: { screen: InvestmentRequestSummary },

  LoanRequest: { screen: LoanRequest },
  LoanType: { screen: LoanType },
  LoanCycle: { screen: LoanCycle },
  LoanRequestSummary: { screen: LoanRequestSummary },

  TrustRequest: { screen: TrustRequest },



});

export default StackNav;