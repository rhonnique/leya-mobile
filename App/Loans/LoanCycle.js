import React, { Component, Fragment } from 'react';
import { Text, View, StyleSheet, Alert, Modal, TouchableOpacity } from 'react-native';
//import Modal from "react-native-modal";
import { ModalAlert } from '../../Components/ModalAlert';
import moment from "moment";

import Icon from "react-native-vector-icons/Ionicons";
import _sty from '../../assets/css/Styles';
import { ElButton } from '../../Components/Elements';

import Cf from '../../Components/Cf';
import { Card, ListItem, Button } from 'react-native-elements'
import { Picker } from 'native-base';


const cycles = () => {
  return ['3', '4', '5', '6', '7', '8', '9', '10', '11', '12']
}

class LoanCycle extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'REQUEST LOAN',
    headerTintColor: '#FFF',
    headerTitleStyle: {
      fontSize: 15
    },
    headerStyle: {
      elevation: 0,
      shadowOpacity: 0,
      backgroundColor: '#00A134',
      borderBottomWidth: 0
    }
  })

  constructor(props) {
    super(props);
    this.state = {
      cycle: 3,
      type: 0,
      amount: 0,
      interest_amount: 0,
      interest_rate: 0,
      tenure: 0,
      modalVisible: false,
      modalAlertVisible: false,
      modalAlertTitle: '',
      modalAlertMessage: '',
      topup: false
    }

    this.handleSubmit = this.handleSubmit.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.handlePreview = this.handlePreview.bind(this)
    this.modalAlertSet = this.modalAlertSet.bind(this)

  }

  componentWillMount() {
    const { navigation } = this.props;

    let type = navigation.getParam('type', 0);
    type = parseInt(type + 1);

    let amount = navigation.getParam('amount', 0);
    amount = parseFloat(amount);

    let interest_rate = this.props.screenProps.settings.loan_interest_rate;
    interest_rate = parseInt(interest_rate);

    let interest_amount = (amount / 100) * interest_rate;
    interest_amount = parseFloat(interest_amount);

    const topup = navigation.getParam('topup', false);

    this.setState({ amount, type, topup });

    /* if (topup) { 
      this.setModalVisible(true);
    } */
  }

  componentWillUnmount() {
    this.setState({ modalVisible: false });
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  closeModal() {
    this.setModalVisible(!this.state.modalVisible)
  }

  goBack() {
    const { goBack } = this.props.navigation;
    goBack();
  }

  handleSubmit() {
    this.setModalVisible(false);
    this.props.screenProps.setSpinner(true);
    const postData = {
      amount: this.state.amount,
      tenure: this.state.cycle,
      type: 2,
      request_type: 4,
      loan_type: this.state.type,
      loan_topup: (this.state.topup ? 1 : 0),
    }

    console.log(postData, 'post loan data......');

    this.props.screenProps.requestAdd(postData)
      .then(response => {
        return response
      })
      .then(response => {
        //const { email } = response.data.data;
        this.setState({ modalVisible: false });
        //console.log(response.data.data, 'response 0000')
        this.props.navigation.navigate('Success', {
          title: 'Awesome!',
          message: `Your request has been sent.\nYour account officer will contact you upon approval.`,
          goto: 'Beans',
          callback: { reload: true }
        })

      })
      .catch((error) => {
        this.props.screenProps.setError(error);
      }).then(() => {
        this.props.screenProps.setSpinner(false);
      })
  }

  handlePreview() {
    this.setModalVisible(true)
  }

  modalAlertSet(status) {
    this.setState({ modalAlertVisible: status })
  }

  render() {
    const { type, amount, cycle } = this.state;
    return (<Fragment>

      <ModalAlert
        visible={this.state.modalAlertVisible}
        onClosePress={this.modalAlertSet}
        title={this.state.modalAlertTitle}
        message={this.state.modalAlertMessage}
      />

      <Modal
        presentationStyle={`overFullScreen`}
        animationType="slide"
        transparent={true}
        visible={this.state.modalVisible}
        onRequestClose={() => { }}
      >
        <View style={{ backgroundColor: 'rgba(0,0,0,.5)', flex: 1, justifyContent: 'flex-end' }}>
          <View
            style={{ backgroundColor: '#fff', minHeight: 350, maxHeight: '100%', margin: 20, borderRadius: 7 }}>



            <View style={[_sty.rowSpace, { paddingHorizontal: 15, paddingVertical: 10 }]}>
              <Text
                style={[_sty.py5, _sty.fontSize22,
                _sty.textBlack, _sty.fontWeightSemiBold]}>Let's go over your request</Text>

              <TouchableOpacity onPress={() => this.setModalVisible(!this.state.modalVisible)}>
                <Icon name="ios-close" size={30} color='#999' />
              </TouchableOpacity>

            </View>
            <View style={[_sty.rowPx, _sty.py15, _sty.flex]}>

              <Card
                containerStyle={{ backgroundColor: 'rgba(0,161,52, .1)', elevation: 0, borderWidth: 0, borderRadius: 5, overflow: "hidden", paddingVertical: 0, margin: 0 }} >

                <View
                  style={[_sty.rowSpace, { borderBottomWidth: 1, borderBottomColor: '#C5E9CF', paddingVertical: 13 }]}>
                  <Text style={[_sty.textTheme]}>Loan amount requested</Text>
                  <Text style={[_sty.textTheme]}><Cf value={parseFloat(amount)} /></Text>
                </View>

                <View
                  style={[_sty.rowSpace, { borderBottomWidth: 1, borderBottomColor: '#C5E9CF', paddingVertical: 13 }]}>
                  <Text style={[_sty.textTheme]}>Loan Type</Text>
                  <Text style={[_sty.textTheme]}>{type === 1 ? 'Retail' : 'Commercial'}</Text>
                </View>

                <View
                  style={[_sty.rowSpace, { borderBottomWidth: 0, borderBottomColor: '#C5E9CF', paddingVertical: 13 }]}>
                  <Text style={[_sty.textTheme]}>Loan Tenor</Text>
                  <Text style={[_sty.textTheme]}>{cycle} Cycles</Text>
                </View>

              </Card>


              <View style={[_sty.vBottom]}>
                <View style={[_sty.rowSpace]}>
                  <TouchableOpacity onPress={this.handleSubmit}
                    style={[_sty.btn, _sty.alignSelfCenter, _sty.bgTheme]}>
                    <Text style={[_sty.btnText, _sty.textWhite]}>Accept</Text>
                  </TouchableOpacity>

                  <TouchableOpacity /* onPress={this.state.tenure === 0 ? this.goBack() : this.setModalVisible(!this.state.modalVisible)}  */
                    onPress={this.closeModal}
                    style={[_sty.btn, _sty.alignSelfCenter, _sty.bgDanger]}>
                    <Text style={[_sty.btnText, _sty.textWhite]}>Cancel</Text>
                  </TouchableOpacity>
                </View>
              </View>

            </View>

          </View>
        </View>
      </Modal>

      <View style={[_sty.containerFlex]}>


        <View style={[_sty.rows, _sty.bgTheme, { paddingVertical: 25 }]}>
          <View style={[_sty.containerMdx]}>
            <Text style={[_sty.fontSize20, _sty.textWhite]}>How many cycles?</Text>
          </View>
        </View>


        <View style={[_sty.containerMdx, { paddingVertical: 25 }]}>

          {/* Cycles Selected */}
          <View style={[_sty.rowSpace, _sty.mb20]}>
            <View><Text style={[_sty.py5, _sty.fontSize18]}>Number of cycles</Text></View>
            <View style={{ height: 40, width: 160, borderWidth: 2, borderColor: 'green', borderRadius: 5, overflow: 'hidden' }}>
              <Picker
                selectedValue={this.state.cycle}
                style={styles.itemStyle}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({ cycle: itemValue })
                }>
                {cycles().map((row, index) => <Picker.Item key={index} label={row} value={row} />)}
              </Picker>
            </View>
          </View>



        </View>

        <View style={[_sty.vBottom, _sty.mb40]}>
          <ElButton
            title="Proceed"
            onPress={this.handlePreview}
            shadow={true}
            width={70}
            bg="#00A134"
            textColor="white"
          />
        </View>





      </View>

    </Fragment>
    );
  }
}

const styles = StyleSheet.create({

  itemStyle: {
    height: 36,
    color: 'black'
  },
  picker: {
    width: 100
  },

  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
  },
  showPickerBtn: {
    height: 44,
    backgroundColor: '#973BC2',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 16,
    borderRadius: 6,
  },
  yearMonthText: {
    fontSize: 20,
    marginTop: 12
  },

});


export default LoanCycle;