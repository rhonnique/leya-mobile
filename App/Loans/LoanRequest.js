import React, { Component } from 'react';
import {
  Text, View, StyleSheet, StatusBar, Image, Alert,
  SafeAreaView, Switch, TouchableOpacity, ScrollView
} from 'react-native';
import VirtualKeyboard from 'react-native-virtual-keyboard';
import Cf from '../../Components/Cf';
import _sty from '../../assets/css/Styles';
import Alerts from '../../Components/Alerts';


import { Card, ListItem, Button, ButtonGroup } from 'react-native-elements'

const ForgotLink = () => {
  return (<Text style={[_sty.textWhite, _sty.fontSize18]}>Cancel</Text>)
}

class LoanRequest extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'REQUEST LOAN',
      headerTintColor: '#FFF',
      headerTitleStyle: {
        fontSize: 15
      },
      headerStyle: {
        elevation: 0,
        shadowOpacity: 0,
        backgroundColor: '#00A134',
        borderBottomWidth: 0
      }
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      amount: '0',
      setAlert: false,
      alertText: '',
      alertType: _sty.alertWarning,
      data_loan: null,
    };

    this.handeleSubmit = this.handeleSubmit.bind(this);
  }

  componentWillMount() {
    const { navigation } = this.props;
    const topup = navigation.getParam('topup', false);
    const data_loan = navigation.getParam('data_loan', null);
    this.setState({ topup, data_loan })
  }

  changeText(amount) {
    amount = !amount ? 0 : amount;
    this.setState({ amount });
  }

  handeleSubmit() {
    const { topup, data_loan, amount } = this.state;
    if (parseFloat(amount) === 0) {
      this.setState({
        setAlert: true,
        alertText: 'Amount is required.',
        alertType: _sty.alertDanger
      })
      return;
    }

    if (topup) {
      if (parseFloat(amount) > parseFloat(data_loan.amount)) { 
        this.setState({
          setAlert: true,
          alertText: `Amount can't be more than principal.`,
          alertType: _sty.alertDanger
        })
        return;
      }
    }

    this.props.navigation.navigate('LoanType', {
      amount,
      topup,
      data_loan
    })
  }

  render() {
    const { amount } = this.state;

    return (
      <View style={[_sty.containerFlex]}>

        {this.state.setAlert && (<Alerts alertType={this.state.alertType} text={this.state.alertText} />)}

        <View style={[_sty.containerFlex, _sty.bgTheme, _sty.containerAlignCC]}>
          <Image style={_sty.positionTopImg} source={require('../../assets/images/bg-oval.png')} />

          <View style={[styles.keyboardContainer]}>
            <View style={[styles.keyboardContentWrapper]}>
              <Text style={[styles.keyboardContent]}><Cf value={parseFloat(amount)} /></Text>
            </View>

            <VirtualKeyboard
              action={<ForgotLink />}
              decimal={true}
              cellStyle={[styles.keyCell]}
              max={9}
              color='#fff'
              pressMode='string'
              onPress={(val) => this.changeText(val)}
            />

            {/* <View style={[_sty.alert, _sty.mt20, _sty.mb20]}>
              <Text style={_sty.alertBadge}>Hey!</Text>
              <Text style={[_sty.alertText, _sty.textWhite]}>You can set your maturity tenure</Text>
            </View> */}


            <TouchableOpacity onPress={this.handeleSubmit}
              style={[_sty.btn, _sty.alignSelfCenter,
              _sty.width60, _sty.bgThemeLight, _sty.boxShadow, _sty.mt15]}>
              <Text style={[_sty.btnText, _sty.textWhite]}>Continue</Text>
            </TouchableOpacity>

          </View>






        </View>
      </View>
    );
  }
}




const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(153,243,182,0.65)'
  },
  LinearGradient: {
    position: 'absolute', left: 0, right: 0, top: 0, height: '100%', flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  keyboardContainer: {
    alignSelf: 'center',
    width: '100%'
  },
  keyboardContentWrapper: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    height: 30,
    position: 'relative',
    marginTop: 20
  },
  keyboardContentRow: {
    width: 150,
    flexDirection: 'row',
    justifyContent: 'flex-start'
  },

  keyboardContent: {
    fontSize: 30,
    color: '#fff',
    fontWeight: 'bold'
  },

  keyCell: {
    width: 70,
    height: 40,
    marginHorizontal: 14
  },
  keyCellClear: {
  },





  textTitle: {
    fontSize: 22,
    display: 'flex',
    textAlign: 'center',
    fontWeight: '700',
    marginBottom: 10,
    marginTop: 30,
    color: '#fff',
  },
  textMsg: {
    fontSize: 18,
    display: 'flex',
    textAlign: 'center',
    color: '#fff',
  }
});

export default LoanRequest; 