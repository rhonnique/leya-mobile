import React, { Component, Fragment } from 'react';
import { Text, View, StyleSheet, Alert, Modal, Picker, TouchableHighlight, TouchableOpacity } from 'react-native';
//import Modal from "react-native-modal";
import { ModalAlert } from '../../Components/ModalAlert';
import { ElButton } from '../../Components/Elements';
import moment from "moment";

import Icon from "react-native-vector-icons/Ionicons";
import _sty from '../../assets/css/Styles';

import Cf from '../../Components/Cf';
import { Card, ListItem, Button, ButtonGroup } from 'react-native-elements'

class LoanType extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'REQUEST LOAN',
    headerTintColor: '#FFF',
    headerTitleStyle: {
      fontSize: 15
    },
    headerStyle: {
      elevation: 0,
      shadowOpacity: 0,
      backgroundColor: '#00A134',
      borderBottomWidth: 0
    }
  })

  constructor(props) {
    super(props);
    this.state = {
      month: moment().format(`MMM`),
      year: moment().format(`YYYY`),
      modalVisible: false,
      amount: 0,
      interest_amount: 0,
      interest_rate: 0,
      tenure: 0,
      modalAlertVisible: false,
      modalAlertTitle: '',
      modalAlertMessage: '',
      selectedIndex: null,
      topup: false
    }

    this.updateIndex = this.updateIndex.bind(this)
    this.type1 = this.type1.bind(this);
    this.type2 = this.type2.bind(this);
    this.handleProceed = this.handleProceed.bind(this)

  }

  componentWillMount() {
    const { navigation } = this.props;
    let amount = navigation.getParam('amount', 0);
    amount = parseFloat(amount);
    const topup = navigation.getParam('topup', false);
    this.setState({ amount, topup });
  }

  updateIndex(selectedIndex) {
    this.setState({ selectedIndex })
  }

  goBack() {
    const { goBack } = this.props.navigation;
    goBack();
  }

  handleProceed() {
    this.props.navigation.navigate('LoanCycle', {
      amount: this.state.amount,
      type: this.state.selectedIndex,
      topup: this.state.topup
    })
  }

  type1() {
    const { selectedIndex } = this.state
    const color = selectedIndex === 0 ? '#ffffff' : '#666666';
    return (<View style={{ paddingVertical: 13, paddingHorizontal: 15 }}>

      <Text style={[_sty.fontSize16, _sty.fontWeightBold, _sty.mb5, { color: color }]}>Beans Retail</Text>
      <Text style={[_sty.fontSize14, { color: color }]}>This type of loan implies that you are requesting for a loan as an individual.</Text>

    </View>);
  }

  type2() {
    const { selectedIndex } = this.state
    const color = selectedIndex === 1 ? '#ffffff' : '#666666';
    return (<View style={{ paddingVertical: 13, paddingHorizontal: 15 }}>
      <Text style={[_sty.fontSize16, _sty.fontWeightBold, _sty.mb5, { color: color }]}>Beans Commercial</Text>
      <Text style={[_sty.fontSize14, { color: color }]}>This type of loan is for organizations and corporate businesses or companies. </Text>

    </View>);
  }


  render() {
    const buttons = [{ element: this.type1 }, { element: this.type2 }]
    const { selectedIndex } = this.state

    return (<Fragment>


      <View style={[_sty.containerFlex]}>


        <View style={[_sty.rows, _sty.bgTheme, { paddingVertical: 25 }]}>
          <View style={[_sty.containerMdx]}>
            <Text style={[_sty.fontSize20, _sty.textWhite]}>What type of loan?</Text>
          </View>
        </View>


        <View style={[_sty.containerMdx, { paddingVertical: 25 }]}>



          <ButtonGroup
            onPress={this.updateIndex}
            selectedIndex={selectedIndex}
            buttons={buttons}
            innerBorderStyle={{ width: 0, color: 'transparent' }}
            containerStyle={{ height: 270, flexDirection: 'column', marginBottom: 0, marginTop: 0, marginLeft: 0, marginRight: 0, borderWidth: 0 }}
            buttonStyle={{ backgroundColor: '#F7F7F7', marginBottom: 40, borderWidth: 0, borderRadius: 5, justifyContent: 'flex-start', alignItems: 'flex-start', height: 120 }}
            selectedButtonStyle={{ backgroundColor: '#00A134' }}
          />


        </View>

        <View style={[_sty.vBottom, _sty.mb40]}>
          <ElButton
            title="Proceed"
            onPress={this.handleProceed}
            shadow={true}
            width={70}
            bg="#00A134"
            textColor="white"
            disabled={selectedIndex === null ? true : false}
          />
        </View>





      </View>

    </Fragment>
    );
  }
}

const styles = StyleSheet.create({

  itemStyle: {
    height: 36,
    color: 'black'
  },
  picker: {
    width: 100
  },

  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
  },
  showPickerBtn: {
    height: 44,
    backgroundColor: '#973BC2',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 16,
    borderRadius: 6,
  },
  yearMonthText: {
    fontSize: 20,
    marginTop: 12
  },

});


export default LoanType;