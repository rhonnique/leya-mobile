import React from 'react';
import { StyleSheet, Text, View, Button, Image, TouchableHighlight, BackHandler } from 'react-native';
import _sty from '../assets/css/Styles';



class Success extends React.Component {
    static navigationOptions = {
        header: null,
        headerLeft: null,
        gesturesEnabled: false,
    };

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', function () { return true })
    }


    render() {
        const { navigation } = this.props;
        const title = navigation.getParam('title', 'Congratulations');
        const message = navigation.getParam('message', '');
        const goto = navigation.getParam('goto', '');
        const callback = navigation.getParam('callback', {});

        //const { navigate } = this.props.navigation;
        //const title = 'Congratulations';
        //const message = `Your registration is successful`;
        return (
            <View style={[_sty.bgGreen, _sty.px10]}>
                <View style={styles.msgContainer}>
                    <Image source={require('./../assets/images/success.png')} />
                    <Text style={styles.textTitle}>{title}</Text>
                    <Text style={styles.textMsg}>{message}</Text>
                </View>

                <View style={styles.btnContainer}>
                    <TouchableHighlight onPress={() => navigation.navigate(goto, {
                        callbackSign: Math.floor(Math.random() * 498584949389),
                        callback: callback
                    })}
                        style={_sty.btnThemeWhite}>
                        <Text style={[_sty.btnTextGreen, _sty.fontBold]}>Continue</Text>
                    </TouchableHighlight>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    msgContainer: {
        height: '70%',
        color: '#fff',
        alignItems: 'center',
        justifyContent: 'center'

    },
    btnContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        height: '30%'
    },
    textTitle: {
        fontSize: 22,
        display: 'flex',
        textAlign: 'center',
        fontWeight: '700',
        marginBottom: 10,
        marginTop: 30,
        color: '#fff',
    },
    textMsg: {
        fontSize: 18,
        display: 'flex',
        textAlign: 'center',
        color: '#fff',
    }
});

export default Success