import React, { Fragment } from "react";
import { ActivityIndicator, AsyncStorage, Platform, Alert, StyleSheet, View, Modal, Image } from "react-native";
import Spinner from "react-native-loading-spinner-overlay";
import axios from "axios";
import GlobalFont from "react-native-global-font";
//import * as Permissions from 'expo-permissions';
import * as Font from 'expo-font';
import { Asset } from 'expo-asset';

const PUSH_ENDPOINT = 'https://exp.host/--/api/v2/push/send';

import GuessNav from "./Guest/GuessNav";
import AppNav from "./App/AppNav";

const Axios = axios.create();
Axios.defaults.timeout = 10000;

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: false,
      showSpinner: false,
      api_url: 'http://leya.trjcompanylimited.com',
      //api_url: 'http://testenv.com.ng/leya-server', 
      //api_url: 'http://192.168.27.69/leya-server',
      //api_url: 'http://192.168.43.140/leya-server',
      //api_url: 'http://172.20.10.2/leya-server',
      //api_url: 'http://192.168.43.161/leya-server',
      //api_url: "http://192.168.100.4/leya-server",
      //api_url: 'http://leya/leya-server', 
      //api_url: 'http://192.168.1.150/leya-server',
      //api_url: 'http://10.178.43.159/leya-server', 
      token: "",
      email: "",
      settings: null,
      customer: null,
      investment: null,
      loan: null,
      trust: null,
      page_loading: true,
      set_notify: false,
      set_terms_investments: false,
      set_terms_loans: false,
      set_terms_trusts: false,
      push_token: null
    };

    this.setSpinner = this.setSpinner.bind(this);
    this.setLogin = this.setLogin.bind(this);
    this.handleAxiosError = this.handleAxiosError.bind(this);
    this.investmentSet = this.investmentSet.bind(this);
    this.loanSet = this.loanSet.bind(this);
    this.trustSet = this.trustSet.bind(this);
    this.customerSet = this.customerSet.bind(this);
    this.notifySet = this.notifySet.bind(this);
    this.termsInvestmentsSet = this.termsInvestmentsSet.bind(this);
    this.termsLoansSet = this.termsLoansSet.bind(this);
    this.termsTrustsSet = this.termsTrustsSet.bind(this);
    this.spinnerShow = this.spinnerShow.bind(this);
    this.spinnerHide = this.spinnerHide.bind(this);
    this.someCheck = this.someCheck.bind(this);
  }

  async componentWillMount() {
    axios.defaults.baseURL = this.state.api_url;
    let customer = (await AsyncStorage.getItem("customer")) || null;
    customer = customer !== null ? JSON.parse(customer) : null;
    let loggedIn = (await AsyncStorage.getItem("loggedIn")) || false;
    loggedIn = loggedIn === "true";
    let token = (await AsyncStorage.getItem("token")) || "";
    let email = (await AsyncStorage.getItem("email")) || "";

    const set_notify = JSON.parse(await AsyncStorage.getItem("set_notify")) || false;
    const set_terms_investments = JSON.parse(await AsyncStorage.getItem("set_terms_investments")) || false;
    const set_terms_loans = JSON.parse(await AsyncStorage.getItem("set_terms_loans")) || false;
    const set_terms_trusts = JSON.parse(await AsyncStorage.getItem("set_terms_trusts")) || false;

    await Asset.loadAsync([
      require("./assets/splash.png"),
      require("./assets/images/bg-circle.png"),
      require("./assets/images/bg-curve-top.png"),
      require("./assets/images/bg-oval.png"),
      require("./assets/images/confetti.png"),
      require("./assets/images/curve-bottom-2.png"),
      require("./assets/images/curve-bottom.png"),
      require("./assets/images/flora-2.png"),
      require("./assets/images/flora.png"),
      require("./assets/images/ico-activity.png"),
      require("./assets/images/ico-bell.png"),
      require("./assets/images/ico-faq.png"),
      require("./assets/images/ico-fingerprint.png"),
      require("./assets/images/ico-help.png"),
      require("./assets/images/ico-lock.png"),
      require("./assets/images/ico-privacy.png"),
      require("./assets/images/ico-settings.png"),
      require("./assets/images/ico-terms.png"),
      require("./assets/images/intro-1.png"),
      require("./assets/images/intro-2.png"),
      require("./assets/images/intro-3.png"),
      require("./assets/images/loan-bg.png"),
      require("./assets/images/logo-sm.png"),
      require("./assets/images/logo-white.png"),
      require("./assets/images/logo.png"),
      require("./assets/images/password-tint.png"),
      require("./assets/images/password.png"),
      require("./assets/images/pending-bg.png"),
      require("./assets/images/success.png"),
      require("./assets/images/tab-icon-1-active.png"),
      require("./assets/images/tab-icon-1.png"),
      require("./assets/images/tab-icon-2.png"),
      require("./assets/images/tab-icon-2-active.png"),
      require("./assets/images/tab-icon-3.png"),
      require("./assets/images/tab-icon-3-active.png"),
      require("./assets/images/trust-bg.png")
    ]);

    await Font.loadAsync({
      //"nunito-sans-light": require("./assets/fonts/NunitoSans-Light.ttf"),
      "nunito-sans-regular": require("./assets/fonts/NunitoSans-Regular.ttf"),
      "nunito-sans-bold": require("./assets/fonts/NunitoSans-Bold.ttf")
      /* "nunito-sans-semibold": require("./assets/fonts/NunitoSans-SemiBold.ttf"),
      "nunito-sans-bold": require("./assets/fonts/NunitoSans-Bold.ttf"),
      "nunito-sans-black": require("./assets/fonts/NunitoSans-Black.ttf") */
    });

    let fontName = "nunito-sans-regular";
    let fontNameBold = "nunito-sans-bold";
    GlobalFont.applyGlobal(fontName);
    GlobalFont.applyGlobal(fontNameBold); 

    this.setState({
      customer,
      loggedIn,
      token,
      email,
      set_notify,
      set_terms_investments,
      set_terms_loans,
      set_terms_trusts,
    });

    // If token exist
    if (token) {
      await this.customerGet()
        .then(response => response)
        .then(async (response) => {
          const { customer, settings } = response.data;
          const notify = parseInt(customer.notify) === 1 ? true : false;
          await this.customerSet(customer);
          await this.notifySet(notify);
          this.settingsSet(settings);
        })
        .catch(error => {
          this.setLogout();
          /* console.log(JSON.stringify(error), "app... customerGet... error...");
          if (
            error.response &&
            error.response.status &&
            error.response.status === 401
          ) {
            this.setLogout();
          } */
        });
    }

    this.setState({ page_loading: false });
    //console.log(this.state, 'state......')
  }

  termsGet = async () => {
    const response = await axios.get(`/api/terms`);
    return response;
  };

  settingsGet = async () => {
    const response = await axios.get(`/api/settings`);
    return response;
  };

  settingsSet = async (settings) => {
    await AsyncStorage.setItem("settings", JSON.stringify(settings));
    this.setState({ settings });
  };

  notifySet = async (set_notify) => {
    await AsyncStorage.setItem("set_notify", JSON.stringify(set_notify));
    this.setState({ set_notify });
  };

  termsInvestmentsSet = async (set_terms_investments) => {
    await AsyncStorage.setItem("set_terms_investments", JSON.stringify(set_terms_investments));
    this.setState({ set_terms_investments });
  };

  termsLoansSet = async (set_terms_loans) => {
    await AsyncStorage.setItem("set_terms_loans", JSON.stringify(set_terms_loans));
    this.setState({ set_terms_loans });
  };

  termsTrustsSet = async (set_terms_trusts) => {
    await AsyncStorage.setItem("set_terms_trusts", JSON.stringify(set_terms_trusts));
    this.setState({ set_terms_trusts });
  };

  customerGet = async () => {
    const response = await axios.get(`/api/customer`, {
      headers: { Authorization: "Bearer " + this.state.token }
    });
    return response;
  };

  customerSet = async (customer) => {
    await AsyncStorage.setItem("customer", JSON.stringify(customer));
    this.setState({ customer });
  };

  customerEdit = async (data) => {
    console.log(data, 'customerEdit...')
    const response = await axios.patch(`/api/customer/${this.state.customer.id}`, data, {
      headers: { Authorization: "Bearer " + this.state.token }
    });
    return response;
  };

  parseBoolean(val) {
    return parseInt(val) === 1 ? true : false;
  }

  changePin = async (data) => {
    const response = await axios.post(`/api/customer/changepin`, data, {
      headers: { Authorization: "Bearer " + this.state.token }
    });
    return response;
  };

  /*********
   *  ******* REGISTER *******
   ****************************/
  register = async data => {
    this.setEmail(data.email);
    const response = await axios.post(`/api/register`, data);
    return response;
  };

  registerPassword = async data => {
    const response = await axios.post(`/api/register/password`, data);
    return response;
  };

  /*********
   *  ******* LOGIN *******
   ****************************/

  setEmail = async email => {
    await AsyncStorage.setItem("email", email);
    this.setState({ email });
  };

  login = async data => {
    this.setEmail(data.email);
    const response = await axios.post(`/api/login`, data);
    return response;
  };

  loginPassword = async data => {
    const response = await axios.post(`/api/login/password`, data);
    return response;
  };

  setLogin = async data => {
    const loggedIn = "true";
    const { token, customer } = data;
    console.log(token, customer);

    /* AsyncStorage.setItem('token', data.token);
    AsyncStorage.setItem('customer', JSON.stringify(data.customer));
    AsyncStorage.setItem('loggedIn', loggedIn); */
    await AsyncStorage.setItem("token", data.token);
    await AsyncStorage.setItem("customer", JSON.stringify(data.customer));
    await AsyncStorage.setItem("loggedIn", loggedIn);

    this.setState({ token, customer, loggedIn });
  };

  setLogout = async () => {
    const loggedIn = false;
    const token = "";
    const customer = null;
    AsyncStorage.removeItem("token");
    AsyncStorage.removeItem("customer");
    AsyncStorage.removeItem("loggedIn");
    this.setState({ token, customer, loggedIn });
  };

  /*********
   *  ******* REQUEST *******
   ****************************/

  requestGet = async type => {
    const response = await axios.get(`/api/request?type=${type}`, {
      headers: { Authorization: "Bearer " + this.state.token }
    });
    return response;
  };

  requestAdd = async data => {
    const response = await axios.post(`/api/request`, data, {
      headers: { Authorization: "Bearer " + this.state.token }
    });
    return response;
  };

  reuqestDelete = async id => {
    const response = await axios.delete(`/api/request/${id}`, {
      headers: { Authorization: "Bearer " + this.state.token }
    });
    return response;
  };

  /*********
   *  ******* INVESTMENTS *******
   ****************************/

  investmentGet = async query => {
    query = query ? query : "";
    const response = await axios.get(`/api/investment${query}`, {
      headers: { Authorization: "Bearer " + this.state.token }
    });
    return response;
  };

  investmentShow = async id => {
    const response = await axios.get(`/api/investment/${id}`, {
      headers: { Authorization: "Bearer " + this.state.token }
    });
    return response;
  };

  investmentRequest = async data => {
    const response = await axios.post(`/api/investment`, data, {
      headers: { Authorization: "Bearer " + this.state.token }
    });
    return response;
  };

  investmentDelete = async id => {
    const response = await axios.delete(`/api/request/${id}`, {
      headers: { Authorization: "Bearer " + this.state.token }
    });
    return response;
  };

  investmentSet = async investment => {
    await AsyncStorage.setItem("investment", JSON.stringify(investment));
    this.setState({ investment });
  };

  /*********
   *  ******* LOANS *******
   ****************************/

  loanGet = async query => {
    query = query ? query : "";
    const response = await axios.get(`/api/loan${query}`, {
      headers: { Authorization: "Bearer " + this.state.token }
    });
    return response;
  };

  loanShow = async id => {
    const response = await axios.get(`/api/loan/${id}`, {
      headers: { Authorization: "Bearer " + this.state.token }
    });
    return response;
  };

  loanRequest = async data => {
    const response = await axios.post(`/api/loan`, data, {
      headers: { Authorization: "Bearer " + this.state.token }
    });
    return response;
  };

  loanDelete = async id => {
    const response = await axios.delete(`/api/request/${id}`, {
      headers: { Authorization: "Bearer " + this.state.token }
    });
    return response;
  };

  loanSet = async loan => {
    await AsyncStorage.setItem("loan", JSON.stringify(loan));
    this.setState({ loan });
  };

  /*********
   *  ******* TRUSTS *******
   ****************************/

  trustGet = async query => {
    query = query ? query : "";
    const response = await axios.get(`/api/trust${query}`, {
      headers: { Authorization: "Bearer " + this.state.token }
    });
    return response;
  };

  trustShow = async id => {
    const response = await axios.get(`/api/trust/${id}`, {
      headers: { Authorization: "Bearer " + this.state.token }
    });
    return response;
  };

  trustRequest = async data => {
    const response = await axios.post(`/api/trust`, data, {
      headers: { Authorization: "Bearer " + this.state.token }
    });
    return response;
  };

  trustDelete = async id => {
    const response = await axios.delete(`/api/request/${id}`, {
      headers: { Authorization: "Bearer " + this.state.token }
    });
    return response;
  };

  trustSet = async trust => {
    await AsyncStorage.setItem("trust", JSON.stringify(trust));
    this.setState({ trust });
  };

  trustGetTrans = async () => {
    const response = await axios.get(`/api/trust`, {
      headers: { Authorization: "Bearer " + this.state.token }
    });
    return response;
  };




  setSpinner(showSpinner) {
    this.setState({ showSpinner });
  }

  spinnerShow() {
    this.setState({ showSpinner: true });
    /* setTimeout(() => {
      this.setState({ showSpinner: false });
    }, 30000); */
  }

  spinnerHide() {
    this.setState({ showSpinner: false });
    /* setTimeout(() => {
      this.setState({ showSpinner: false });
    }, 30000); */
  }

  alertErrorSet(msg) {
    setTimeout(() => {
      Alert.alert(`Oops!`, msg);
    }, 200);
  }

  setError = (error, skip = []) => {
    //console.log(JSON.stringify(error.response), "setError__error..................");
    let setMessage = '';
    let setCode = null;

    if (error.response) {
      const { data, status } = error.response;
      const message = data && data.message;
      const code = data && data.code;
      switch (status) {
        case 0:
          setMessage = 'No connectivity, please check your internet and try again!';
          setCode = 'ERR_L010';
          break;
        case 401:
          if (this.someCheck(skip, 401)) {
            setMessage = message;
            setCode = code;
          } else {
            this.setLogout();
          }
          break;
        case 422:
          let listMsg = '';
          for (let [key, value] of Object.entries(data)) {
            console.log(`${key}: ${value}`);
            listMsg = listMsg + '- ' + value[0] + '\n';
          }

          setMessage = `The following error(s) occurred:
            ${listMsg}`;

          break;
        case 500:
          if (this.someCheck(skip, 500)) {
            return;
          }

          if (message && code) {
            setMessage = message;
            setCode = code;
          } else {
            setMessage = 'An error occurred, please retry!';
            setCode = `ERR_L5100`;
          }
          break;
        default:
          setMessage = 'An error occurred, please retry!';
          setCode = `ERR_D5100`;
      }

    } else {
      setMessage = 'An error occurred, please retry';
      setCode = 'ERR_L00';
    }

    this.alertErrorSet(`${setMessage}${(setCode ? `\n(${setCode})` : '')}`);
  }

  someCheck(check, value) {
    return check.some(val => val === value);
  }

  sss = err => {
    return false;
  }

  handleAxiosError = error => {
    console.log(JSON.stringify(error), "error..................");
    if (error.response) {
      if (error.response.status) {
        console.log(error.response.status, "logging out22....");
        switch (error.response.status) {
          case 401:
            this.setLogout();
            break;
        }
      }

      if (error.response.data && error.response.data.code) {
        console.log(error.response.data.code, "logging out22....");
        switch (error.response.data.code) {
          case "ERR_AUTH_600":
          case "ERR_AUTH_601":
          case "ERR_AUTH_605":
          case "ERR_AUTH_604":
            this.setLogout();
            break;
        }
      }
    } else {
      /* Toastr.error('Can not connect to server, please retry or check you internet connectivity',
          'Connectivity Error!'); */
    }
  };

  render() {
    return (
      <Fragment>
        {/* <AppNav /> */}
        {/* <Spinner visible={this.state.showSpinner} textContent={"Loading..."} /> */}

        {/* <OrientationLoadingOverlay
          visible={this.state.showSpinner}
          color="white"
          indicatorSize="large"
          messageFontSize={24}
          message="aa 6666 Loading..."
        /> */}


        <Modal
          transparent={true}
          animationType={'none'}
          visible={this.state.showSpinner}
          onRequestClose={() => { console.log('close modal') }}>
          <View style={styles.modalBackground}>
            <View style={styles.activityIndicatorWrapper}>
              <ActivityIndicator
                animating={this.state.showSpinner} />
            </View>
          </View>
        </Modal>

        {this.state.page_loading ? (
          <Fragment>
            <Image style={[{ width: '100%', resizeMode: 'contain' }]} source={require('./assets/splash.png')} />
          </Fragment>
        ) : (
            <Fragment>
              {this.state.loggedIn ? (
                <AppNav
                  screenProps={{
                    settingsGet: this.settingsGet,
                    settingsSet: this.settingsSet,
                    termsGet: this.termsGet,
                    parseBoolean: this.parseBoolean,
                    setSpinner: this.setSpinner,
                    spinnerShow: this.spinnerShow,
                    spinnerHide: this.spinnerHide,
                    alertErrorSet: this.alertErrorSet,
                    setError: this.setError,

                    notifySet: this.notifySet,
                    termsInvestmentsSet: this.termsInvestmentsSet,
                    termsLoansSet: this.termsLoansSet,
                    termsTrustsSet: this.termsTrustsSet,

                    customerGet: this.customerGet,
                    customerSet: this.customerSet,
                    customerEdit: this.customerEdit,
                    changePin: this.changePin,

                    requestGet: this.requestGet,
                    requestAdd: this.requestAdd,
                    reuqestDelete: this.reuqestDelete,

                    investmentRequest: this.investmentRequest,
                    investmentGet: this.investmentGet,
                    investmentShow: this.investmentShow,
                    investmentDelete: this.investmentDelete,
                    investmentSet: this.investmentSet,

                    loanGet: this.loanGet,
                    loanShow: this.loanShow,
                    loanRequest: this.loanRequest,
                    loanDelete: this.loanDelete,
                    loanSet: this.loanSet,

                    trustGet: this.trustGet,
                    trustShow: this.trustShow,
                    trustRequest: this.trustRequest,
                    trustDelete: this.trustDelete,
                    trustSet: this.trustSet,

                    setLogout: this.setLogout,
                    handleAxiosError: this.handleAxiosError,

                    ...this.state
                  }}
                />
              ) : (
                  <GuessNav
                    screenProps={{
                      setSpinner: this.setSpinner,
                      spinnerShow: this.spinnerShow,
                      spinnerHide: this.spinnerHide,
                      alertErrorSet: this.alertErrorSet,
                      register: this.register,
                      registerPassword: this.registerPassword,
                      login: this.login,
                      loginPassword: this.loginPassword,
                      setLogin: this.setLogin,
                      handleAxiosError: this.handleAxiosError,
                      setError: this.setError,

                      ...this.state
                    }}
                  />
                )}
            </Fragment>
          )}
      </Fragment>
    );
  }
}


const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'
  },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF',
    height: 80,
    width: 80,
    borderRadius: 8,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around'
  }
});

export default App;
